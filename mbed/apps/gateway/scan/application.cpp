#include "application.h"
#include <ble/bledefs.h>
#include <ble/bleevent.h>
#include <ble/blesystem.h>
#include <core/corecommand.h>
#include <core/corestatus.h>
#include <devices/devicessettings.h>
#include <devices/devicesprotocol.h>
#include <devices/devicesserial.h>
#include <radio/radiocontrol.h>
#include <radio/radioschedule.h>
#include <utils/utilsprint.h>
#include <utils/utilsendian.h>
#include <BLE.h>
#include <vector>
#include <stddef.h>
#include <std/stdvector.h>
#include <utils/utilswatchdog.h>

extern Serial pc;
using namespace iot;
using namespace iot::devices;
using namespace iot::utils;
using namespace iot::radio;
using namespace iot::ble;

#define STATUS_PERIOD 5.0f
static const char scan[] = "Scan";

#define UART_MEMORY_SIZE 0x00000200

static uint8_t memory[UART_MEMORY_SIZE];

namespace iot {
/**
 * @brief The Status class
 */
class Status: public core::Status
{
public:
    Status(const core::Status::Resets &resets = 0):
        core::Status(resets) {

    }

    virtual ~Status() {

    }

public:
    virtual core::Status::Period period() const {
        return STATUS_PERIOD;
    }

    virtual core::Status::Device device() const {
        return core::Status::Scanner;
    }

    virtual const char *descrition() const {
        return  &scan[0];
    }
};

/**
 * @brief The Application::Private class
 */
class Application::Private
{
public:

    Private():
        _sequence(0) {
        memset(&_payload[0], 0, BLE_ADDRESS_SIZE);
    }

    virtual ~Private() {

    }

    void sent(const devices::Serial::Id &id, bool rejected) {
    }

    void received(System::Rssi rssi,
                  const uint8_t *address,
                  const uint8_t *details,
                  uint8_t size) {

        int error;

        ble::Message::Role role = ble::Message::role(details, size, error);

        if (error != 0)
            return;

        if ((role & 0x0F) != Message::Device)
            return;

        if (memcmp(&_payload[0], address, BLE_ADDRESS_SIZE) == 0) {

            ble::Message::Sequence sequence = ble::Message::sequence(details,
                                                                     size,
                                                                     error);
            if (error != 0) {
                PRINTF("*application received* sequence error: %d\r\n", error);
                return;
            }

            if (_sequence == sequence) {
                PRINTF("*application received* ignored sequence: %d\r\n",
                       _sequence);
                return;
            }
            _sequence = sequence;
        }

        PRINTF("*application received* received sequence: %d\r\n",
               _sequence);

        _payload[0] = address[5];
        _payload[1] = address[4];
        _payload[2] = address[3];
        _payload[3] = address[2];
        _payload[4] = address[1];
        _payload[5] = address[0];

        _payload[BLE_ADDRESS_SIZE] = rssi;

        memcpy(&_payload[BLE_ADDRESS_SIZE] + sizeof(System::Rssi),
               details,
               size);

        devices::Serial::Id id;

        _serial.send(&_payload[0],
                     BLE_ADDRESS_SIZE + sizeof(System::Rssi) + size,
                     id);
    }

    void advertised(const System::Id &id, bool rejected) {

    }

    int initialize(ble::System &system) {

        PRINTF("*application* serial opening...successed\r\n");
        devices::Serial::Sent sent(this, &Private::sent);
        int error = _serial.open(pc, _protocol, &memory[0], UART_MEMORY_SIZE, sent);
        if (error != 0) {
            PRINTF("*application* serial opening...failed with error: %d\r\n",
                   error);
            return error;
        }

        ble::System::Received received(this, &Private::received);
        ble::System::Advertised advertised(this, &Private::advertised);
        error = system.start(180, received, 64, advertised);
        if (error != BLE_ERROR_NONE) {
            PRINTF("*application* scan starting...failed with error: %d\r\n",
                   error);
            _serial.close();
            return error;
        }

        PRINTF("*application* scan starting...successed\r\n");
        return 0;
    }

    void release(ble::System &system) {
        system.stop();
        _serial.close();
    }

private:
    devices::Serial _serial;
    devices::slip::Protocol _protocol;
    uint8_t _payload[BLE_ADDRESS_SIZE +
                     sizeof(System::Rssi) +
                     BLE_PAYLOAD_SIZE];
    ble::Message::Sequence _sequence;
    int _error;
};

/**
 * @brief Application::Application
 */
Application::Application():
    _private(new Private)
{

}

Application::~Application()
{
    delete _private;
}

int Application::initialize(ble::System &system)
{
    return _private->initialize(system);
}


void Application::release(ble::System &system)
{
    _private->release(system);
}

core::Status &Application::status()
{
    static Status _status;
    return _status;
}

Application &Application::instance()
{
    static Application app;
    return app;
}

} // namespace iot
