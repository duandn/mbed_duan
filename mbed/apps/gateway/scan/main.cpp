/* mbed Microcontroller Library
 * Copyright (c) 2006-2014 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <utils/utilsprint.h>
#include <mbed.h>
#include "application.h"

#if defined HARDWARE_REVISION

#if HARDWARE_REVISION==4

    #define SERIAL_TX p10
    #define SERIAL_RX p11
    #define SERIAL_RTS p9
    #define SERIAL_CTS p8

    #define ANTENA_LNA_ENABLE p17
    #define ANTENA_BYPASS p16
    #define ANTENA_SWITCH p15

#elif HARDWARE_REVISION==5
    #define SERIAL_TX p15
    #define SERIAL_RX p16
    #define SERIAL_RTS p14
    #define SERIAL_CTS p13

    #define ANTENA_LNA_ENABLE p19
    #define ANTENA_PA_ENABLE p20
    #define ANTENA_BYPASS p18
    #define ANTENA_SWITCH p17
#else
    #error "Hardware revision not campatibled"
#endif

DigitalOut antenaLNAEnable(ANTENA_LNA_ENABLE);
#if HARDWARE_REVISION==5
DigitalOut antenaPAEnable(ANTENA_PA_ENABLE);
#endif

DigitalOut antenaBypass(ANTENA_BYPASS);
DigitalOut antenaSwitch(ANTENA_SWITCH);
Serial pc(SERIAL_TX, SERIAL_RX);

int main()
{
    antenaLNAEnable = 1;

#if HARDWARE_REVISION==5
    antenaPAEnable = 1;
#endif

    antenaBypass = 0;
    antenaSwitch = 0;

    pc.baud(115200);
    PRINTF_INITIALIZE();
    pc.set_flow_control(Serial::RTSCTS, SERIAL_RTS, SERIAL_CTS);
    PRINTF("*scan main* started\r\n");
    iot::Application &app = iot::Application::instance();
    while (true)
        app.exec();
    return 0;
}

#else
    #error "Hardware revision not found"
#endif

