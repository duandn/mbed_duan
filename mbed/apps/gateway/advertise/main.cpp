/* mbed Microcontroller Library
 * Copyright (c) 2006-2014 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <utils/utilsprint.h>
#include <mbed.h>
#include "application.h"

#if defined HARDWARE_REVISION

#if HARDWARE_REVISION==4

    #define SERIAL_TX p10
    #define SERIAL_RX p11
    #define SERIAL_RTS p9
    #define SERIAL_CTS p8

    #define ANTENA_ENABLE p17
    #define ANTENA_BYPASS p16
    #define ANTENA_SELECT p15
#elif HARDWARE_REVISION==5
    #define SERIAL_TX p10
    #define SERIAL_RX p11
    #define SERIAL_RTS p9
    #define SERIAL_CTS p8

    #define ANTENA_ENABLE p17
    #define ANTENA_BYPASS p16
    #define ANTENA_SELECT p15
#else
    #error "Hardware revision not campatibled"
#endif

DigitalOut antenaEnable(ANTENA_ENABLE);
DigitalOut antenaBypass(ANTENA_BYPASS);
DigitalOut antenaSelect(ANTENA_SELECT);
Serial pc(SERIAL_TX, SERIAL_RX);

int main()
{
    antenaEnable = 1;
    antenaBypass = 0;
    antenaSelect = 0;

    pc.baud(115200);
    PRINTF_INITIALIZE();
    pc.set_flow_control(Serial::RTSCTS, SERIAL_RTS, SERIAL_CTS);
    PRINTF("*scan main* started\r\n");
    iot::Application &app = iot::Application::instance();
    while (true)
        app.exec();
    return 0;
}

#else
    #error "Hardware revision not found"
#endif

