#include "application.h"
#include <ble/bledefs.h>
#include <ble/blesystem.h>
#include <ble/gap/blegappacket.h>
#include <core/corestatus.h>
#include <devices/devicesserial.h>
#include <devices/devicesprotocol.h>
#include <radio/radiocontrol.h>
#include <radio/radioschedule.h>
#include <utils/utilsprint.h>
#include <utils/utilsendian.h>
#include <BLE.h>
#include <stddef.h>
#define STATUS_PERIOD 5.0f
static const char Description[] = "Adv";

extern Serial pc;
using namespace iot::devices;
using namespace iot::radio;
using namespace iot::ble;
using namespace iot::ble::gap;
namespace iot {

/**
 * @brief The Application::Private class
 */
class Application::Private
{
public:

    /**
     * @brief The Status class
     */
    class Status: public core::Status
    {
    public:
        Status(const core::Status::Resets &resets = 0):
            core::Status(resets) {

        }

        virtual ~Status() {

        }

    public:
        virtual core::Status::Period period() const {
            return STATUS_PERIOD;
        }

        virtual core::Status::Device device() const {
            return core::Status::Advertiser;
        }

        virtual const char *descrition() const {
            return  &Description[0];
        }
    };

    Private(System &system):
        _system(system) {
    }

    virtual ~Private() {

    }

    void advertised(const System::Id &id, bool rejected) {
        PRINTF("*application* advertised packet with id: %d\r\n", (int)id);
    }

    void received(const uint8_t *payload, const devices::Serial::Size &size) {
        PRINTF("*application* packet size: %d advertising...%d\r\n", size);

        int error = _system.advertise(payload, size, 7, _id);

        if (error == 0) {
            PRINTF(" successed\r\n");
        } else {
            PRINTF(" failed with error: %d\r\n", error);
        }
    }

    int initialize() {

        PRINTF("*application* initializing..");

        System::Advertised advertised(this, &Private::advertised);
        int error = _system.start(512, advertised);

        if (error != 0) {
            PRINTF("with error: %d\r\n", error);
            return error;
        }

        PRINTF("successed\r\n");

        PRINTF("*application* serial initializing...");
        devices::Serial::Received received(this, &Private::received);
        error = _serial.open(pc, _protocol, received);
        if (error != 0) {
            _system.stop();
            PRINTF("with error: %d\r\n", error);
            return error;
        }
        PRINTF("successed\r\n");
        return 0;
    }

    void release() {
        _serial.close();
        _system.stop();
    }
private:
    iot::ble::System &_system;
    devices::Serial _serial;
    slip::Protocol _protocol;
    System::Id _id;
};
/**
 * @brief Application::Application
 */
Application::Application():
    _private(NULL)
{

}

Application::~Application()
{
}

int Application::initialize(ble::System &system)
{
    if (_private != NULL)
        return -1;
    _private = new Private(system);
    int error = _private->initialize();
    if (error != 0) {
        delete _private;
        _private = NULL;
        return error;
    }
    return 0;
}


void Application::release(ble::System &system)
{
    if (_private == NULL)
        return;
    _private->release();
    delete _private;
    _private = NULL;
}

core::Status &Application::status()
{
    static Private::Status _status;
    return _status;
}

Application &Application::instance()
{
    static Application app;
    return app;
}

} // namespace iot
