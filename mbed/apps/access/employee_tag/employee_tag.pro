TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
INCLUDEPATH += ./../../../iot \
               ./../../../mbed-os \
               ./../../../mbed-os/features/FEATURE_BLE \
               ./../../../mbed-os/features/FEATURE_BLE/ble \
               ./../../../mbed-os/targets/TARGET_NORDIC/TARGET_NRF5/sdk/device \
               ./../../../mbed-os/targets/TARGET_NORDIC/TARGET_NRF5/TARGET_MCU_NRF51822_UNIFIED/sdk/softdevice/s130/headers \
               ./../../../mbed-os/targets/TARGET_NORDIC/TARGET_NRF5/sdk/libraries/experimental_section_vars \
               ./../../../mbed-os/targets/TARGET_NORDIC/TARGET_NRF5/sdk/libraries/fstorage \
               ./../../../mbed-os/targets/TARGET_NORDIC/TARGET_NRF5/sdk/libraries/fsd \
               ./../../../mbed-os/targets/TARGET_NORDIC/TARGET_NRF5/sdk/libraries/util \
               ./../../../mbed-os/targets/TARGET_NORDIC/TARGET_NRF5/TARGET_MCU_NRF51822_UNIFIED/TARGET_NRF51_DK

DEFINES += DEVICE_SERIAL=1 \
           DEVICE_PWMOUT=1 \
           DEVICE_I2C=1 \
           DEVICE_SPI=1 \
           DEVICE_ANALOGIN=1 \
           DEVICE_SERIAL_ASYNCH=1 \
           DEVICE_LOWPOWERTIMER

SOURCES += main.cpp \
    application.cpp

HEADERS += \
    application.h

DISTFILES += \
    Makefile
