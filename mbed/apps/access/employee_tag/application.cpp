#include "application.h"
#include <ble/gap/blegapevent.h>
#include <ble/gap/blegappacket.h>
#include <ble/blesystem.h>
#include <core/corecommand.h>
#include <devices/devicesnfc.h>
#include <devices/devicessettings.h>
#include <radio/radiocontrol.h>
#include <radio/radioschedule.h>
#include <utils/utilsendian.h>
#include <utils/utilsprint.h>
#include <stddef.h>
#include <BLE.h>


using namespace iot;
using namespace iot::utils;
using namespace iot::devices;
using namespace iot::devices::serial;
using namespace iot::ble::gap;
using namespace iot::radio;

namespace iot {

uint32_t _count = 0;
const uint8_t interval = 10;
/**
 * @brief The Application::Private class
 */
class Application::Private
{
public:

    /**
     * @brief The Event class
     */
    class Event: public ble::Event
    {
    public:
        Event() {

        }

        virtual ble::Message::Command command() const {
            return ble::Message::TagSensor;
        }

        virtual uint8_t _size() const {
            return strlen("VG-07828");
        }

        virtual void _fill(uint8_t *details) const {
            memcpy(details, "VG-07828", strlen("VG-07828"));
        }

    };

    Private(ble::System &system):
        _system(system),
        _led(p8),
        _nfc_vdd(VDD_NFC){
    }

    ~Private() {

    }

    void advertised(const Controller::Id &id, bool rejected) {

    }

    void ticker() {
        _system.advertise(_event, "Tag");
    }

    int initialize() {

        _ticker.attach(Callback<void()>(this, &Private::ticker), 1.0f);
        _led = 1;
        _nfc_vdd = 1;
        iot::devices::i2c::Settings settings(I2C_SDA,I2C_SCL,I2C_FREQ,NFC_ADD);
        int error = _nfc.initialize(settings);
        if (error != 0) {
            PRINTF("*NFC init* failed with error: %d\r\n", error);
            return error;
        }

        error = _nfc.writeNFC("Domain:QuangHM ID:VG-07828 Contact:(+84)974848698 Dept:IoT");
        if (error != 0) {
            PRINTF("*NFC write* failed with error: %d\r\n", error);
            return error;
        }
        _nfc_vdd = 0;

        return 0;
    }

    void release() {
    }
private:
    ble::System &_system;
    Ticker _ticker;
    DigitalOut _led;
    DigitalOut _nfc_vdd;
    NT3H1201::NfcCom _nfc;    
    Event _event;
};
/**
 * @brief Application::Application
 */
Application::Application():
    _private(NULL)
{

}

Application::~Application()
{
}

int Application::initialize(ble::System &system)
{
    if (_private != NULL)
        return -1;
    _private = new Private(system);
    int error = _private->initialize();
    if (error != 0) {
        delete _private;
        _private = NULL;
        return error;
    }
    return 0;
}


void Application::release(ble::System &system)
{
    if (_private == NULL)
        return;
    _private->release();
    delete _private;
    _private = NULL;
}

Application &Application::instance()
{
    static Application app;
    return app;
}

ble::status::Event::Device Application::device() const
{
    return ble::status::Event::AccessDoor;
}
} // namespace iot
