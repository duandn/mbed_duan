#ifndef APPLICATION_H
#define APPLICATION_H
#include <ble/bleapplication.h>

#define SERIAL_TX p9
#define SERIAL_RX p10

#define I2C_SDA     (p14)
#define I2C_SCL     (p15)
#define I2C_FREQ    (400000)
//#define NFC_ADD     (0x02 << 1)
#define NFC_ADD     (0x55 << 1)

#define VDD_NFC     (p11)
namespace iot {

class Application: public ble::Application
{
public:
    Application();

    virtual ~Application();

private:

    virtual int initialize(ble::System &system);

    virtual void release(ble::System &system);

    virtual ble::status::Event::Device device() const;

private:
    class Private;
    Private *_private;

public:
    static Application &instance();
};

} // namespace iot
#endif // APPLICATION_H
