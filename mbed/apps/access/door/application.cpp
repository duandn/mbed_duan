#include "application.h"
#include <ble/blesystem.h>
#include <core/corestatus.h>
#include <ble/blerequest.h>
#include <ble/blereply.h>
#include <core/corecommand.h>
#include <devices/devicessettings.h>
#include <radio/radiocontrol.h>
#include <radio/radioschedule.h>
#include <utils/utilsprint.h>
#include <utils/utilsendian.h>
#include <utils/utilswatchdog.h>
#include <BLE.h>
#include <stddef.h>
#include <nrf51.h>

//#define FIRST_DOOR
#define STATUS_PERIOD 5.0f

#define RELAY_PIN p2
#define LED_PIN p4

#define ON 0
#define OFF 1

using namespace iot;
using namespace iot::ble;
using namespace iot::devices::serial;
using namespace iot::radio;
using namespace iot::utils;
using namespace iot::utils::endian;

static const char Door[] = "ADR";
static const char Open[] = "OPE";

/**
 * @brief The Application::Private class
 */
#define ACCESS_DOOR_PIN p7

namespace iot {

class Application::Private
{

public:

    /**
     * @brief The Status class
     */
    class Status: public core::Status
    {
    public:
        Status(const core::Status::Resets &resets = 0):
            core::Status(resets) {

        }

        virtual ~Status() {

        }

    public:
        virtual core::Status::Period period() const {
            return STATUS_PERIOD;
        }

        virtual core::Status::Device device() const {
            return core::Status::AccessDoor;
        }

        virtual const char *descrition() const {
            return &Door[0];
        }
    };

    /**
     * @brief The Request class
     */
    class Request: public ble::Request
    {
    public:
        Request() {

        }

        virtual ~Request() {

        }

        virtual Message::Command command() const {
            return Message::AccessDoor;
        }

    private:
        virtual int parseRequest(const uint8_t *details, uint8_t size) {
            return 0;
        }
    };

    /**
     * @brief The Reply class
     */
    class Reply: public ble::Reply
    {
    public:
        Reply(const Request &request):
            ble::Reply(request) {

        }

        virtual ~Reply() {

        }

        virtual uint8_t _size() const {
            return 0;
        }

        virtual void fill(const ble::Request &request,
                          uint8_t *details) const {
        }

        virtual const char *descrition() const {
            return &Open[0];
        }
    };

    /**
     * @brief The Command class
     */
    class Command: public core::Command
    {
    public:
        Command(Private &app, ble::System &system):
            _app(app),
            _system(system) {

        }

        virtual ~Command() {

        }

        virtual void execute() {
            _app.open(_system);
        }

        inline const uint8_t *address() const {
            return _system.address();
        }

    private:
        Private &_app;
        ble::System &_system;
    };

    /**
     * @brief Private
     */
    Private():
        _led(LED_PIN),
        _door(RELAY_PIN),
        _request(NULL),
        _reply(NULL),
        _command(NULL) {
        _led = OFF;
        _door = OFF;
    }

    virtual ~Private() {

    }


    void open(System &system) {
        _led = ON;
        _door = ON;
        _timeout.attach(Callback<void()>(this, &Private::close), 5.0f);
        System::Id id;
        system.advertise(*_reply, 7, id);
    }

    void close() {
        _led = OFF;
        _door = OFF;
    }

    void received(System::Rssi rssi,
                  const uint8_t *address,
                  const uint8_t *details,
                  uint8_t length) {

        PRINTF("application* received from: %02X:%02X:%02X:%02X:%02X:%02X\r\n",
               address[0],
               address[1],
               address[2],
               address[3],
               address[4],
               address[5]);

        int error = _request->parse(details, length);
        if (error != 0) {
            PRINTF("*application* received parse request"
                   " failed with error: %d.\r\n",
                   error);
            return;
        }

        error = memcmp(_command->address(), _request->address(), 6);

        if(error != 0) {
            PRINTF("*application* received target not matched.\r\n",
                   error);
            PRINTF("                       target address: ");
            for(uint8_t index = 0; index < 5; index++)
                PRINTF("%02X:", address[index]);
            PRINTF("%02X\r\n", address[5]);
            return;
        }

        PRINTF("Open door\r\n");
        core::Application::instance()->schedule(_command);
    }

    void advertised(const System::Id &id, bool rejected) {

    }


    int initialize(ble::System &system) {
        ble::System::Received received(this, &Private::received);
        ble::System::Advertised advertised(this, &Private::advertised);
        int error = system.start(180, received, 128, advertised);
        if (error != BLE_ERROR_NONE) {
            return error;
        }

        _request = new Request;
        _reply = new Reply(*_request);
        _command = new Command(*this, system);
        return 0;
    }


    void release(ble::System &system) {
        system.stop();
        delete _reply;
        _reply = NULL;
        delete _request;
        _request = NULL;
        delete _command;
        _command = NULL;
    }


private:
    DigitalOut _led;
    DigitalOut _door;
    Timeout _timeout;
    Request *_request;
    Reply *_reply;
    Command *_command;
};

/**
 * @brief Application::Application
 */
Application::Application():
    _private(new Private)
{

}

Application::~Application()
{
    delete _private;
}

int Application::initialize(ble::System &system)
{

    return _private->initialize(system);
}


void Application::release(ble::System &system)
{
    _private->release(system);
}

Application &Application::instance()
{
    static Application app;
    return app;
}

core::Status &Application::status()
{
    static Private::Status _status;
    return _status;
}

} // namespace iot
