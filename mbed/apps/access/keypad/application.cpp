#include "application.h"
#include <ble/blesystem.h>
#include <ble/bleevent.h>
#include <core/corestatus.h>
#include <devices/devicessettings.h>
#include <devices/devicesprotocol.h>
#include <devices/devicesserial.h>
#include <radio/radiocontrol.h>
#include <radio/radioschedule.h>
#include <utils/utilsprint.h>
#include <utils/utilsendian.h>
#include <BLE.h>
#include <stddef.h>

using namespace iot;
using namespace iot::ble;
using namespace iot::radio;
using namespace iot::utils;

#define STATUS_PERIOD 5.0f
static const char keyboard[] = "KBD";
static const char unlock[] = "ULK";

extern Serial pc;

namespace iot {
/**
 * @brief The Status class
 */
class Status: public core::Status
{
public:
    Status(const core::Status::Resets &resets = 0):
        core::Status(resets) {

    }

    virtual ~Status() {

    }

public:
    virtual core::Status::Period period() const {
        return STATUS_PERIOD;
    }

    virtual core::Status::Device device() const {
        return core::Status::AccessKeypad;
    }

    virtual const char *descrition() const {
        return &keyboard[0];
    }
};

/**
 * @brief The Application::Private class
 */
class Application::Private
{
public:
    class Event: public ble::Event
    {
    public:
        typedef uint8_t Length;
        Event():
            _password(NULL),
            _length(0) {

        }

        virtual ~Event() {

        }

        virtual ble::Message::Command command() const {
            return ble::Message::AccessKeypad;
        }

        void setPassword(const uint8_t *password, Length length) {
            _password = password;
            _length = length;

        }

        virtual const char *descrition() const {
            return &unlock[0];
        }

    private:

        virtual uint8_t _size() const {
            return _length;
        }

        virtual void _fill(uint8_t *details) const {
            memcpy(details, _password, _length);
        }

    private:
        const uint8_t *_password;
        Length _length;
    };


    Private(ble::System &system):
        _system(system),
        _protocol('B',23) {
    }

    virtual ~Private() {

    }

    void received(const uint8_t *password, const devices::Serial::Size &size) {
        _event.setPassword(password, size);
        System::Id id;
        _system.advertise(_event, 7, id);
        PRINTF("*application* advertised\r\n");
    }

    void advertised(const System::Id &id, bool rejected) {

    }


    int initialize() {

        PRINTF("*application* initializing..");

        System::Advertised advertised(this, &Private::advertised);
        int error = _system.start(512, advertised);

        if (error != 0) {
            PRINTF("with error: %d\r\n", error);
            return error;
        }

        PRINTF("successed\r\n");

        devices::Serial::Received received(this, &Private::received);
        error = _serial.open(pc, _protocol, received);
        if (error != 0) {
            PRINTF("*application* serial open failed with error: %d\r\n",
                   error);
            _system.stop();
            return error;
        }
        return 0;
    }

    void release() {
        _serial.close();
        _system.stop();
    }

private:
    iot::ble::System &_system;
    iot::devices::keypad::Protocol _protocol;
    iot::devices::Serial _serial;
    Event _event;
};

/**
 * @brief Application::Application
 */
Application::Application()
{

}

Application::~Application()
{
}

int Application::initialize(ble::System &system)
{
    if (_private != NULL)
        return -1;
    _private = new Private(system);
    int error = _private->initialize();
    if (error != 0) {
        delete _private;
        _private = NULL;
        return error;
    }
    return 0;
}


void Application::release(ble::System &system)
{
    if (_private == NULL)
        return;
    _private->release();
    delete _private;
    _private = NULL;
}

core::Status &Application::status()
{
    static Status _status;
    return _status;
}

Application &Application::instance()
{
    static Application app;
    return app;
}
} // namespace iot
