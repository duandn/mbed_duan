#include "application.h"
#include <ble/bleevent.h>
#include <ble/blesystem.h>
#include <ble/gap/blegappacket.h>
#include <core/corestatus.h>
#include <devices/devicessettings.h>
#include <devices/devicesprotocol.h>
#include <devices/devicesserial.h>
#include <radio/radiocontrol.h>
#include <radio/radioschedule.h>
#include <utils/utilsendian.h>
#include <utils/utilsprint.h>
#include <stddef.h>
extern Serial pc;
extern Timeout Buzzer_timerout;
#define ON  1
#define OFF 0
#define COMMAND_CARD_DETECTED_PN7462 0x03
#define COMMAND_CARD_REMOVED_PN7462 0x04
#define LED_GREEN_PIN   p10
#define LED_RED_PIN     p9
#define BUZZER_PIN      p8

#define STATUS_PERIOD 5.0f
static const char reader[] = "RDR";
static const char nfcId[] = "NFCI";
using namespace iot;
using namespace iot::radio;
using namespace iot::ble;
using namespace iot::ble::gap;

namespace iot {


/**
 * @brief The Application::Private class
 */
class Application::Private
{
public:
    /**
     * @brief The Status class
     */
    class Status: public core::Status
    {
    public:
        Status(const core::Status::Resets &resets = 0):
            core::Status(resets) {

        }

        virtual ~Status() {

        }

    public:
        virtual core::Status::Period period() const {
            return STATUS_PERIOD;
        }

        virtual core::Status::Device device() const {
            return core::Status::AccessReader;
        }

        virtual const char *descrition() const {
            return &reader[0];
        }
    };

    class Event: public ble::Event
    {
    public:
        Event():
            _nfcId(NULL) {

        }

        virtual ~Event() {

        }

        virtual ble::Message::Command command() const {
            return ble::Message::AccessReader;
        }

        void setNfcId(const uint8_t *nfcId) {
            _nfcId = nfcId;
        }

        virtual const char *descrition() const {
            return &nfcId[0];
        }

    private:

        virtual uint8_t _size() const {
            return 7;
        }

        virtual void _fill(uint8_t *details) const {
            memcpy(details, _nfcId, 7);
        }
    private:
        const uint8_t *_nfcId;
    };

    Private(ble::System &system):
        _system(system),
        _greenLed(LED_GREEN_PIN),
        _redLed(LED_RED_PIN),
        _buzzer(BUZZER_PIN),
        _id(0xFFFF) {
        _greenLed = OFF;
        _redLed = ON;
        _buzzer = OFF;
    }

    virtual ~Private() {

    }

    void buzzerOff(){
      _buzzer = OFF;
    }

    void cardDetected(){
        _greenLed = ON;
        _redLed = OFF;
    }

    void cardRemoved(){
        _greenLed = OFF;
        _redLed = ON;
    }

    void advertised(const System::Id &id, bool rejected) {
        if (id != _id)
            return;

        _buzzer = ON;
        _timeout.attach(Callback<void()>(this,&Private::buzzerOff),0.5);
    }

    void received(const uint8_t *id, const devices::Serial::Size &size) {

        if( COMMAND_CARD_DETECTED_PN7462 == id[0]) {
            if (size != 9)
                return;

            _event.setNfcId(id + 2);
            int error = _system.advertise(_event, 7, _id);
            if (error != 0)
                return;
            cardDetected();
        } else if(COMMAND_CARD_REMOVED_PN7462 == id[0]){
            cardRemoved();
        }
    }

    int initialize() {

        PRINTF("*application* advertise initializing..");

        System::Advertised advertised(this, &Private::advertised);
        int error = _system.start(512, advertised);

        if (error != 0) {
            PRINTF("failed with error: %d\r\n", error);
            return error;
        }

        PRINTF("successed\r\n");

        PRINTF("*application* serial initializing...");
        devices::Serial::Received received(this, &Private::received);
        error = _serial.open(pc, _protocol, received);
        if (error != 0) {
            _system.stop();
            PRINTF("with error: %d\r\n", error);
            return error;
        }
        PRINTF("successed\r\n");
        return 0;
    }

    void release() {
        _serial.close();
        _system.stop();
    }
private:
    iot::ble::System &_system;
    devices::slip::Protocol _protocol;
    devices::Serial _serial;
    DigitalOut  _greenLed;
    DigitalOut  _redLed;
    DigitalOut  _buzzer;
    Timeout _timeout;
    Event _event;
    System::Id _id;
};
/**
 * @brief Application::Application
 */
Application::Application()
{

}

Application::~Application()
{
}

int Application::initialize(ble::System &system)
{
    if (_private != NULL)
        return -1;
    _private = new Private(system);
    int error = _private->initialize();
    if (error != 0) {
        delete _private;
        _private = NULL;
        return error;
    }
    return 0;
}


void Application::release(ble::System &system)
{
    if (_private == NULL)
        return;
    _private->release();
    delete _private;
    _private = NULL;
}

core::Status &Application::status()
{
    static Private::Status _status;
    return _status;
}

Application &Application::instance()
{
    static Application app;
    return app;
}


} // namespace iot
