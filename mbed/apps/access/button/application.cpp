#include "application.h"
#include <ble/blesystem.h>
#include <ble/blecommand.h>
#include <core/corestatus.h>
#include <devices/devicesbutton.h>
#include <devices/devicessettings.h>
#include <radio/radiocontrol.h>
#include <radio/radioschedule.h>
#include <utils/utilsprint.h>
#include <utils/utilsendian.h>
#include <stddef.h>
#include <BLE.h>

#define BUTTON_PIN p23

using namespace iot;
using namespace iot::devices;
using namespace iot::devices::serial;
using namespace iot::radio;
using namespace iot::ble;
using namespace iot::utils;

#define STATUS_PERIOD 0.0f
#define WATCHDOG_TIMEOUT 20.0f

namespace iot {

/**
 * @brief The Application::Private class
 */
class Application::Private
{
public:
    /**
     * @brief The Status class
     */
    class Status: public core::Status
    {
    public:

        Status(const core::Status::Resets &resets = 0):
            core::Status(resets) {

        }

        virtual ~Status() {

        }

    public:
        virtual core::Status::Period period() const {
            return STATUS_PERIOD;
        }

        virtual core::Status::Device device() const {
            return core::Status::AccessButton;
        }

        virtual const char *descrition() const {
            static const char _descrition[] = "DBT";
            return &_descrition[0];
        }
    };

    /**
     * @brief The Command class
     */
    class Command: public ble::Command
    {
    public:

        Command(const uint8_t *target):
            ble::Command(target) {

        }

        virtual ~Command() {

        }

        virtual ble::Message::Command command() const {
            return ble::Message::AccessDoor;
        }

        virtual const char *descrition() const {
            static const char _descrition[] = "DOP";
            return &_descrition[0];
        }

    private:

        virtual uint8_t _commandSize() const {
            return 0;
        }

        virtual void _commandFill(uint8_t */*details*/) const {

        }
    };


    Private(ble::System &system):
        _system(system) {
    }

    ~Private() {

    }

    void advertised(const Controller::Id &id, bool rejected) {

    }


    void down() {
        PRINTF("*application* down advertising..");
        System::Id id;
        int error = _system.advertise(*_command, 7, id);
        if (error) {
            PRINTF("failed with error: %d\r\n", error);
        }
        PRINTF("successed\r\n");
    }

    void up() {

    }

    int initialize() {
        PRINTF("*application* advertise initializing..");

        System::Advertised advertised(this, &Private::advertised);
        int error = _system.start(512, advertised);

        if (error != 0) {
            PRINTF("failed with error: %d\r\n", error);
            return error;
        }

        PRINTF("successed\r\n");
        PRINTF("*application* button initializing..");

        error = _button.initialize(interrupt::Settings(BUTTON_PIN),
                                   Button::Down(this, &Private::down),
                                   Button::Down(this, &Private::up));
        if (error != 0) {
            PRINTF("failed with error: %d\r\n", error);
            return error;
        }
        PRINTF("successed\r\n");

//        uint8_t target[] = {0x3B, 0x6D, 0xCF, 0x40, 0x79, 0xF4};
        uint8_t target[] = {0x60, 0x2A, 0x29, 0x70, 0xA3, 0xEB};

        _command = new Command(target);
        return 0;
    }

    void release() {
        _system.stop();
        delete _command;
    }

private:
    iot::ble::System &_system;
    devices::Button _button;
    Command *_command;
};

/**
 * @brief Application::Application
 */
Application::Application()
{

}

Application::~Application()
{
}

int Application::initialize(ble::System &system)
{
    if (_private != NULL)
        return -1;
    _private = new Private(system);
    int error = _private->initialize();
    if (error != 0) {
        delete _private;
        _private = NULL;
        return error;
    }
    return 0;
}


void Application::release(ble::System &system)
{
    if (_private == NULL)
        return;

    _private->release();
    delete _private;
    _private = NULL;
}

float Application::watchdog() const
{
    return WATCHDOG_TIMEOUT;
}

core::Status &Application::status()
{
    static Private::Status _status;
    return _status;
}

Application &Application::instance()
{
    static Application app;
    return app;
}
} // namespace iot
