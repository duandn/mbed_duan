#include <utils/utilsprint.h>
#include <mbed.h>
#include "application.h"
#define SERIAL_TX p10
#define SERIAL_RX p11
#ifdef DEBUG_UARTPRINT
Serial pc(SERIAL_TX, SERIAL_RX);
#endif
int main()
{
#ifdef DEBUG_UARTPRINT
    pc.baud(115200);
#endif
    PRINTF("*main* started\r\n");
    iot::Application &app = iot::Application::instance();
    return app.exec();
}
