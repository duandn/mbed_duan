#ifndef APPLICATION_H
#define APPLICATION_H
#include <ble/bleapplication.h>
namespace iot {
class Application: public ble::Application
{
public:
    Application();

    virtual ~Application();

private:

    virtual int initialize(ble::System &system);

    virtual void release(ble::System &system);

    virtual core::Status &status();

private:
    class Private;
    Private *_private;

public:
    static Application &instance();
};

} // namespace iot
#endif // APPLICATION_H
