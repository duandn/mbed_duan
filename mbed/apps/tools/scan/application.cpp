#include "application.h"
#include <ble/bledefs.h>
#include <ble/blerequest.h>
#include <ble/blesystem.h>
#include <core/corecommand.h>
#include <core/corestatus.h>
#include <devices/devicessettings.h>
#include <devices/devicesprotocol.h>
#include <devices/devicesserial.h>
#include <radio/radiocontrol.h>
#include <radio/radioschedule.h>
#include <utils/utilsprint.h>
#include <utils/utilsendian.h>
#include <BLE.h>
#include <vector>
#include <stddef.h>
#include <std/stdvector.h>
#include <utils/utilswatchdog.h>
#define LIGHT_LAMP_PIN p7

extern Serial pc;
using namespace iot;
using namespace iot::ble;
using namespace iot::devices;
using namespace iot::utils;
using namespace iot::radio;
#define STATUS_PERIOD 1.0f
static const char scan[] = "TSC";
static const uint8_t _address[] = {0xF8, 0xAB, 0x79, 0x6A, 0xBA, 0x18};
using namespace iot::utils::endian;
namespace iot {
/**
 * @brief The Status class
 */
class Status: public core::Status
{
public:
    Status(const core::Status::Resets &resets = 0):
        core::Status(resets) {

    }

    virtual ~Status() {

    }

public:
    virtual core::Status::Period period() const {
        return STATUS_PERIOD;
    }

    virtual core::Status::Device device() const {
        return core::Status::Scanner;
    }

    virtual const char *descrition() const {
        return  &scan[0];
    }
};

/**
 * @brief The Application::Private class
 */
class Application::Private
{
public:

    class Message: public ble::Message
    {
    public:

        Message() {
        }

        virtual ~Message() {

        }

        int parse(const uint8_t *address,
                  const uint8_t *payload,
                  uint8_t size) {
            if (memcmp(&_address[0], address, BLE_ADDRESS_SIZE) != 0) {
//                PRINTF("*parse* address not match\r\n");
                return -8;
            }

            if (size < BLE_MESSAGE_HEADER_SIZE + BLE_ADDRESS_SIZE) {
//                PRINTF("*parse* paload too small\r\n");
                return -1;
            }

            int error;

            if (Message::isEvent(payload, size, error) != true) {
//                PRINTF("*parse* type not match\r\n");
                return -2;
            }

            if (error != 0)
                return error;

            memcpy(&_sequence, payload, sizeof(Message::Sequence));
            _sequence = toLittle<Message::Sequence>(_sequence);

            Message::Command command;
            memcpy(&command,
                   payload + sizeof(Message::Sequence),
                   sizeof(Message::Command));

            command = toLittle<Message::Sequence>(command);

            if (command != this->command()) {
                PRINTF("*parse* command: 0x%04X not match\r\n", command);
                return -3;
            }

            return 0;
        }

        inline Message::Sequence sequence() const {
            return _sequence;
        }

        virtual ble::Message::Command command() const {
            return ble::Message::Status;
        }

    private:
        Message::Sequence _sequence;
    };


    Private():
        _sequence(0),
        _count(0),
        _misses(0) {
    }

    virtual ~Private() {

    }

    void sent(const devices::Serial::Id &id, bool rejected) {
    }

    void received(System::Rssi rssi,
                  const uint8_t *address,
                  const uint8_t *details,
                  uint8_t size) {

        PRINTF("*application received* address:"
               " %02X:%02X:%02X:%02X:%02X:%02X\r\n",
               address[0],
               address[1],
               address[2],
               address[3],
               address[4],
               address[5]);

        int error = _message.parse(address, details, size);

        if (error != 0) {
            return;
        }
        if (_sequence == _message.sequence())
            return;

        if (_sequence == 0) {
            _sequence = _message.sequence();
        } else if (_message.sequence() > _sequence + 1) {
            _misses += 1;
            PRINTF("*application* miss: %d\r\n", _misses);
        }

        _count += 1;
        PRINTF("*application* count: %d prev sequence: %d,"
               " sequence: %d, miss: %d\r\n",
               _count,
               _sequence,
               _message.sequence(),
               _misses);
        _sequence = _message.sequence();
    }

    void advertised(const System::Id &id, bool rejected) {
    }

    int initialize(ble::System &system) {
        PRINTF("*application* initializing starting...");

        ble::System::Received received(this, &Private::received);
        ble::System::Advertised advertised(this, &Private::advertised);

        int error = system.start(180, received, 128, advertised);

        if (error != BLE_ERROR_NONE) {
            PRINTF("failed with error: %d\r\n", error);
            return error;
        }

        PRINTF("successed\r\n");
        return 0;
    }

    void release(ble::System &system) {
        system.stop();
    }

private:
    ble::Message::Sequence _sequence;
    Message _message;
    typedef uint32_t Count;
    Count _count;
    Count _misses;
};

/**
 * @brief Application::Application
 */
Application::Application():
    _private(new Private)
{

}

Application::~Application()
{
    delete _private;
}

int Application::initialize(ble::System &system)
{
    return _private->initialize(system);
}


void Application::release(ble::System &system)
{
    _private->release(system);
}

core::Status &Application::status()
{
    static Status _status;
    return _status;
}

Application &Application::instance()
{
    static Application app;
    return app;
}

} // namespace iot
