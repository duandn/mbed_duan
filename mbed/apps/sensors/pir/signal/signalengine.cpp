/*
 * detect.cpp
 *
 *  Created on: Mar 8, 2016
 *      Author: trungbq
 */

#include <signal/signalengine.h>
#include <signal/signalprocess.h>
#include <signal/signalrecord.h>
#include <signal/signalsample.h>
#include <utils/utilsprint.h>

namespace iot {
namespace signal {

class Engine::Private
{
public:
    Private(Recorder &recorder,
            Processor &processor,
            const Engine::Evented &evented):
        _recorder(recorder),
        _processor(processor),
        _evented(evented) {

    }

    virtual ~Private() {

    }

public:
    int start(unsigned int rate) {
        int error = _processor.start(rate);
        if (error != 0) {
            PRINTF("Start processor failed with error: %d\r\n", error);
            return error;
        }
        error = _recorder.start(rate);
        if (error != 0) {
            _processor.stop();
            PRINTF("Start recorder failed with error: %d\r\n", error);
            return error;
        }
        return 0;
    }

    int update() {
        return _recorder.record(_sample);
    }


    void processed(const Event &event) {
        _evented.call(event);
    }

    int process() {
        return _processor.process(_sample,
                                  Processor::Processed(this,
                                                       &Private::processed));
    }
    void stop() {
        _recorder.stop();
        _processor.stop();
    }

private:
    Recorder &_recorder;
    Processor &_processor;
    Engine::Evented _evented;
    Sample _sample;
};



/**
 * @brief Engine::Engine
 */
Engine::Engine():
    _private(NULL)
{
}

Engine::~Engine()
{
}


int Engine::start(unsigned int rate,
                  Recorder &recorder,
                  Processor &processor,
                  const Evented &evented)
{
    if (_private != NULL)
        return false;

    _private = new Private(recorder, processor, evented);

    int error = _private->start(rate);
    if (error == 0)
        return 0;

    delete _private;
    _private = NULL;
    return error;
}

int Engine::update()
{
    return _private->update();
}

int Engine::process()
{
    return _private->process();
}

void Engine::stop()
{
    if (_private == NULL)
        return;
    _private->stop();
    delete _private;
    _private = NULL;
}

} /* namespace Signal */
} /* namespace VNG */
