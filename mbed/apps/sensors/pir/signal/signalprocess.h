#ifndef SIGNALPROCESS_H
#define SIGNALPROCESS_H
#include <mbed.h>

namespace iot {
namespace signal {

class Sample;
class Event;
/**
 *  forward declare the class DetectPrivate
 *
 **/
class Processor
{
public:
    typedef Callback<void(const Event &event)> Processed;

    Processor();

    virtual ~Processor();    
public:
    virtual int start(int sampleRate) = 0;

    virtual void stop() = 0;

    int process(const Sample &sample, const Processed &processed);

private:
    virtual int preprocess(const Sample &sample, Sample &preprocessed) = 0;
    virtual int postprocess(const Sample &preprocessed, const Processed &processed) = 0;
};


} /* namespace signal */
} /* namespace iot */
#endif // SIGNALPROCESS_H
