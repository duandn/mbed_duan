/*
 * sample.h
 *
 *  Created on: Mar 9, 2016
 *      Author: trungbq
 */

#ifndef SIGNALSAMPLE_H
#define SIGNALSAMPLE_H
#include <std/stdvector.h>
namespace iot {
namespace signal {

/**
 *
 *
 */
class Sample
{
public:
    typedef unsigned long Time;
    typedef float Value;

    Sample();
    Sample(const Time &time, const Value &value);
    Sample(const Sample &sample);
    virtual ~Sample();

    inline void setTime(const Time &time) {
        _time = time;
    }

    inline const Time &time() const {
        return _time;
    }

    inline Value &value() {
        return _value;
    }

    inline const Value &value() const {
        return _value;
    }

    inline void setValue(const Value &value) {
        _value = value;
    }

    inline Sample &operator = (const Sample &sample) {
        _time = sample.time();
        _value = sample.value();
        return *this;
    }
private:
    Time _time;
    Value _value;
};

} // namespace signal
} // namespace iot

#endif // SIGNALSAMPLE_H
