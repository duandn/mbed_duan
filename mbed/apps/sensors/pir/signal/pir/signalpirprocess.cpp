/*
 * soundprocess.cpp
 *
 *  Created on: Mar 8, 2016
 *      Author: trungbq
 */

#include "../signalsample.h"
#include "signalpirprocess.h"
namespace iot {
namespace signal {
namespace pir {

class Processor::Private
{
public:
    Private(const lowpass::Filter::Alpha &lowpass,
            const hipass::Filter::Alpha &hipass):
        _lowpass(lowpass),
        _hipass(hipass) {

    }
    virtual ~Private() {

    }

    int preprocess(const Sample &sample, Sample &preprocessed) {
        Sample lowpassed;
        int error = _lowpass.apply(sample, lowpassed);
        if (error != 0)
            return error;

        return _hipass.apply(lowpassed, preprocessed);
    }
private:
    lowpass::Filter _lowpass;
    lowpass::Filter _hipass;
};

Processor::Processor(const lowpass::Filter::Alpha &lowpass,
                     const hipass::Filter::Alpha &hipass):
    _private(new Private(lowpass, hipass))
{

}

Processor::~Processor() {

}

int Processor::preprocess(const iot::signal::Sample &sample,
                          iot::signal::Sample &preprocessed)
{
	preprocessed = sample;
	return 0;
//    return _private->preprocess(sample, preprocessed);
}

int Processor::postprocess(const iot::signal::Sample &sample,
                           const Processed &processed)
{
    return processPir((const Sample &)sample, processed);
}

} /* namespace PIR */
} /* namespace Signal */
} /* namespace VNG */
