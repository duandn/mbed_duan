/*
 * soundrecord.h
 *
 *  Created on: Mar 8, 2016
 *      Author: trungbq
 */

#ifndef SIGNAL_PIR_SIGNALPIRRECORD_H_
#define SIGNAL_PIR_SIGNALPIRRECORD_H_
#include <mbed.h>
#include "../signalrecord.h"
namespace iot {
namespace signal {
namespace pir {

class Recorder: public signal::Recorder {
public:
    Recorder(PinName signal, PinName power);

	virtual ~Recorder();

    virtual int start(unsigned int rate);

    virtual int record(signal::Sample &sample);

    virtual void stop();
private:
    class Private;
    Private *_private;
};

} /* namespace PIR */
} /* namespace Signal */
} /* namespace VNG */

#endif /* SIGNAL_PIR_SIGNALPIRRECORD_H_ */
