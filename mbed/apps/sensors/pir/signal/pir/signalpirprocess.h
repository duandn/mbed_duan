/*
 * soundrocess.h
 *
 *  Created on: Mar 8, 2016
 *      Author: trungbq
 */

#ifndef SIGNAL_SOUND_SOUNDROCESS_H_
#define SIGNAL_SOUND_SOUNDROCESS_H_

#include "../signalfilter.h"
#include "../signalprocess.h"
#include "signalpirrecord.h"

namespace iot {
namespace signal {
namespace pir {

class Processor: public signal::Processor {
public:
    Processor(const lowpass::Filter::Alpha &lowpass,
              const hipass::Filter::Alpha &hipass);
	virtual ~Processor();
private:
    virtual int preprocess(const signal::Sample &sample,
                           signal::Sample &preprocessed);
    virtual int postprocess(const signal::Sample &preprocessed,
                            const signal::Processor::Processed &processed);
private:
    virtual int processPir(const signal::Sample &sample,
                           const signal::Processor::Processed &processed) = 0;
private:
    class Private;
    Private *_private;
};

} /* namespace PIR */
} /* namespace Signal */
} /* namespace VNG */

#endif /* SIGNAL_SOUND_SOUNDROCESS_H_ */
