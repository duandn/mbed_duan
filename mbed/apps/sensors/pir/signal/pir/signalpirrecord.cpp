/*
 * soundrecord.cpp
 *
 *  Created on: Mar 8, 2016
 *      Author: trungbq
 */

#include <mbed.h>
#include <signal/signalsample.h>
#include <signal/pir/signalpirrecord.h>
#include <utils/utilsprint.h>
namespace iot {
namespace signal {
namespace pir {

/**
 * @brief The Recorder::Private class
 */
class Recorder::Private
{
public:
    Private(PinName signal, PinName power):
        _signalPin(signal),
        _powerPin(power),
        _time(0),
        _signal(NULL),
        _power(NULL) {

    }


    int start() {
        _signal = new AnalogIn(_signalPin);
        _power = new DigitalOut(_powerPin);
        _power->write(1);
        return 0;
    }

    int record(Sample &sample) {

        float value = _signal->read();

        sample.setValue(value);
//        PRINTF("*signal pir record* value: %1.3f\r\n", value);
    	sample.setTime(_time);
        _time += 1;
        return 0;
    }

    void stop() {
        delete _signal;
        _signal = NULL;
        *_power = 0;
        delete _power;
        _power = NULL;
    }

private:
    PinName _signalPin;
    PinName _powerPin;
    Sample::Time _time;
    AnalogIn *_signal;
    DigitalOut *_power;
};

/**
 * @brief Recorder::Recorder
 * @param pin
 */
Recorder::Recorder(PinName signal, PinName power):
    _private(new Private(signal, power))
{
}

Recorder::~Recorder()
{
    delete _private;
}



int Recorder::start(unsigned int /*sampleRate*/)
{
    return _private->start();
}

int Recorder::record(signal::Sample &sample)
{
    return _private->record(sample);
}

void Recorder::stop()
{
    _private->stop();
}


} /* namespace Sound */
} /* namespace Signal */
} /* namespace VNG */
