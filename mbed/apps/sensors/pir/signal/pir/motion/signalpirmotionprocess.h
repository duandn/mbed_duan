/*
 * GlassProcess.h
 *
 *  Created on: Mar 8, 2016
 *      Author: trungbq
 */

#ifndef SIGNALPIRMOTIONPROCESS_H
#define SIGNALPIRMOTIONPROCESS_H
#include <signal/pir/signalpirprocess.h>
namespace iot {
namespace signal {
namespace pir {
namespace motion {
class Processor: public pir::Processor
{
public:
    Processor(const lowpass::Filter::Alpha &lowpass,
              const lowpass::Filter::Alpha &hipass,
              float adaptTime,
              float beginTime,
              float endTime);
    virtual ~Processor();
public:
    virtual int start(int sampleRate);
    virtual void stop();
private:
    virtual int processPir(const signal::Sample &sample,
                           const signal::Processor::Processed &processed);
private:
    class Private;
    Private *_private;
};

} // namespace motion
} // namespace pir
} // namespace signal
} // namespace iot

#endif // SIGNALPIRMOTIONPROCESS_H
