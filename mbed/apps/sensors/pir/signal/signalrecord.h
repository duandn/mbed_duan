/*
 * record.h
 *
 *  Created on: Mar 8, 2016
 *      Author: trungbq
 */

#ifndef SIGNALRECORD_H
#define SIGNALRECORD_H
#include <mbed.h>
namespace iot {
namespace signal {

class Sample;
class Recorder
{
public:
    typedef Callback<void()> Started;

	virtual ~Recorder();

    virtual int start(unsigned int rate) = 0;

    virtual int record(Sample &sample) = 0;

    virtual void stop() = 0;
};

} /* namespace Signal */
} /* namespace VNG */

#endif // SIGNALRECORD_H
