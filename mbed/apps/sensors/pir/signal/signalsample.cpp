/*
 * sample.cpp
 *
 *  Created on: Mar 9, 2016
 *      Author: trungbq
 */

#include "signalsample.h"

namespace iot {
namespace signal {

/**
 *
 *
 */

Sample::Sample():
    _time(0),
    _value(0)
{

}

Sample::Sample(const Time &time, const Value &value):
    _time(time),
    _value(value)
{

}

Sample::Sample(const Sample &sample):
    _time(sample.time()),
    _value(sample.value())
{

}

Sample::~Sample()
{

}


} /* namespace Signal */
} /* namespace VNG */
