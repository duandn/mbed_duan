#include "application.h"
#include <ble/bleevent.h>
#include <ble/blesystem.h>
#include <core/corecommand.h>
#include <core/corestatus.h>
#include <devices/devicessensor.h>
#include <devices/devicessettings.h>
#include <radio/radiocontrol.h>
#include <radio/radioschedule.h>
#include <utils/utilsprint.h>
#include <utils/utilsendian.h>
#include <BLE.h>
#include <stddef.h>

#define STATUS_PERIOD 4.0f

#define SENSOR_PERIOD 10.0f
#define SENSOR_VDD p10
#define SENSOR_CLOCK p9
#define SENSOR_DATA p8


using namespace iot;
using namespace iot::utils;
using namespace iot::devices;
using namespace iot::devices::sensor;

/**
 * @brief The Application::Private class
 */
namespace iot {
/**
 * @brief The Status class
 */
class Status: public core::Status
{
public:
    Status(const core::Status::Resets &resets = 0):
        core::Status(resets) {

    }

    virtual ~Status() {

    }

public:
    virtual core::Status::Period period() const {
        return STATUS_PERIOD;
    }

    virtual core::Status::Device device() const {
        return core::Status::ActivePir;
    }

    virtual const char *descrition() const {
        static const char _descrition[] = "SI7";
        return &_descrition[0];
    }
};

class Application::Private
{
public:
    /**
     * @brief The Temperature class
     */
    class Temperature: public ble::Event
    {
    public:
        typedef float Value;
        Temperature() {

        }

        virtual ~Temperature() {

        }

        inline void setValue(const Value &value) {
            _value = value;
        }

    public:

        virtual const char *descrition() const {
            static const char _descrition[] = "STM";
            return &_descrition[0];
        }


        virtual ble::Message::Command command() const {
            return ble::Message::TemperatureSensor;
        }

    private:

        virtual uint8_t _size() const {
            return sizeof(Value);
        }

        virtual void _fill(uint8_t *details) const {
            Value value = endian::toBig<Value>(_value);
            memcpy(details, &value, sizeof(Value));
        }

    private:
        Value _value;
    };

    /**
     * @brief The Temperature class
     */
    class Humidity: public ble::Event
    {
    public:
        typedef float Value;
        Humidity() {
        }

        virtual ~Humidity() {
        }

        inline void setValue(const Value &value) {
            _value = value;
        }

    public:

        virtual const char *descrition() const {
            static const char _descrition[] = "SHM";
            return &_descrition[0];
        }

        virtual ble::Message::Command command() const {
            return ble::Message::HumiditySensor;
        }

    private:

        virtual uint8_t _size() const {
            return sizeof(Value);
        }
        virtual void _fill(uint8_t *details) const {

            PRINTF("*humidity * _fill.....\r\n");
//            Value value = endian::littleToBig(_value);
            memcpy(details, &_value, sizeof(Value));


            for(int i = 0 ; i < sizeof(float) ; i ++){
                PRINTF("%x  ",details[i]);
            }
            PRINTF("\r\n");
        }

    private:
        Value _value;
    };





    Private(ble::System &system):
        _system(system),
        _sensor(NULL)
    {
    }

    ~Private() {

    }

    void advertised(const ble::System::Id &id, bool rejected) {

    }

    void evented(const sensor::Event &event) {

        PRINTF("*application*  evented.......................................\r\n");

        sensor::Event::Type type = event.type();
        if (type == sensor::Event::TypeStarted) {
            const started::Event &started = (const started::Event &) event;
            if (started.error() != 0)
                Application::instance().exit();
            return;
        }


        ble::System::Id id;

        if(event.type() == sensor::Event::TypeHumidity) {
            _humidity.setValue(((const humidity::Event &) event).value());
            PRINTF("*applicatin* TypeHumidity  advertising.. \r\n");
//            int error = _system.advertise(_humidity, 7, id);
//            if (error != 0) {
//                PRINTF("failed with error: %d\r\n", error);
//                return;
//            }

        } else if(event.type() == sensor::Event::TypeTemperature) {
            _temp.setValue(((const temperature::Event &) event).value());
            PRINTF("*applicatin* TypeHumidity  advertising.. \r\n");
//            int error = _system.advertise(_temp, 7, id);
//            if (error != 0) {
//                PRINTF("failed with error: %d\r\n", error);
//                return;
//            }
        }




        PRINTF("successed\r\n");
    }

    int initialize() {
        PRINTF("*applicatin* system initializing..");
        ble::System::Advertised advertised(this, &Private::advertised);
        int error = _system.start(1024, advertised);
        if (error != 0) {
            PRINTF("failed with error: %d\r\n", error);
            return error;
        }
        PRINTF("successed\r\n");

        _sensor = new si7021a20::Sensor(SENSOR_VDD,
                                        SENSOR_DATA,
                                        SENSOR_CLOCK,
                                        devices::I2C::Frequence400k,
                                        0x40 << 1,
                                        SENSOR_PERIOD);
        wait_ms(100);
        error = _sensor->start(Sensor::Evented(this, &Private::evented));
        if (error != 0) {
            PRINTF("*applications* sensor initialize failed with error: %d\r\n", error);
            delete _sensor;
            _sensor = NULL;
            return error;
        }

        PRINTF("*applications* sensor initialize successed\r\n");

        return 0;
    }

    void release() {
        _sensor->stop();
        delete _sensor;
        _sensor = NULL;
        _system.stop();
    }
private:
    ble::System &_system;
    devices::si7021a20::Sensor *_sensor;
    Temperature _temp;
    Humidity _humidity;
};


/**
 * @brief Application::Application
 */
Application::Application():
    _private(NULL)
{

}

Application::~Application()
{
}

float Application::watchdog() const
{
    return 0.0f;
}

int Application::initialize(ble::System &system)
{
    if (_private != NULL)
        return -1;

    _private = new Private(system);
    int error = _private->initialize();
    if (error != 0) {
        delete _private;
        _private = NULL;
        return error;
    }

    return 0;
}


void Application::release(ble::System &system)
{
    if (_private == NULL)
        return;
    _private->release();
    delete _private;
    _private = NULL;
}

core::Status &Application::status()
{
    static Status _status;
    return _status;
}

Application &Application::instance()
{
    static Application app;
    return app;
}

} // namespace iot
