#ifndef APPLICATION_H
#define APPLICATION_H
#include <core/coreapplication.h>
namespace iot {
class Application: public core::Application
{
public:
    Application();

    virtual ~Application();

private:

    virtual int initialize();

    virtual int systems() const;

    virtual iot::core::System *system(int system);

    virtual void release();
private:
    class Private;
    Private *_private;

public:
    static Application &instance();
};
} // namespace iot

#endif // APPLICATION_H
