#include "application.h"
#include <ble/gap/blegapevent.h>
#include <ble/gap/blegappacket.h>
#include <ble/blesystem.h>
#include <core/corecommand.h>
#include <devices/devicessensor.h>
#include <devices/devicessettings.h>
#include <radio/radiocontrol.h>
#include <radio/radioschedule.h>
#include <utils/utilsprint.h>
#include <utils/utilsendian.h>
#include <BLE.h>
#include <stddef.h>

#define ALALOG_SENSOR_PERIOD 0.025
#define ANALOG_SENSOR_PIN p2
#define ANALOG_SENSOR_WAKEUP_TIME 15

using namespace iot;
using namespace iot::utils;
using namespace iot::devices;
using namespace iot::devices::serial;
using namespace iot::radio;
using namespace iot::ble::gap;
/**
 * @brief The Application::Private class
 */
namespace iot {

class Application::Private
{
public:
    Private() {
    }

    ~Private() {

    }

    void advertised(const Controller::Id &id, bool rejected) {

    }

    void updated(const devices::Value &value) {
        if (value.type() != devices::Value::Analog)
            return;

        iot::ble::gap::Packet packet;

        uint8_t flags[] = {0x00};


        int error = packet.setField(Packet::TypeFlags, flags, sizeof(flags));
        if (error != 0) {
            PRINTF("*applications* updated set flags failed with error:"
                   " %d\r\n",
                   error);
            return;
        }
        uint8_t appearance[] = {0};
        error = packet.setField(Packet::TypeAppearance,
                                appearance,
                                sizeof(appearance));
        if (error != 0) {
            PRINTF("*applications* updated set appearance failed with error:"
                   " %d\r\n",
                   error);
            return;
        }

        uint8_t payload[sizeof(uint16_t) +
                        sizeof(struct iot::ble::gap::_Event) +
                        sizeof(devices::light::Value::Light)];

        CompanyId *companyId = (CompanyId *) &payload[0];
        *companyId = endian::littleToBig((uint16_t)0xFF0F);

        struct _Event *event;
        event = (struct _Event *) (&payload[0] +
                                                sizeof(uint16_t));
        event->commandId = endian::littleToBig((uint16_t)EVENT_PIR_SENSOR);
        event->gateway = 0;
        const devices::analog::Value &analogValue =
                (const devices::analog::Value &) value;
        const devices::light::Value::Light &analog = analogValue.analog();
        devices::light::Value::Light bigValue = endian::littleToBig(analog);
        const uint8_t *source = (const uint8_t *) &bigValue;
        uint8_t *target = event->payload;
        for(uint8_t index = 0;
            index < sizeof(devices::light::Value::Light);
            index++)
            target[index] = source[index];

        PRINTF("*applications* value: %3.2f\r\n", analog);

        error = packet.setField(Packet::TypeManufacturerSpecificData,
                                &payload[0],
                                sizeof(payload));
        if (error != 0) {
            PRINTF("*application* updated set manufacturer specific data"
                   "failed with error: %d\r\n",
                   error);
            return;
        }

        uint8_t name[] = "Analog XX";
        size_t length = sizeof(name) - 1;
        error = packet.setField(Packet::TypeCompleteLocalName, name, length);
        if (error != 0) {
            PRINTF("*application* updated set local name failed with error:"
                   " %d\r\n", error);
            return;
        }

        Controller &controller = Controller::instance();
        Controller::Id id;
        error = controller.advertise(packet.payload(), packet.length(), 3, id);
        if (error != 0) {
            PRINTF("*application* updated asvertise failed with error: %d\r\n",
                   error);
        }
    }

    int initialize() {

        PRINTF("*applications* initialize scheduler ...");

        Scheduler &scheduler = Scheduler::instance();
        int error = scheduler.initialize();
        if (error != 0) {
            PRINTF("with error: %d\r\n", error);
            return error;
        }
        PRINTF("successed\r\n");

        BLE &ble = _ble.ble();
        BLEProtocol::AddressType_t type;
        BLEProtocol::AddressBytes_t address;
        ble.gap().getAddress(&type, address);

        PRINTF("Address: %02X:%02X:%02X:%02X:%02X:%02X\r\n",
               address[0],
                address[1],
                address[2],
                address[3],
                address[4],
                address[5]);

        PRINTF("*applications* initialize controller ...");
        Controller &controller = Controller::instance();
        Controller::Advertised advertised(this, &Private::advertised);
        error = controller.initialize(address,
                                      Controller::All,
                                      1024,
                                      advertised);
        if (error != 0) {
            PRINTF("with error: %d\r\n", error);
            scheduler.release();
            return error;
        }
        PRINTF("successed\r\n");

        PRINTF("*applications* analog sensor initializing...");
        devices::analog::Settings settings(ANALOG_SENSOR_PIN);
        devices::Sensor::Evented updated(this, &Private::updated);
        error = _sensor.start(settings,
                              ANALOG_SENSOR_WAKEUP_TIME,
                              ALALOG_SENSOR_PERIOD,
                              updated);
        if (error != 0) {
            PRINTF("with error: %d\r\n", error);
            controller.release();
            scheduler.release();
            return error;
        }

        PRINTF("successed\r\n");

        return 0;
    }

    inline int subsystems() const {
        return 1;
    }

    inline iot::core::System *subsystem(int subsystem) {
        return subsystem == 0? &_ble: NULL;
    }

    void release() {
        Scheduler::instance().release();
        Controller::instance().release();
    }
private:
    iot::ble::System _ble;
    devices::analog::Sensor _sensor;
};
/**
 * @brief Application::Application
 */
Application::Application():
    _private(new Private)
{

}

Application::~Application()
{
    delete _private;
}

int Application::initialize()
{
    return _private->initialize();
}

int Application::systems() const
{
    return _private->subsystems();
}

iot::core::System *Application::system(int subsystem)
{
    return _private->subsystem(subsystem);
}


void Application::release()
{
    _private->release();
}

Application &Application::instance()
{
    static Application app;
    return app;
}

} // namespace iot
