#include "application.h"
#include <ble/blesystem.h>
#include <ble/bleevent.h>
#include <core/corecommand.h>
#include <core/corestatus.h>
#include <devices/devicessensor.h>
#include <devices/devicessettings.h>
#include <radio/radiocontrol.h>
#include <radio/radioschedule.h>
#include <utils/utilsprint.h>
#include <utils/utilsendian.h>
//#include <utils/utilsvdd.h>

#include <BLE.h>
#include <stddef.h>

#define LIGHT_SENSOR_PERIOD 3.0f
#define LIGHT_SENSOR_CLOCK p14
#define LIGHT_SENSOR_DATA p16
#define STATUS_PERIOD 20.0f

using namespace iot;
using namespace iot::utils::endian;
using namespace iot::devices;
using namespace iot::devices::serial;
using namespace iot::radio;
using namespace iot::ble;

static const char opt[] = "OPT3";
static const char slig[] = "SLIG";
/**
 * @brief The Application::Private class
 */
namespace iot {

/**
 * @brief The Application::Private class
 */
class Application::Private
{
public:

    /**
     * @brief The Status class
     */
    class Status: public core::Status
    {
    public:
        Status(const core::Status::Resets &resets = 0):
            core::Status(resets) {

        }

        virtual ~Status() {

        }

    public:
        virtual core::Status::Period period() const {
            return STATUS_PERIOD;
        }

        virtual core::Status::Device device() const {
            return core::Status::AccessDoor;
        }

        virtual const char *descrition() const {
            return &opt[0];
        }
    };

    /**
     * @brief The Event class
     */
    class Event: public ble::Event
    {
    public:
        Event():
            _light(0) {

        }

        inline void setLight(const light::Value::Light &light) {
            _light = light;
        }

        virtual ble::Message::Command command() const {
            return Message::LightSensor;
        }

        virtual const char *descrition() const {
            return &slig[0];
        }
    private:

        virtual uint8_t _size() const {
            return sizeof(light::Value::Light);
        }

        virtual void _fill(uint8_t *details) const {
            memcpy(details, &_light, sizeof(light::Value::Light));
        }

    private:
        light::Value::Light _light;
    };


    /**
     * @brief Private
     */
    Private(iot::ble::System &system):
        _system(system),
        _sensor(NULL) {
    }

    ~Private() {

    }

    void updated(const devices::Value &value) {
        PRINTF("*application* updated\r\n");

        if (value.type() != devices::Value::Light) {
            PRINTF("*application* type invalided\r\n");
            return;
        }
        const light::Value &light = (const light::Value &) value;
        _event.setLight(toBig<light::Value::Light>(light.light()));
        System::Id id;
        int error = _system.advertise(_event, 7, id);
        if (error != 0) {
            PRINTF("*applications* advertise failed with error: %d", error);
        }
    }

    void advertised(const Controller::Id &id, bool rejected) {

    }


    int initialize() {

        PRINTF("*application* advertise initializing..");

        System::Advertised advertised(this, &Private::advertised);
        int error = _system.start(512, advertised);

        if (error != 0) {
            PRINTF("failed with error: %d\r\n", error);
            return error;
        }

        PRINTF("successed\r\n");

        PRINTF("*applications* light sensor initializing...");

        i2c::Settings settings(LIGHT_SENSOR_DATA,
                               LIGHT_SENSOR_CLOCK,
                               100000,
                               0x44 << 1);

        _sensor = new opt3001::Sensor(settings, LIGHT_SENSOR_PERIOD);
        error = _sensor->start(Sensor::Evented(this, &Private::updated));
        if (error != 0) {
            PRINTF("with error: %d\r\n", error);
            delete _sensor;
            _sensor = NULL;
            _system.stop();
            return error;
        }

        PRINTF("successed\r\n");

        return 0;
    }

    inline int subsystems() const {
        return 1;
    }

    inline iot::core::System *subsystem(int subsystem) {
        return subsystem == 0? &_system: NULL;
    }

    void release() {
        _system.stop();
        delete _sensor;
        _sensor = NULL;
        _system.stop();
    }
private:
    iot::ble::System &_system;
    opt3001::Sensor *_sensor;
    Event _event;
};

/**
 * @brief Application::Application
 */
Application::Application():
    _private(NULL)
{

}

Application::~Application()
{
}

int Application::initialize(ble::System &system)
{
    PRINTF("*application* start\r\n");
    if (_private != NULL)
        return -1;
    _private = new Private(system);
    int error = _private->initialize();
    if (error != 0) {
        delete _private;
        _private = NULL;
        return error;
    }
    return 0;
}


void Application::release(ble::System &system)
{
    if (_private == NULL)
        return;
    _private->release();
    delete _private;
    _private = NULL;
}

core::Status &Application::status()
{
    static Private::Status _status;
    return _status;
}

Application &Application::instance()
{
    static Application app;
    return app;
}

} // namespace iot

