/*
 * filter.h
 *
 *  Created on: Mar 15, 2016
 *      Author: trungbq
 */

#ifndef SIGNAL_SIGNALFILTER_H_
#define SIGNAL_SIGNALFILTER_H_

namespace iot {
namespace signal {
class Sample;

/**
 *
 *
 */
class Filter
{
public:
    virtual ~Filter();
public:
    virtual int apply(const Sample &sample, Sample &filtered) = 0;
};

namespace lowpass {
/**
 *
 *
 */
class Filter: public signal::Filter
{
public:
    typedef float Alpha;

    Filter(const Alpha &alpha);

    virtual ~Filter();
public:

    virtual int apply(const Sample &sample, Sample &filtered);
private:
    class Private;
    Private *_private;
};

} // namespace lowpass



namespace hipass {
/**
 *
 *
 */
class Filter: public signal::Filter
{
public:
    typedef float Alpha;
    Filter(const Alpha &alpha);
    virtual ~Filter();
public:
    virtual int apply(const Sample &sample, Sample &filtered);
private:
    class Private;
    Private *_private;
};

} // namespace hipass
} // namespace signal
} // namespace iot

#endif /* SIGNAL_SIGNALFILTER_H_ */

