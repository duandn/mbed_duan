/*
 * event.cpp
 *
 *  Created on: Mar 9, 2016
 *      Author: trungbq
 */

#include <mbed.h>
#include "signalevent.h"

namespace iot {
namespace signal {

/**
 *
 *
 */
Event::Event(const long &id):
    _id(id)
{

}

Event::~Event()
{

}

/**
 *
 *
 */
BeginEvent::BeginEvent(const long &id):
    Event(id)
{

}

BeginEvent::~BeginEvent()
{

}

Event::Type BeginEvent::type() const
{
    return Event::Begin;
}

/**
 *
 *
 */
EndEvent::EndEvent(const long &id):
    Event(id)
{

}

EndEvent::~EndEvent()
{

}

Event::Type EndEvent::type() const
{
    return Event::End;
}

} /* namespace Signal */
} /* namespace VNG */
