/*
 * gbdetector.h
 *
 *  Created on: Mar 8, 2016
 *      Author: trungbq
 */

#ifndef SIGNALSIGNALENGINE_H
#define SIGNALSIGNALENGINE_H
#include <mbed.h>
namespace iot {
namespace signal {
class Recorder;
class Processor;
class Event;
class Engine
{
public:
    typedef Callback<void(const Event &event)> Evented;
    typedef Callback<void(int error)> Started;
    Engine();
    virtual ~Engine();

public:
    int start(unsigned int rate,
              Recorder &recorder,
              Processor &processor,
              const Started &stated,
              const Evented &evented);
    int update();
    int process();
    void stop();
private:
    class Private;
    Private *_private;
    friend class Private;
};

} // namespace signal
} // namespace iot
#endif /* SIGNALSIGNALENGINE_H */
