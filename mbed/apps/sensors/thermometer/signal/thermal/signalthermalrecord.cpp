/**
 *  Created on: Mar 8, 2016
 *      Author: trungbq
 **/

#include <core/coreapplication.h>
#include <core/corecommand.h>
#include <signal/signalsample.h>
#include <signal/thermal/signalthermalrecord.h>
#include <utils/utilsprint.h>
#include <mbed.h>

#define MLX_EEPADDR             0xa0    //eep i2c address
#define MLX_RAMADDR             0xc0    //ram i2c address

//EEPROM offsets
#define MLX620_PTATSENS         0x90    //PTAT sensor reading for MLX90620, 16b
#define MLX621_PTATSENS         0x40    //PTAT sensor reading for MLX90621, 16b
#define MLX620_TGCSENS          0x91    //TGC sensor reading for MLX90620, 16b
#define MLX621_TGCSENS          0x41    //TGC sensor reading for MLX90621, 16b
#define MLX_CONFIG              0x92    //config register, 16b
#define MLX_TRIM                0x93    //oscillator trim, lsb>6b of 16b

#define MLX621_EE_ID_BASE       0xf8    //8 bytes, ID info
#define MLX620_EETRIM           0xf7    //1 byte, oscillator trim value
#define MLX621_CONFIG_LO        0xf5    //Config reg, lo byte
#define MLX621_CONFIG_HI        0xf6    //Config reg, hi byte

#define MLX_TOINDEX             0xd4    //0xD4 and 0xE0 (0xD4 + 0x0C), 6 bytes + 6 bytes
#define MLX_TAINDEX             0xda    //0xDA, 6 bytes

#define MLX621_KT1SCALE_BITS    0xf0    //Kt1_scale bits 7:4
#define MLX621_KT2SCALE_BITS    0x0f    //Kt2_scale bits 3:0

#define MLX621_KS_SCALE         0xc0    //KS scaling register
#define MLX621_KS4_EE           0xc4    //Ks4_ee register

#define MLX621_A_COMMON_LO      0xd0    //Acommon register, lo byte
#define MLX621_A_COMMON_HI      0xd1    //Acommon register, hi byte
#define MLX621_KT12_SCALE       0xd2    //Kt1_Kt2__scale register
#define MLX621_ACP_LO           0xd3    //ACP register, lo byte
#define MLX621_ACP_HI           0xd4    //ACP register, hi byte
#define MLX621_BCP              0xd5    //BCP register
#define MLX621_ALPHACP_LO       0xd6    //AlphaCP register, lo byte
#define MLX621_ALPHACP_HI       0xd7    //AlphaCP register, hi byte
#define MLX621_TGC              0xd8    //TGC reg
#define MLX621_DELTA_ABI_SCALE  0xd9    //deltaAi_deltaBi_scale register
#define MLX621_VTH25_LO         0xda    //Vth(25) register lo byte
#define MLX621_VTH25_HI         0xdb    //Vth(25) register hi byte
#define MLX621_KT1_LO           0xdc    //Kt1 register lo byte
#define MLX621_KT1_HI           0xdd    //Kt1 register lo byte
#define MLX621_KT2_LO           0xde    //Kt2 register lo byte
#define MLX621_KT2_HI           0xdf    //Kt2 register lo byte

#define MLX621_THETA0_LO        0xe0    //theta 0 register, lo byte
#define MLX621_THETA0_HI        0xe1    //theta 0 register, hi byte
#define MLX621_THETA0_SCALE     0xe2    //theta 0 scale register
#define MLX621_DELTA_TH_SCALE   0xe3    //delta theta scale register
#define MLX621_A_EMMIS_LO       0xe4    //emissivity register, lo byte
#define MLX621_A_EMMIS_HI       0xe5    //emissivity register, hi byte
#define MLX621_KSTA_LO          0xe6    //KsTa register, lo byte
#define MLX621_KSTA_HI          0xe7    //KsTa register, hi byte

#define MLX621_DELTA_AI_LO      0x3f    //deltaAi register, lo byte
#define MLX621_DELTA_AI_HI      0x00    //deltaAi register, hi byte


#define MLX621_DAISCALE_BITS    0xf0    //deltaAi_scale bits 7:4
#define MLX621_DBISCALE_BITS    0x0f    //deltaBi_scale bits 3:0
#define MLX621_KS_SCALE_BITS    0x0f    //KS scaling bits (3:0)

#define MLX621_BIIJ_EEP_LO      0x00    //Bi(ij)eeprom lo byte ******NEED ADDRESS******
#define MLX621_BIIJ_EEP_HI      0x00    //Bi(ij)eeprom lo byte

#define MLX_TGCX_REG            MLX_TOINDEX + 4
#define MLX_THETA0_REG_LO       MLX_TOINDEX + 12
#define MLX_THETA0_REG_HI       MLX_TOINDEX + 13
#define MLX_THETA0_SCALE_REG    MLX_TOINDEX + 14
#define MLX_DELTA_TH_SCALE_REG  MLX_TOINDEX + 15
#define MLX_EPSILON_REG_LO      MLX_TOINDEX + 16
#define MLX_EPSILON_REG_HI      MLX_TOINDEX + 17

//CONFIG register bit equates
#define MLX_BRNOUTFLAG          0x0400  //brownout flag, 0 = brownout occured
#define MLX_IRMEASFLAG          0x0200  //IR measurement, 1 = measurement in progress
#define MLX_TAMEASFLAG          0x0100  //TA measurement, 1 = measurement in progress

#define MLX621_EEP_ENA          0x1000  //mlx621 enable EEPROM access
#define MLX621_RESOLUTION       0x0030  //mlx621 resolution bits 5 & 4

//config register equates
#define MLX621_CONF_LO_REF_ENA  0x4000  //default
#define MLX621_CONF_HI_REF_ENA  0x0000
#define MLX621_CONF_EEP_DIS     0x1000
#define MLX621_CONF_EEP_ENA     0x0000  //seems like default

#define MLX621_CONF_I2C_1MB     0x0000  //default
#define MLX621_CONF_I2C_400K    0x0800
#define MLX621_CONF_MD_1        0x0400  //default MD must write a "1" when uploading Config Reg
#define MLX621_CONF_POR         0x0000  //POR or Brownout occured
#define MLX621_CONF_IR_BUSY     0x0200  //IR measurement running
#define MLX621_CONF_IR_IDLE     0x0000  //IR measurement -not- running

#define MLX621_CONF_MODE_SLEEP  0x0080  //sleep mode
#define MLX621_CONF_MODE_NORM   0x0000  //default, normal mode
#define MLX621_CONF_MODE_STEP   0x0040  //step mode
#define MLX621_CONF_MODE_CONT   0x0000  //default, continious mode
#define MLX621_CONF_ADC_MASK    0x0030  //ADC mask bits
#define MLX621_CONF_ADC18       0x0030  //default, ADC in 18 bit mode
#define MLX621_CONF_ADC17       0x0020  //ADC in 17 bit mode
#define MLX621_CONF_ADC16       0x0010  //ADC in 16 bit mode
#define MLX621_CONF_ADC15       0x0000  //ADC in 15 bit mode

#define MLX621_CONF_REF_MASK    0x000f  //refresh mask bits
#define MLX621_CONF_REF_HALF    0x000f  //refresh rate 0.5Hz
#define MLX621_CONF_REF_1       0x000e  //default, refresh rate 1Hz
#define MLX621_CONF_REF_2       0x000d  //refresh rate 2Hz
#define MLX621_CONF_REF_4       0x000c  //refresh rate 4Hz
#define MLX621_CONF_REF_8       0x000b  //refresh rate 8Hz
#define MLX621_CONF_REF_16      0x000a  //refresh rate 16Hz
#define MLX621_CONF_REF_32      0x0009  //refresh rate 32Hz
#define MLX621_CONF_REF_64      0x0008  //refresh rate 64Hz
#define MLX621_CONF_REF_128     0x0007  //refresh rate 128Hz
#define MLX621_CONF_REF_256     0x0006  //refresh rate 256Hz
#define MLX621_CONF_REF_512     0x0005  //refresh rate 512Hz
#define MLX621_CONF_REF_512a    0x0004  //refresh rate 512Hz
#define MLX621_CONF_REF_512b    0x0003  //refresh rate 512Hz
#define MLX621_CONF_REF_512c    0x0002  //refresh rate 512Hz
#define MLX621_CONF_REF_512d    0x0001  //refresh rate 512Hz
#define MLX621_CONF_REF_512e    0x0000  //refresh rate 512Hz
#define WARMINGUP_TIME 10.0f
namespace iot {
namespace signal {
namespace thermal {

/**
 * @brief The Recorder::Private class
 */
class Recorder::Private
{
public:
    class Start: public core::Command
    {
    public:
        Start(Recorder::Private &recorder):
            _recorder(recorder) {

        }

        virtual void execute() {
            _recorder.start();
            delete this;
        }

    private:
        Recorder::Private &_recorder;
    };
    /**
     * Public data structure for MLX9062x data values.
     *
    **/
    enum MLXdevice {
        mlx90620,  //device is an MLX90620
        mlx90621,  //device is an MLX90620
    };
    struct _MLX {
        uint16_t Config;                /*!< MLX9062x configuration register*/
        uint16_t OscTrim;               /*!< MLX9062x oscillator trim register*/
        uint16_t PtatD;                 /*!< MLX9062x PTAT data register*/
        int16_t VCP;                    /*!< MLX90621 VCP data register*/
        int mlxDevice;                  /*!< MLX90620 or MLX90621 device type flag*/
        uint32_t mlx621IDhi;            /*!< MLX90621 only, device ID hi 32 bits*/
        uint32_t mlx621IDlo;            /*!< MLX90621 only, device ID lo 32 bits*/
        char MLXEEbuf[256];             /*!< MLX9062x EEPROM data buffer, 0x00 - 0xff*/
        char MLXRamBuf[128];            /*!< MLX9062x RAM data buffer, 0x00 - 0x7f*/
        char MLXRamCmmd[8];             /*!< MLX9062x i2c command i/o buffer*/
        double DieTemp;                 /*!< MLX9062x die temperature*/
    };


    Private(PinName data, PinName clock, PinName power):
        _signal(data, clock),
        _power(power) {

    }


    void warmingUp() {
        Start *start = new Start(*this);
        if (core::Application::instance()->schedule(start) == 0) {
            delete start;
            _started.call(-1);
        }
    }

    void start() {
        _mlx.mlx621IDhi = 0;
        _mlx.mlx621IDlo = 0;
        _mlx.mlxDevice = mlx90621;                                    //make strurcture device same as local device

        //clear out buffer first
        for(int index = 0; index < 256; index++)
            _mlx.MLXEEbuf[index] = 0;  //clear out entire EEMPROM buffer
    //the following code addition of a 1 byte I2C fetch is due to a bug in the F746NG which can only transfer 255 bytes at a time.  grrr!!!....
    #if defined(TARGET_DISCO_F746NG)
    #warning "TARGET_DISCO_F746NG i2c bug, 255 byte limit!!!"
        _mlx.MLXEEbuf[0] = 255;
        if(!_signal.write(MLX_EEPADDR, _mlx.MLXEEbuf, 1, true)) {
            _signal.read((MLX_EEPADDR + 1), _mlx.MLXEEbuf, 1, false);
            _mlx.MLXEEbuf[255] = _mlx.MLXEEbuf[0];
            _mlx.MLXEEbuf[0] = 0;
            _signal.write(MLX_EEPADDR, _mlx.MLXEEbuf, 1, true);
            _signal.read((MLX_EEPADDR + 1), _mlx.MLXEEbuf, 255, false); //s/b 256 if F746NG bug wasn't there
    #else
        if(!_signal.write(MLX_EEPADDR, _mlx.MLXEEbuf, 1, true)) {      //send command, 0 returned is ok
            _signal.read((MLX_EEPADDR + 1), _mlx.MLXEEbuf, 256, false);
    #endif
        } else {
            _signal.stop();                                    //don't read EEP if write is broken
            _started.call(-1);
            return;                                      //return with error
        }
        if(_mlx.mlxDevice == mlx90621) {
            _mlx.mlx621IDhi = (_mlx.MLXEEbuf[MLX621_EE_ID_BASE + 7] << 24) |
                              (_mlx.MLXEEbuf[MLX621_EE_ID_BASE + 6] << 16) |
                              (_mlx.MLXEEbuf[MLX621_EE_ID_BASE + 5] <<  8) |
                              _mlx.MLXEEbuf[MLX621_EE_ID_BASE + 4];
            _mlx.mlx621IDlo = (_mlx.MLXEEbuf[MLX621_EE_ID_BASE + 3] << 24) |
                              (_mlx.MLXEEbuf[MLX621_EE_ID_BASE + 2] << 16) |
                              (_mlx.MLXEEbuf[MLX621_EE_ID_BASE + 1] <<  8) |
                              _mlx.MLXEEbuf[MLX621_EE_ID_BASE + 0];
        }

        _mlx.MLXRamCmmd[0] = 4;                                     //command
        _mlx.MLXRamCmmd[1] = _mlx.MLXEEbuf[MLX620_EETRIM] - 0xaa;   //LS byte check
        _mlx.MLXRamCmmd[2] = _mlx.MLXEEbuf[MLX620_EETRIM];          //oscillator trim value
        _mlx.MLXRamCmmd[3] = 0x100 - 0xaa;                          //MS byte check
        _mlx.MLXRamCmmd[4] = 0;                                     //MS byte = 0


        int error = _signal.write(MLX_RAMADDR, _mlx.MLXRamCmmd, 5, false);
        if (error != 0) {
            _started.call(error);
            _signal.stop();
            return;
        }

        _mlx.MLXRamCmmd[0] = 3;                                     //command
        if(_mlx.mlxDevice == mlx90621) {
            _mlx.MLXRamCmmd[2] = _mlx.MLXEEbuf[MLX621_CONFIG_LO];   //LS byte
            _mlx.MLXRamCmmd[4] = _mlx.MLXEEbuf[MLX621_CONFIG_HI];   //MS byte
            //here you can manipulate the Config Register contents from the default EEPROM values of 0x463e
            _mlx.MLXRamCmmd[2] = (_mlx.MLXRamCmmd[2] & ~MLX621_CONF_ADC_MASK) | MLX621_CONF_ADC18;
            _mlx.MLXRamCmmd[4] = _mlx.MLXRamCmmd[4] | (MLX621_CONF_MD_1 >> 8);
            _mlx.MLXRamCmmd[1] = _mlx.MLXRamCmmd[2] - 0x55;         //LS byte check
            _mlx.MLXRamCmmd[3] = _mlx.MLXRamCmmd[4] - 0x55;         //MS byte check
        } else {
            _mlx.MLXRamCmmd[1] = 0x10c - 0x55;                      //LS byte check
            _mlx.MLXRamCmmd[2] = 0x0c;                              //LS config value, normal mode, 4Hz array  *******
            _mlx.MLXRamCmmd[3] = 0x5c - 0x55;                       //MS byte check
            _mlx.MLXRamCmmd[4] = 0x5c;                              //MS config value, 8Hz Ta, 400k i2c
        }
        eror = _i2c.write(MLX_RAMADDR, _mlx.MLXRamCmmd, 5, false);
        if (error != 0) {
            _started.call(error);
            _signal.stop();
            return;
        }

        _started.call(0);
    }

    int start(const signal::Recorder::Started &started) {
        _started = started;
        _power = 1;
        _timeout.attach(Callback<void()>(this, &Private::warmingUp),
                        WARMINGUP_TIME);
        return 0;
    }

    int record(signal::Sample &sample) {
        _mlx.MLXRamCmmd[0] = 2;                                     //command
        _mlx.MLXRamCmmd[1] = 0;                                     //start address
        _mlx.MLXRamCmmd[2] = 1;                                     //address step
        _mlx.MLXRamCmmd[3] = 0x40;                                  //# of reads
        int error = _i2c.write(MLX_RAMADDR, _mlx.MLXRamCmmd, 4, true);
        if (error != 0)
            return error;

        error = _i2c.read(MLX_RAMADDR, _mlx.MLXRamBuf, 0x80);
        if (error != 0)
            return error;
        signal::Sample::Values &values = sample.values();
        values.resize(64);
        signal::Sample::Value *cells = values.values();
        for(uint8_t index = 0; index < 64; index++) {
            cells[index] = (_mlx.MLXRamBuf[index * 2 + 1] << 8) +
                            _mlx.MLXRamBuf[index * 2];
        }

        return measurement();
    }

    int measurement() {
        if(_mlx.mlxDevice == mlx90621)
            return 0;                         //there is no measurement in the 90621
        _mlx.MLXRamCmmd[0] = 1;               //command
        _mlx.MLXRamCmmd[1] = 8;               //address of config register
        return _i2c.write(MLX_RAMADDR, _mlx.MLXRamCmmd, 2, false);
    }

    void stop() {
        _power = 0;
    }

private:
    I2C _signal;
    DigitalOut _power;
    struct _MLX _mlx;
    signal::Recorder::Started _started;
    Timeout _timeout;
};

/**
 * @brief Recorder::Recorder
 * @param pin
 */
Recorder::Recorder(PinName data, PinName clock, PinName power):
    _private(new Private(data, clock, power))
{
}

Recorder::~Recorder()
{
    delete _private;
}

int Recorder::start(unsigned int /*rate*/,
                    const signal::Recorder::Started &started)
{
    return _private->start(started);
}

int Recorder::record(signal::Sample &sample)
{
    return _private->record(sample);
}

void Recorder::stop()
{
    _private->stop();
}


} // namespace thermal
} // namespace signal
} // namespace iot
