/**
 *  Created on: Mar 8, 2016
 *      Author: trungbq
 */

#ifndef SIGNALTHERMALMOTIONPROCESS_H
#define SIGNALTHERMALMOTIONPROCESS_H
#include <signal/thermal/signalthermalprocess.h>
namespace iot {
namespace signal {
namespace thermal {
namespace motion {
class Processor: public thermal::Processor
{
public:
    Processor(const lowpass::Filter::Alpha &lowpass,
              const lowpass::Filter::Alpha &hipass,
              float adaptTime,
              float beginTime,
              float endTime);

    virtual ~Processor();

public:
    virtual int start(int sampleRate);

    virtual void stop();
private:

    virtual int processThermal(const signal::Sample &sample,
                               const signal::Processor::Processed &processed);
private:
    class Private;
    Private *_private;
};

} // namespace motion
} // namespace thermal
} // namespace signal
} // namespace iot

#endif // SIGNALTHERMALMOTIONPROCESS_H
