/**
 *  Created on: Mar 8, 2016
 *      Author: trungbq
 */

#include <signal/thermal/motion/signalthermalmotionprocess.h>
#include <signal/signalevent.h>
#include <signal/signalsample.h>
#include <std/stdring.h>
#include <utils/utilsprint.h>

namespace iot {
namespace signal {
namespace thermal {
namespace motion {

class Processor::Private
{
public:
    enum State {
        Idle,
        Adapting,
        BeginpointDetecting,
        EndpointDetecting
    };
public:
    Private(const float &adaptTime,
            const float &beginTime,
            const float &endTime):
        _state(Idle),
        _adaptTime(adaptTime),
        _beginTime(beginTime),
        _endTime(endTime),
        _adapt(0.0f),
        _adapts(NULL),
        _average(0.0f),
        _adaptSquare(0.0f),
        _adaptSquares(NULL),
        _begin(0.0f),
        _begins(NULL),
        _beginSquare(0.0f),
        _beginSquares(NULL),
        _end(0.0f),
        _ends(NULL),
        _endSquare(0.0f),
        _endSquares(NULL),
        _size(0),
        _count(0) {
    }

    int start(int sampleRate) {
        if (_state != Idle)
            return -1;
        _size = uint32_t(_adaptTime * float(sampleRate));
        uint32_t adapts = uint32_t(_adaptTime * float(sampleRate));

        if (adapts == 0)
            return -1;

        _adapt = 0.0f;
        _adapts = new Samples(adapts);

        _adaptSquare = 0.0f;
        _adaptSquares = new Samples(adapts);

        uint32_t begins = uint32_t(_beginTime * float(sampleRate));
        if (begins == 0) {

            delete _adaptSquares;
            _adaptSquares = NULL;

            delete _adapts;
            _adapts = NULL;

            return -1;
        }

        _begin = 0.0f;
        _begins = new Samples(begins);

        _beginSquare = 0.0f;
        _beginSquares = new Samples(begins);

        uint32_t ends = uint32_t(_endTime * float(sampleRate));
        if (ends == 0) {
            delete _beginSquares;
            _beginSquares = NULL;

            delete _begins;
            _begins = NULL;


            delete _adaptSquares;
            _adaptSquares = NULL;

            delete _adapts;
            _adapts = NULL;
            return -1;
        }

        _end = 0.0f;
        _ends = new Samples(ends);

        _endSquare = 0.0f;
        _endSquares = new Samples(ends);
        _state = Adapting;
        return 0;
    }

    void processAdapt(const iot::signal::Sample::Value &value,
                      const iot::signal::Sample::Value &square) {
        _adapt += value;
        _adapts->push(value);
        _adaptSquare += square;
        _adaptSquares->push(value);

        if (_adapts->fulled()) {
            _average = _adapt / iot::signal::Sample::Value(_adapts->size());
            _average = 1.0;
            PRINTF("Average: %1.4f\r\n", _average);
            _adapt = 0.0f;
            _adaptSquare = 0.0f;
            _adapts->reset();
            _state = BeginpointDetecting;
        }

        if (_begins->fulled()) {
            iot::signal::Sample::Value last;
            _begins->pop(last);
            _begin -= last;

            _beginSquares->pop(last);
            _beginSquare -= last;
        }

        _begin += value;
        _begins->push(value);
        _beginSquare += square;
        _beginSquares->push(square);
    }

    void processBeginpoint(const iot::signal::Sample::Value &value,
                           const iot::signal::Sample::Value &square,
                           const Processor::Processed &processed) {
        if (_begins->fulled()) {
            iot::signal::Sample::Value last;
            _begins->pop(last);
            _begin -= last;

            _beginSquares->pop(last);
            _beginSquare -= last;
        }

        _begin += value;
        _begins->push(value);

        _beginSquare += square;
        _beginSquares->push(square);

        if (_begins->fulled()) {
            float size = _begins->size();
            float energy = (_beginSquare  - 2.0f * _begin * _average) / size +
                    _average * _average;
            if (energy > 0.0625f) {
                PRINTF("Begin energy: %1.4f\r\n", energy);
                BeginEvent event(0);
                processed.call(event);
                _state = EndpointDetecting;
                _begins->reset();
                _begin = 0.0f;
                _beginSquare = 0.0f;
            }
        }

        if (_ends->fulled()) {
            iot::signal::Sample::Value last;
            _ends->pop(last);
            _end -= last;

            _endSquares->pop(last);
            _endSquare -= last;
        }

        _end += value;
        _ends->push(value);

        _endSquare += square;
        _endSquares->push(square);
    }

    void processEndpoint(const iot::signal::Sample::Value &value,
                         const iot::signal::Sample::Value &square,
                         const Processor::Processed &processed) {
        if (_ends->fulled()) {
            iot::signal::Sample::Value last;
            _ends->pop(last);
            _end -= last;

            _endSquares->pop(last);
            _endSquare -= last;
        }

        _end += value;
        _ends->push(value);

        _endSquare += square;
        _endSquares->push(square);

        if (_ends->fulled()) {
            float size = _ends->size();
            float energy = (_endSquare  - 2.0f * _end * _average) / size +
                    _average * _average;

            if (energy < 0.015625f) {
                PRINTF("End energy: %1.4f\r\n", energy);
                EndEvent event(0);
                processed.call(event);
                _state = Adapting;
                _ends->reset();
                _end = 0.0f;
                _endSquare = 0.0f;
            }
        }

    }
    int process(const Sample &sample,
                const signal::Processor::Processed &processed) {

        const iot::signal::Sample::Value &value = sample.values()[0];
        iot::signal::Sample::Value square = value * value;
        switch(_state) {
        case Idle:
            return -1;
        case Adapting:
            processAdapt(value, square);
            break;
        case BeginpointDetecting:
            processBeginpoint(value, square, processed);
            break;
        case EndpointDetecting:
            processEndpoint(value, square, processed);
            break;
        }

        return 0;
    }

    void stop() {

        delete _adapts;
        _adapts = NULL;

        delete _adaptSquares;
        _adaptSquares = NULL;

        delete _begins;
        _begins = NULL;
        delete _beginSquares;
        _beginSquares = NULL;

        delete _ends;
        _ends = NULL;
        delete _endSquares;
        _endSquares = NULL;

        _state = Idle;
    }

private:

    State _state;

    float _adaptTime;
    float _beginTime;
    float _endTime;

    typedef std::Ring<iot::signal::Sample::Value> Samples;

    iot::signal::Sample::Value _adapt;
    Samples *_adapts;

    iot::signal::Sample::Value _average;

    iot::signal::Sample::Value _adaptSquare;
    Samples *_adaptSquares;

    iot::signal::Sample::Value _begin;
    Samples *_begins;
    iot::signal::Sample::Value _beginSquare;
    Samples *_beginSquares;


    iot::signal::Sample::Value _end;
    Samples *_ends;
    iot::signal::Sample::Value _endSquare;
    Samples *_endSquares;
    uint32_t _size;
    uint32_t _count;

};

Processor::Processor(const lowpass::Filter::Alpha &lowpass,
                     const hipass::Filter::Alpha &hipass,
                     float adaptTime,
                     float beginTime,
                     float endTime):
    thermal::Processor(lowpass, hipass),
    _private(new Private(adaptTime, beginTime, endTime))
{

}

Processor::~Processor()
{
}

int Processor::start(int sampleRate)
{
    return _private->start(sampleRate);
}

int Processor::processThermal(const signal::Sample &sample,
                              const signal::Processor::Processed &processed)
{
    return _private->process(sample, processed);
}

void Processor::stop()
{
    _private->stop();
}

} // namespace motion
} // namespace thermal
} // namespace signal
} // namespace iot
