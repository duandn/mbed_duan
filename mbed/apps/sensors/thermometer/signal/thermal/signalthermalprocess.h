/*
 * soundrocess.h
 *
 *  Created on: Mar 8, 2016
 *      Author: trungbq
 */

#ifndef SIGNALTHERMALROCESS_H
#define SIGNALTHERMALROCESS_H

#include <signal/signalfilter.h>
#include <signal/signalprocess.h>

namespace iot {
namespace signal {
namespace thermal {

class Processor: public signal::Processor {
public:
    Processor(const lowpass::Filter::Alpha &lowpass,
              const hipass::Filter::Alpha &hipass);
    virtual ~Processor();
private:
    virtual int preprocess(const signal::Sample &sample,
                           signal::Sample &preprocessed);
    virtual int postprocess(const signal::Sample &preprocessed,
                            const signal::Processor::Processed &processed);
private:
    virtual int processThermal(const signal::Sample &sample,
                               const signal::Processor::Processed &processed)
                = 0;
private:
    class Private;
    Private *_private;
};

} // namespace thermal
} // namespace signal
} // namespace iot

#endif // SIGNALTHERMALROCESS_H
