/*
 * soundrecord.h
 *
 *  Created on: Mar 8, 2016
 *      Author: trungbq
 */

#ifndef SIGNALTHERMALRECORD_H
#define SIGNALTHERMALRECORD_H
#include <mbed.h>
#include <signal/signalrecord.h>
namespace iot {
namespace signal {
namespace thermal {

class Recorder: public signal::Recorder {
public:
    Recorder(PinName data, PinName clock, PinName power);

	virtual ~Recorder();

    virtual int start(unsigned int rate,
                      const signal::Recorder::Started &started);

    virtual int record(signal::Sample &sample);

    virtual void stop();
private:
    class Private;
    Private *_private;
};

} // namespace thermal
} // namespace signal
} // namespace iot

#endif // SIGNALTHERMALRECORD_H
