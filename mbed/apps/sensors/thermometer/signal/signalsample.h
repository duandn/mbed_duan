/*
 * sample.h
 *
 *  Created on: Mar 9, 2016
 *      Author: trungbq
 */

#ifndef SIGNALSAMPLE_H
#define SIGNALSAMPLE_H
#include <std/stdvector.h>
namespace iot {
namespace signal {

/**
 *
 *
 */
class Sample
{
public:
    typedef unsigned long Time;
    typedef float Value;
    typedef std::Vector<Value, uint8_t> Values;
    Sample();
    Sample(const Time &time, const Values &values);
    Sample(const Sample &sample);
    virtual ~Sample();

    inline void setTime(const Time &time) {
        _time = time;
    }

    inline const Time &time() const {
        return _time;
    }

    inline Values &values() {
        return _values;
    }

    inline const Values &values() const {
        return _values;
    }

    inline Sample &operator = (const Sample &sample) {
        _time = sample.time();
        _values = sample.values();
        return *this;
    }
private:
    Time _time;
    Values _values;
};

} // namespace signal
} // namespace iot

#endif // SIGNALSAMPLE_H
