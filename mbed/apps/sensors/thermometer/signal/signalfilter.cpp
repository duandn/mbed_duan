/*
 * filter.cpp
 *
 *  Created on: Mar 15, 2016
 *      Author: trungbq
 */

#include "signalfilter.h"
#include "signalsample.h"

namespace iot {
namespace signal {

/**
 *
 *
 */
Filter::~Filter()
{
}

namespace lowpass {

/**
 * @brief The Filter::Private class
 */
class Filter::Private
{
public:
    Private(const Filter::Alpha &alpha):
        _skipped(true),
        _alpha(alpha) {

    }
    int apply(const Sample &sample, Sample &filtered) {
        if (_skipped) {
            _filtered = sample;
            _skipped = false;
            return -1;
        }

        Sample::Values &filteredValues = _filtered.values();
        const Sample::Values &values = sample.values();

        uint8_t size = filteredValues.size();
        for(uint8_t index = 0; index < size; index++) {
            Sample::Value &filteredValue = filteredValues[index];
            filteredValue =  _alpha * (values[index] - filteredValue);
        }

        filtered = _filtered;
        return 0;
    }
private:
    bool _skipped;
    Filter::Alpha _alpha;
    Sample _filtered;
};

/**
 * @brief Filter::Filter
 * @param alpha
 */
Filter::Filter(const Alpha &alpha):
    _private(new Private(alpha))
{
    delete _private;
}

Filter::~Filter()
{
    delete _private;
}

int Filter::apply(const Sample &sample, Sample &filtered)
{
    return _private->apply(sample, filtered);
}
} // namespace lowpass


namespace hipass {
/**
 * @brief The Filter::Private class
 */
class Filter::Private
{
public:
    Private(const Filter::Alpha &alpha):
        _skipped(true),
        _alpha(alpha) {

    }
    int apply(const Sample &sample, Sample &filtered) {
        if (_skipped) {
            _filtered = sample;
            _previous = sample;
            _skipped = false;
            return -1;
        }

        Sample::Values &filteredValues = _filtered.values();
        Sample::Values &previousValues = _previous.values();
        const Sample::Values &values = sample.values();

        uint8_t size = filteredValues.size();
        for(uint8_t index = 0; index < size; index++) {
            Sample::Value &filteredValue = filteredValues[index];
            filteredValue =  _alpha * (filteredValue +
                                       values[index] -
                                       previousValues[index]);
            previousValues[index] = values[index];
        }

        filtered = _filtered;
        return 0;
    }
private:
    bool _skipped;
    Filter::Alpha _alpha;
    Sample _filtered;
    Sample _previous;
};


/**
 * @brief Filter::Filter
 * @param alpha
 */
Filter::Filter(const Alpha &alpha):
    _private(new Private(alpha))
{
    delete _private;
}

Filter::~Filter()
{
    delete _private;
}

int Filter::apply(const Sample &sample, Sample &filtered)
{
    return _private->apply(sample, filtered);
}

} // hipass
} // namespace signal
} // namespace iot
