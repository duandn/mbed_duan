/*
 * event.h
 *
 *  Created on: Mar 9, 2016
 *      Author: trungbq
 */

#ifndef SIGNAL_SIGNALEVENT_H_
#define SIGNAL_SIGNALEVENT_H_

namespace iot {
namespace signal {

/**
 * class Event declare
 *
 *
 **/
class Event
{
public:
    enum Type {
        Begin,
        End
    };

public:
    Event(const long &id);
    virtual ~Event();
public:
    virtual Type type() const = 0;
public:
    inline const long &id() const {
        return _id;
    }
private:
    long _id;
};

class BeginEvent: public Event
{
public:
    BeginEvent(const long &id);
    virtual ~BeginEvent();
public:
    virtual Type type() const;
};

class EndEvent: public Event
{
public:
    EndEvent(const long &id);
    virtual ~EndEvent();
public:
    virtual Type type() const;
};


} /* namespace Signal */
} /* namespace VNG */

#endif /* SIGNAL_SIGNALEVENT_H_ */
