/*
 * sample.cpp
 *
 *  Created on: Mar 9, 2016
 *      Author: trungbq
 */

#include "signalsample.h"

namespace iot {
namespace signal {

/**
 *
 *
 */

Sample::Sample():
    _time(0)
{

}

Sample::Sample(const Time &time, const Values &values):
    _time(time),
    _values(values)
{

}

Sample::Sample(const Sample &sample):
    _time(sample.time()),
    _values(sample.values())
{

}

Sample::~Sample()
{

}


} /* namespace Signal */
} /* namespace VNG */
