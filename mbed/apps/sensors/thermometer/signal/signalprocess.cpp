#include <mbed.h>
#include "signalprocess.h"
#include "signalsample.h"

namespace iot {
namespace signal {
     
Processor::Processor()
{
}

Processor::~Processor()
{
}        


int Processor::process(const Sample &sample, const Processed &processed)
{
	Sample preprocessed;
	int error = preprocess(sample, preprocessed);
	if (error != 0)
	    return error;

    return postprocess(preprocessed, processed);
}

} /* namespace Signal */
} /* namespace VNG */
