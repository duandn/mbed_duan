#include "application.h"
#include <ble/blesystem.h>
#include <ble/bleevent.h>
#include <core/corestatus.h>
#include <core/corecommand.h>
#include <devices/devicessettings.h>
#include <devices/devicesprotocol.h>
#include <devices/devicesserial.h>
#include <radio/radiocontrol.h>
#include <radio/radioschedule.h>
#include <signal/signalengine.h>
#include <signal/signalevent.h>
#include <signal/thermal/signalthermalrecord.h>
#include <signal/thermal/motion/signalthermalmotionprocess.h>
#include <utils/utilsprint.h>
#include <utils/utilsendian.h>
#include <BLE.h>
#include <stddef.h>

using namespace iot;
using namespace iot::radio;
using namespace iot::utils;
#define THEMAL_DATA_PIN p5
#define THEMAL_CLOCK_PIN p5
#define THEMAL_POWER_PIN p4
#define DETECT_DURATION 0.1f
#define NOTIFY_DURATION 1.0f

#define STATUS_PERIOD 5.0f
static const char pir[] = "TMM";
static const char active[] = "ATM";

namespace iot {
/**
 * @brief The Status class
 */
class Status: public core::Status
{
public:
    Status(const core::Status::Resets &resets = 0):
        core::Status(resets) {

    }

    virtual ~Status() {

    }

public:
    virtual core::Status::Period period() const {
        return STATUS_PERIOD;
    }

    virtual core::Status::Device device() const {
        return core::Status::ActiveThermometer;
    }

    virtual const char *descrition() const {
        return &pir[0];
    }
};

/**
 * @brief The Application::Private class
 */
class Application::Private
{
public:
    class Detect: public core::Command
    {
    public:
        Detect(Application::Private &app):
            _app(app) {

        }

        virtual ~Detect() {

        }

        virtual void execute() {
            _app.detect();
        }

    private:
        Application::Private &_app;
    };

    /**
     * @brief The Event class
     */
    class Event: public ble::Event
    {
    public:
        typedef uint8_t Actived;
        Event():
            _actived(0) {

        }

        virtual ~Event() {

        }

        virtual ble::Message::Command command() const {
            return ble::Message::ActiveThermometer;
        }


        virtual const char *descrition() const {
            return &active[0];
        }

        inline void setActived(Actived actived) {
            _actived = actived;
        }

        inline Actived actived() const {
            return _actived;
        }

    private:

        virtual uint8_t _size() const {
            return sizeof(Actived);
        }

        virtual void _fill(uint8_t *details) const {
            memcpy(details,&_actived, sizeof(Actived));
        }

    private:
        Actived _actived;
    };


    Private(ble::System &system):
        _system(system),
        _recorder(THEMAL_DATA_PIN, THEMAL_CLOCK_PIN, THEMAL_POWER_PIN),
        _processor(0.46f, 0.54f, 0.5f, 1.0f, 1.0f),
        _actived(false),
        _actives(0) {
    }

    virtual ~Private() {

    }

    void evented(const signal::Event &event) {

        if (event.type() == signal::Event::Begin) {
            _actived = true;
            _actives += 1;
        } else {
            _actived = false;
        }

    }

    void onNotify() {
        _event.setActived(_actives > 0 || _actives);
        _system.advertise(_event);
        if (!_actived)
            _actives = 0;
    }

    void onDetect() {
        Application::instance().schedule(_detect);
    }

    void detect() {
        _engine.update();
        _engine.process();
    }

    void started(int error) {
        if (error != 0)
            return;

        _detector.attach(Callback<void()>(this, &Private::onDetect),
                         DETECT_DURATION);
        _notifier.attach(Callback<void()>(this, &Private::onNotify),
                         NOTIFY_DURATION);
    }

    int initialize() {
        PRINTF("*applicatin* system initializing..");
        int error = _system.start(128);
        if (error != 0) {
            PRINTF("failed with error: %d\r\n", error);
            return error;
        }
        PRINTF("successed\r\n");
        PRINTF("*applicatin* engine starting..");
        error = _engine.start(10,
                              _recorder,
                              _processor,
                              signal::Engine::Started(this,
                                                      &Private::started),
                              signal::Engine::Evented(this,
                                                      &Private::evented));
        if (error != 0) {
            PRINTF("failed with error: %d\r\n", error);
            _system.stop();
            return error;
        }
        PRINTF("successed\r\n");
        _detect = new Detect(*this);
        return 0;
    }

    void release() {
        _detector.detach();
        delete _detect;
        _detect = NULL;
        _engine.stop();
        _system.stop();
    }

private:
    iot::ble::System &_system;
    signal::thermal::Recorder _recorder;
    signal::thermal::motion::Processor _processor;
    signal::Engine _engine;
    Ticker _detector;
    Detect *_detect;
    Ticker _notifier;
    bool _actived;
    uint8_t _actives;
    Event _event;
};

/**
 * @brief Application::Application
 */
Application::Application()
{

}

Application::~Application()
{
}

int Application::initialize(ble::System &system)
{
    if (_private != NULL)
        return -1;

    _private = new Private(system);
    int error = _private->initialize();
    if (error != 0) {
        delete _private;
        _private = NULL;
        return error;
    }
    return 0;
}


void Application::release(ble::System &system)
{
    if (_private == NULL)
        return;
    _private->release();
    delete _private;
    _private = NULL;
}

core::Status &Application::status()
{
    static Status _status;
    return _status;
}

Application &Application::instance()
{
    static Application app;
    return app;
}
} // namespace iot
