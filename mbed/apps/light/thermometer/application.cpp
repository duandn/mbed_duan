#include "application.h"
#include <ble/gap/blegapevent.h>
#include <ble/gap/blegappacket.h>
#include <ble/blesystem.h>
#include <core/corecommand.h>
#include <devices/devicessensor.h>
#include <devices/devicessettings.h>
#include <radio/radiocontrol.h>
#include <radio/radioschedule.h>
#include <utils/utilsprint.h>
#include <utils/utilsendian.h>
#include <BLE.h>
#include <stddef.h>

#define LIGHT_SENSOR_PERIOD 0.05
#define LIGHT_SENSOR_CLOCK p14
#define LIGHT_SENSOR_DATA p16

#define LIGHT_SENSOR_PIN p6
#define LIGHT_WAKEUP_TIME 0.25

#define VOLTAGE_SENSOR_PIN p2
#define VOLTAGE_WAKEUP_TIME 0.25
#define VOLTAGE_SENSOR_PERIOD 0.25


#define HUMIDITY_SENSOR_CLOCK p2
#define HUMIDITY_SENSOR_DATA p3


using namespace iot;
using namespace iot::utils;
using namespace iot::devices;
using namespace iot::devices::serial;
using namespace iot::radio;
using namespace iot::ble::gap;
/**
 * @brief The Application::Private class
 */
namespace iot {

class Application::Private
{
public:
    Private() {
    }

    ~Private() {

    }

    void advertised(const Controller::Id &id, bool rejected) {

    }

    void updated(const Value &value) {
        iot::ble::gap::Packet packet;

        uint8_t flags[] = {0x00};
        float _value;


        int error = packet.setField(Packet::TypeFlags, flags, sizeof(flags));
        if (error != 0) {
            PRINTF("*applications* updated set flags failed with error:"
                   " %d\r\n",
                   error);
            return;
        }
        uint8_t appearance[] = {0};
        error = packet.setField(Packet::TypeAppearance,
                                appearance,
                                sizeof(appearance));
        if (error != 0) {
            PRINTF("*applications* updated set appearance failed with error:"
                   " %d\r\n",
                   error);
            return;
        }

        uint8_t payload[sizeof(uint16_t) +
                sizeof(struct iot::ble::gap::_Event) +
                sizeof(_value)];

        CompanyId *companyId = (CompanyId *) &payload[0];
        *companyId = endian::littleToBig((uint16_t)0xFF0F);
        struct _Event *event = (struct _Event *) (payload + sizeof(uint16_t));


        if(value.type() == devices::Value::Humidity){

            const devices::humidity::Value &hum = (const devices::humidity::Value &) value;
            _value = hum.humidity();
            event->commandId = endian::littleToBig((uint16_t)EVENT_HUMIDITY_SENSOR);
            PRINTF("*applications* value.type() =  Humidity\r\n");
            PRINTF("*applications* value.humidity() = : %3.2f\r\n", hum.humidity());

        }else if(value.type() == devices::Value::Temperature){

            const devices::temperature::Value &temp = (const devices::temperature::Value &) value;
            _value = temp.temperature();
            event->commandId = endian::littleToBig((uint16_t)EVENT_TEMPERATURE_SENSOR);

            PRINTF("*applications* value.type() =  Temperature\r\n");
            PRINTF("*applications* value.temperature() = : %3.2f\r\n", temp.temperature());

        }else{
            _value = -1;
        }


        PRINTF("*applications* value: %3.2f\r\n", _value);

        event->gateway = 0;
        float bigValue = endian::littleToBig(_value);
        const uint8_t *source = (const uint8_t *) &bigValue;
        uint8_t *target = event->payload;
        for(uint8_t index = 0; index < sizeof(_value); index++)
            target[index] = source[index];

        error = packet.setField(Packet::TypeManufacturerSpecificData,
                                &payload[0],
                sizeof(payload));
        if (error != 0) {
            PRINTF("*application* updated set manufacturer specific data"
                   "failed with error: %d\r\n",
                   error);
            return;
        }

        uint8_t name[] = "Thermometer";
        size_t length = sizeof(name) - 1;
        error = packet.setField(Packet::TypeCompleteLocalName, name, length);
        if (error != 0) {
            PRINTF("*application* updated set local name failed with error:"
                   " %d\r\n", error);
            return;
        }

        Controller &controller = Controller::instance();
        Controller::Id id;
        error = controller.advertise(packet.payload(), packet.length(), 3, id);
        if (error != 0) {
            PRINTF("*application* updated asvertise failed with error: %d\r\n",
                   error);
        }
    }

    int initialize() {

        PRINTF("*applications* initialize scheduler ...");

        Scheduler &scheduler = Scheduler::instance();
        int error = scheduler.initialize();
        if (error != 0) {
            PRINTF("with error: %d\r\n", error);
            return error;
        }
        PRINTF("successed\r\n");

        BLE &ble = _ble.ble();
        BLEProtocol::AddressType_t type;
        BLEProtocol::AddressBytes_t address;
        ble.gap().getAddress(&type, address);

        PRINTF("Address: %02X:%02X:%02X:%02X:%02X:%02X\r\n",
               address[0],
                address[1],
                address[2],
                address[3],
                address[4],
                address[5]);

        PRINTF("*applications* initialize controller ...");
        Controller &controller = Controller::instance();
        Controller::Advertised advertised(this, &Private::advertised);
        error = controller.initialize(address,
                                      Controller::All,
                                      1024,
                                      advertised);
        if (error != 0) {
            PRINTF("with error: %d\r\n", error);
            scheduler.release();
            return error;
        }
        PRINTF("successed\r\n");

        PRINTF("*applications* thermometer initializing...");
        devices::i2c::Settings light(HUMIDITY_SENSOR_DATA,
                                     HUMIDITY_SENSOR_CLOCK,
                                     4000000,
                                     0x40 << 1);
        devices::Sensor::Updated updated(this, &Private::updated);
        error = _sensor.start(light,
                              LIGHT_SENSOR_PERIOD,
                              updated);
        if (error != 0) {
            PRINTF("with error: %d\r\n", error);
            controller.release();
            scheduler.release();
            return error;
        }

        PRINTF("successed\r\n");

        return 0;
    }

    inline int subsystems() const {
        return 1;
    }

    inline iot::core::System *subsystem(int subsystem) {
        return subsystem == 0? &_ble: NULL;
    }

    void release() {
        Scheduler::instance().release();
        Controller::instance().release();
    }
private:
    iot::ble::System _ble;
    devices::si7021a20::Sensor _sensor;
};
/**
 * @brief Application::Application
 */
Application::Application():
    _private(new Private)
{

}

Application::~Application()
{
    delete _private;
}

int Application::initialize()
{
    return _private->initialize();
}

int Application::systems() const
{
    return _private->subsystems();
}

iot::core::System *Application::system(int subsystem)
{
    return _private->subsystem(subsystem);
}


void Application::release()
{
    _private->release();
}

Application &Application::instance()
{
    static Application app;
    return app;
}

} // namespace iot
