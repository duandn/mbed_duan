#include "application.h"
#include <ble/blerequest.h>
#include <ble/blereply.h>
#include <ble/blesystem.h>
#include <ble/gap/blegappacket.h>
#include <core/corecommand.h>
#include <core/corestatus.h>
#include <devices/devicespwmout.h>
#include <radio/radiocontrol.h>
#include <radio/radioschedule.h>
#include <utils/utilsprint.h>
#include <utils/utilsfstorage.h>

using namespace iot;
using namespace iot::ble;
using namespace iot::ble::gap;
using namespace iot::radio;
using namespace iot::utils;
/**
 * @brief The Application::Private class
 */

#define LIGHT_DIMMER p23
#define STATUS_PERIOD 5.0f
#define STEP_DIMMER 0.7f

#define LIGHT_DIMMER_LEVELS 100
#define LIGHT_DIMMER_DURATION 1.0f
#define LIGHT_DIMMER_INTERVAL (LIGHT_DIMMER_DURATION / (LIGHT_DIMMER_LEVELS))
static const char lamp[] = "LMP";
static const char dimmed[] = "DIM";
static uint8_t groupID[6] = {0,0,0,0,0,0x0D};
namespace iot {

class Application::Private
{
public:
    typedef float Level;
    /**
     * @brief The Status class
     */
    class Status: public core::Status
    {
    public:
        Status(const core::Status::Resets &resets = 0):
            core::Status(resets) {
        }

        virtual ~Status() {
        }

    public:
        virtual core::Status::Period period() const {
            return STATUS_PERIOD;
        }

        virtual core::Status::Device device() const {
            return core::Status::AccessDoor;
        }

        virtual const char *descrition() const {
            return &lamp[0];
        }
    };

    /**
     * @brief The Dim class
     */
    class Dim: public ble::Request
    {
    public:
        Dim() {
        }
        virtual ~Dim() {

        }

        virtual ble::Message::Command command() const {
            return ble::Message::LightDimmer;
        }

        inline uint8_t level() const {
            return _level;
        }
    private:
        virtual int parseRequest(const uint8_t *details, uint8_t size) {
            if (size < sizeof(uint8_t))
                return -5;
            memcpy(&_level, details, sizeof(uint8_t));
            return 0;
        }
    private:
        uint8_t _level;
    };


    /**
     * @brief The Group class
     */
    class Group: public ble::Request
    {
    public:
        Group() {

        }

        virtual ~Group() {

        }


        virtual ble::Message::Command command() const {
            return ble::Message::LightGroupId;
        }
        inline const uint8_t * id() const {
            return _id;
        }

    private:

        virtual int parseRequest(const uint8_t *details, uint8_t size) {
            if (size < BLE_ADDRESS_SIZE)
                return -5;
            _id = details;
            return 0;
        }

    private:
        const uint8_t * _id;
    };

    /**
     * @brief The Reply class
     */
    class Reply: public ble::Reply
    {
    public:
        Reply(const Dim &dim):
            ble::Reply(dim) {

        }

        virtual ~Reply() {

        }

        virtual const char *descrition() const {
            return &dimmed[0];
        }
    private:
        virtual uint8_t _size() const {
            return 0;
        }

        virtual void fill(const ble::Request &request,
                          uint8_t *details) const {
        }

    };

    /**
     * @brief The Command class
     */
    class Command: public core::Command
    {
    public:
        Command(Private &app):
            _app(app) {

        }

        virtual ~Command() {

        }

        virtual void execute() {
            _app.dim();
        }

    private:
        Private &_app;
    };


    /**
     * @brief Private
     */
    Private():
        _dimmer(LIGHT_DIMMER) {
        _dimmer.period_ms(4);
        _dimmer.write(0.7);
        _current = 0.0f;
        _step = 0.0f;
    }

    virtual ~Private() {

    }


    void dim() {
//        _dimmer.write(1.0f - _level/100.0f);

        _timeout.attach(Callback<void()>(this, &Private::timeout),
                        LIGHT_DIMMER_INTERVAL);
    }


    void timeout() {
        if(_level > _current) {
            _step = _level - _current;
            if(_step > STEP_DIMMER){
               _current = _current + STEP_DIMMER;
            }else{
               _current = _current + _step;
            }
//            _dimmer.setLevel(_current/100.0f);
            _dimmer.write(1.0f - _current/100.0f);

            _timeout.attach(Callback<void()>(this, &Private::timeout), 0.01f);
        } else if(_level < _current) {
            _step = _current - _level ;
            if(_step > STEP_DIMMER){
                _current = _current - STEP_DIMMER;
            }else{
                _current = _current - _step;
            }
            _dimmer.write(1.0f - _current/100.0f);
            _timeout.attach(Callback<void()>(this, &Private::timeout), 0.01f);
        }
    }


    void received(System::Rssi rssi,
                  const uint8_t *source,
                  const uint8_t *details,
                  uint8_t size) {
        PRINTF("received\r\n", _level);
        uint8_t level;
        int error = _group->parse(details, size);

        if (error == 0) {
            if (memcmp(_group->address(), _address, BLE_ADDRESS_SIZE) == 0)
                memcpy(groupID, _group->id(), BLE_ADDRESS_SIZE);

            return;
        }

        error = _dim->parse(details, size);
        if (error != 0) {
            return;
        }

        const uint8_t *address = _dim->address();
        if (memcmp(address, _address, BLE_ADDRESS_SIZE) != 0 &&
            memcmp(address, groupID, BLE_ADDRESS_SIZE) != 0) {
#if defined(DEBUG_UARTPRINT) && (DEBUG_UARTPRINT == 1)
            PRINTF("Address mot match: ");
            for(uint8_t index = 0; index < 5; index++)
                PRINTF("%02X:", address[index]);
            PRINTF("%02X\r\n", address[5]);
#endif
            return;
        }

        level = _dim->level();
        if (level > 100)
            level = 100;

        if (_level == (Level) level)
            return;

        _level = (Level) level;
        PRINTF("Dim level: %2.3f\r\n", _level);
        Application::instance().schedule(_command);
    }

    void advertised(const System::Id &id, bool rejected) {

    }


    int initialize(ble::System &system) {


        ble::System::Received received(this, &Private::received);
        ble::System::Advertised advertised(this, &Private::advertised);
        _address = system.address();
#if defined(DEBUG_UARTPRINT) && (DEBUG_UARTPRINT == 1)
        PRINTF("Address: ");
        for(uint8_t index = 0; index < 5; index++)
            PRINTF("%02X:", _address[index]);
        PRINTF("%02X\r\n", _address[5]);
#endif
        int error = system.start(180, received, 64, advertised);

        if (error != 0) {
            PRINTF("*application* scan starting...failed with error: %d\r\n",
                   error);
            return error;
        }

        PRINTF("*application* scan starting...successed\r\n");

        _dim = new Dim;
        _group = new Group;
        _reply = new Reply(*_dim);
        _command = new Command(*this);

        return 0;
    }

    void release(ble::System &system) {
        system.stop();
        delete _command;
        _command = NULL;
        delete _reply;
        _reply = NULL;
        delete _group;
        _group = NULL;
        delete _dim;
        _dim = NULL;
    }


private:
    PwmOut _dimmer;
    Level _current;
    Level _step;
    Level _level;
    Dim *_dim;
    Group *_group;
    Reply *_reply;
    Command *_command;
    Timeout _timeout;
    Message::Sequence _sequence;
    const uint8_t *_address;
};


/**
 * @brief Application::Application
 */
Application::Application():
    _private(new Private)
{

}

Application::~Application()
{
    delete _private;
}

int Application::initialize(ble::System &system)
{
    PRINTF("*application* initialize\r\n");
    return _private->initialize(system);
}


void Application::release(ble::System &system)
{
    _private->release(system);
}

core::Status &Application::status()
{
    static Private::Status _status;
    return _status;
}

Application &Application::instance()
{
    static Application app;
    return app;
}

} // namespace iot
