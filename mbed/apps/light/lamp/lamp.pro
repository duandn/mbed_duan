TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += ./../../../iot \
               ./../../../mbed-os \
               ./../../../mbed-os/features/FEATURE_BLE \
               ./../../../mbed-os/features/FEATURE_BLE/ble \
               ./../../../mbed-os/targets/TARGET_NORDIC/TARGET_NRF5/sdk/device \
               ./../../../mbed-os/targets/TARGET_NORDIC/TARGET_NRF5/TARGET_MCU_NRF51822_UNIFIED/sdk/softdevice/s130/headers \
               ./../../../mbed-os/targets/TARGET_NORDIC/TARGET_NRF5/sdk/libraries/experimental_section_vars \
               ./../../../mbed-os/targets/TARGET_NORDIC/TARGET_NRF5/sdk/libraries/fstorage \
               ./../../../mbed-os/targets/TARGET_NORDIC/TARGET_NRF5/sdk/libraries/fsd \
               ./../../../mbed-os/targets/TARGET_NORDIC/TARGET_NRF5/sdk/libraries/util \
               ./../../../mbed-os/targets/TARGET_NORDIC/TARGET_NRF5/TARGET_MCU_NRF51822_UNIFIED/TARGET_NRF51_DK

DEFINES += -std=gnu++98 \
           -fno-rtti \
           -Wvla \
           NRF_DFU_SETTINGS_VERSION=1 \
           DEVICE_ERROR_PATTERN=1 \
           NRF51 \
           BOARD_N5DK1 \
           NRF51_SDK_VERSION_12_0 \
           __MBED__=1 \
           TARGET_LIKE_MBED \
           TARGET_NRF51822 \
           DEVICE_PORTINOUT=1 \
           __MBED_CMSIS_RTOS_CM \
           DEVICE_LOWPOWERTIMER=1 \
           DEVICE_RTC=1 \
           TOOLCHAIN_object \
           __CMSIS_RTOS \
           TARGET_MCU_NRF51822_UNIFIED \
           TOOLCHAIN_GCC \
           TARGET_CORTEX_M \
           ARM_MATH_CM0 \
           TARGET_UVISOR_UNSUPPORTED \
           TARGET_NRF5 \
           FEATURE_BLE=1 \
           TARGET_M0 \
           NRF51822 \
           TARGET_MCU_NRF51 \
           TARGET_MCU_NRF51_UNIFIED \
           DEVICE_INTERRUPTIN=1 \
           __CORTEX_M0 \
           DEVICE_I2C=1 \
           DEVICE_PORTOUT=1 \
           DEVICE_SERIAL=1 \
           DEVICE_SERIAL_FC=1 \
           DEVICE_SERIAL_ASYNCH=1 \
           DEVICE_SPI_ASYNCH=1 \
           TARGET_FF_ARDUINO \
           DEVICE_PORTIN=1 \
           TARGET_RELEASE \
           TARGET_NORDIC \
           MBED_BUILD_TIMESTAMP=1491335357.89 \
           BLE_STACK_SUPPORT_REQD \
           TARGET_MCU_NRF51_32K_UNIFIED \
           TARGET_NRF51_DK \
           DEVICE_SLEEP=1 \
           TOOLCHAIN_GCC_ARM \
           TARGET_MCU_NRF51822 \
           TARGET_MCU_NORDIC_32K \
           SOFTDEVICE_PRESENT \
           DEVICE_SPI=1 \
           DEVICE_SPISLAVE=1 \
           DEVICE_ANALOGIN=1 \
           DEVICE_PWMOUT=1 \
           S130 \
           TARGET_LIKE_CORTEX_M0 \
           TARGET_MCU_NRF51_32K \
           TWI_ENABLED=1 \
           NDEBUG \
           DEBUG_UARTPRINT=1 \
           MBED_CONF_NORDIC_NRF_LF_CLOCK_SRC=NRF_LF_SRC_XTAL \
           MBED_CONF_NSAPI_PRESENT=1 \
           MBED_CONF_FILESYSTEM_PRESENT=1 \
           MBED_CONF_PLATFORM_STDIO_CONVERT_NEWLINES=0 \
           MBED_CONF_PLATFORM_STDIO_BAUD_RATE=115200 \
           MBED_CONF_EVENTS_PRESENT=1 \
           MBED_CONF_RTOS_PRESENT=1 \
           MBED_CONF_NORDIC_UART_HWFC=0 \
           MBED_CONF_PLATFORM_DEFAULT_SERIAL_BAUD_RATE=115200 \
           MBED_CONF_PLATFORM_STDIO_FLUSH_AT_EXIT=1

SOURCES += main.cpp \
    application.cpp

HEADERS += \
    application.h

DISTFILES += \
    nrf51/Makefile
