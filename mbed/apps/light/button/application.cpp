#include "application.h"
#include <ble/blesystem.h>
#include <ble/bleevent.h>
#include <core/corestatus.h>
#include <devices/devicesbutton.h>
#include <devices/devicessettings.h>
#include <radio/radiocontrol.h>
#include <radio/radioschedule.h>
#include <utils/utilsprint.h>
#include <utils/utilsendian.h>
#include <stddef.h>
#include <BLE.h>

#define BUTTON_PIN p23

using namespace iot;
using namespace iot::devices;
using namespace iot::devices::serial;
using namespace iot::radio;
using namespace iot::ble;
using namespace iot::utils;
#define STATUS_PERIOD 15.0
static const char button[] = "DBT";
static const char push[] = "DBTP";

namespace iot {

/**
 * @brief The Application::Private class
 */
class Application::Private
{
public:
    /**
     * @brief The Status class
     */
    class Status: public core::Status
    {
    public:
        static const char _descrition[];

        Status(const core::Status::Resets &resets = 0):
            core::Status(resets) {

        }

        virtual ~Status() {

        }

    public:
        virtual core::Status::Period period() const {
            return STATUS_PERIOD;
        }

        virtual core::Status::Device device() const {
            return core::Status::AccessButton;
        }

        virtual const char *descrition() const {
            return &button[0];
        }
    };

    class Pushed: public ble::Event
    {
    public:
        Pushed() {

        }

        virtual ~Pushed() {

        }

        virtual ble::Message::Command command() const {
            return ble::Message::LightButton;
        }

        virtual const char *descrition() const {
            return &push[0];
        }

    private:

        virtual uint8_t _size() const {
            return 0;
        }

        virtual void _fill(uint8_t *details) const {

        }
    };


    Private(ble::System &system):
        _system(system) {
    }

    ~Private() {

    }

    void advertised(const Controller::Id &id, bool rejected) {

    }


    void down() {
        _system.advertise(_pushed);
    }

    void up() {

    }

    int initialize() {
        return _system.start(1024);
    }

    void release() {
        _system.stop();
    }

private:
    iot::ble::System &_system;
    Pushed _pushed;
};

/**
 * @brief Application::Application
 */
Application::Application()
{

}

Application::~Application()
{
}

int Application::initialize(ble::System &system)
{
    if (_private != NULL)
        return -1;
    _private = new Private(system);
    int error = _private->initialize();
    if (error != 0) {
        delete _private;
        _private = NULL;
        return error;
    }
    return 0;
}


void Application::release(ble::System &system)
{
    if (_private == NULL)
        return;
    _private->release();
    delete _private;
    _private = NULL;
}

core::Status &Application::status()
{
    static Private::Status _status;
    return _status;
}

Application &Application::instance()
{
    static Application app;
    return app;
}
} // namespace iot
