#include "utilswatchdog.h"
#ifdef NRF51
#include <nrf51.h>
#include <nrf51_bitfields.h>
#endif
#ifdef NRF52
#include <nrf52.h>
#include <nrf52_bitfields.h>
#endif
namespace iot {
namespace utils {

void Watchdog::start(uint32_t timeout)
{
    //Configure Watchdog.
    //   a) Pause watchdog while the CPU is halted by the debugger.
    //   b) Keep the watchdog running while the CPU is sleeping.
    NRF_WDT->CONFIG = (WDT_CONFIG_HALT_Pause << WDT_CONFIG_HALT_Pos) |
                      (WDT_CONFIG_SLEEP_Run << WDT_CONFIG_SLEEP_Pos);

    NRF_WDT->CRV = timeout * 32768;             //ca 3 sek. timout
    NRF_WDT->RREN |= WDT_RREN_RR0_Msk;  //Enable reload register 0
    NRF_WDT->TASKS_START = 1;           //Start the Watchdog timer
}

void Watchdog::reset()
{
    NRF_WDT->RR[0] = WDT_RR_RR_Reload;  //Reload watchdog register 0
}

void Watchdog::stop()
{
    NRF_WDT->TASKS_START = 0;           //Start the Watchdog timer
}
} // namespace utils
} // namespace iot
