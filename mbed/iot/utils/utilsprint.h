#ifndef UTILS_UTILSPRINT_H_
#define UTILS_UTILSPRINT_H_

#if defined (NRF_LOG_USES_RTT) && (NRF_LOG_USES_RTT == 1)
#ifdef __cplusplus
extern "C" {
#endif

#include <nrf_log.h>
#ifdef __cplusplus
}
#endif
#define PRINTF_INITIALIZE() log_rtt_init()

#define PRINTF(FORMAT,args...) log_rtt_printf(0, (const char *)FORMAT, ##args)

#elif defined(DEBUG_UARTPRINT) && (DEBUG_UARTPRINT == 1)
#include <mbed.h>
    extern Serial pc;
    #define PRINTF_INITIALIZE()
    #define PRINTF(FORMAT,args...) pc.printf(FORMAT, ##args)
#else
    #define PRINTF_INITIALIZE()
    #define PRINTF(FORMAT,args...)
#endif

#endif // UTILS_UTILSPRINT_H_
