#ifndef UTILSRANDOM_H
#define UTILSRANDOM_H
#include <stdint.h>
namespace iot {
namespace utils {

class random
{
public:
    static uint32_t generate();
};

} // namespace utils
} // namespace iot
#endif // UTILSRANDOM_H
