#ifndef UTILSSTORAGE_H
#define UTILSSTORAGE_H
#include <mbed.h>
#include <stdint.h>
#include <std/stdvector.h>

namespace iot {
namespace utils {

class Storage
{
public:
    typedef std::Vector<uint8_t, uint8_t> Record;
    typedef uint16_t Key;
    typedef uint8_t Size;
    enum Operation {
        Initialize,
        Store,
        Remove
    };

    typedef uint32_t Result;

    typedef Callback<void(Operation opearion, const Result &result)> Completed;

    static int initialize(const Completed &completed);

    static bool initialized();

    static int remove(Key key);

    static int store(Key key, const uint8_t *data, Size size);

    static int restore(Key key, Record &record);

};

} // namespace utils
} // namespace iot

#endif // UTILSSTORAGE_H
