#include <utils/utilsstorage.h>
#include <utils/utilsprint.h>
#include <rtos/Semaphore.h>
#include <mbed.h>
extern "C"
{
#include <fds.h>
}
#define STORAGE_FSD_FILE_ID 0x1824
namespace iot {
namespace utils {
static bool _initialized = false;
static Storage::Completed _completed;

static void fds_evt_handler(fds_evt_t const * const p_fds_evt)
{
    switch (p_fds_evt->id)
    {
    case FDS_EVT_INIT:
        PRINTF("*Storage event* initialization finished with result: %d.\r\n",
               p_fds_evt->result);
        if (p_fds_evt->result == 0)
            _initialized = true;
        _completed.call(Storage::Initialize, p_fds_evt->result);
        break;
    case FDS_EVT_WRITE:
        PRINTF("*Storage event* write finished with result: %d.\r\n",
               p_fds_evt->result);
        _completed.call(Storage::Store, p_fds_evt->result);
        break;
    case FDS_EVT_UPDATE:
        PRINTF("*Storage event* update finished with result: %d.\r\n",
               p_fds_evt->result);
        _completed.call(Storage::Store, p_fds_evt->result);
        break;
    case FDS_EVT_DEL_RECORD:
        PRINTF("*Storage event* delete record finished with result: %d.\r\n",
               p_fds_evt->result);
        _completed.call(Storage::Remove, p_fds_evt->result);
        break;
    case FDS_EVT_DEL_FILE:
        PRINTF("*Storage event* delete file finished with result: %d.\r\n",
               p_fds_evt->result);

        break;
    default:
        break;
    }
}

int Storage::initialize(const Completed &completed)
{
    _completed = completed;
    ret_code_t ret = fds_register(fds_evt_handler);
    if (ret != FDS_SUCCESS) {
        PRINTF("*Storage initialize* register failed with error: %d\r\n", ret);
        return ret;
    }
    ret = fds_init();
    if (ret != FDS_SUCCESS) {
        PRINTF("*Storage initialize* init failed with error: %d\r\n", ret);
        return ret;
    }
    return 0;
}

bool Storage::initialized()
{
    return _initialized;
}

int Storage::remove(Key key)
{
    fds_record_desc_t record_desc;
    fds_find_token_t    token;
    ret_code_t error = fds_record_find(STORAGE_FSD_FILE_ID,
                                       key,
                                       &record_desc,
                                       &token);
    if (error != FDS_SUCCESS) {
        PRINTF("*Storage* remove failed with error: %d\r\n", error);
        return error;
    }
    return fds_record_delete(&record_desc);

}

int Storage::store(Key key, const uint8_t *data, Size size)
{
    fds_record_t record;
    fds_record_desc_t record_desc;
    fds_record_chunk_t record_chunk;
    // Set up data.
    record_chunk.p_data = data;
    record_chunk.length_words = size / sizeof(uint16_t) +
            size % sizeof(uint16_t);
    // Set up record.
    record.file_id                  = STORAGE_FSD_FILE_ID;
    record.key               = key;
    record.data.p_chunks     = &record_chunk;
    record.data.num_chunks   = 1;
    ret_code_t error = fds_record_write(&record_desc, &record);
    if (error != FDS_SUCCESS) {
        PRINTF("*Storage store* write failed with error: %d\r\n", error);
        return error;
    }
    PRINTF("*Storage store* write successed\r\n", error);
    return 0;
}

int Storage::restore(Key key, Record &record)
{
    fds_flash_record_t  flash_record;
    fds_record_desc_t record_desc;
    fds_find_token_t    token;
    memset(&token, 0x00, sizeof(fds_find_token_t));
    ret_code_t error = fds_record_find(STORAGE_FSD_FILE_ID,
                                       key,
                                       &record_desc,
                                       &token);

    if (error != FDS_SUCCESS) {
        PRINTF("*Storage restore* find failed with error: %d\r\n", error);
        return error;
    }

    error = fds_record_open(&record_desc, &flash_record);
    if (error != FDS_SUCCESS) {
        PRINTF("*Storage restore* open failed with error: %d\r\n", error);
        return error;
    }

    uint16_t size = flash_record.p_header->tl.length_words * sizeof(uint16_t);
    PRINTF("*Storage restore* record size in bytes: %d\r\n", size);

    record.resize(size);
    memcpy(record.values(), flash_record.p_data, size);
    error = fds_record_close(&record_desc);
    if (error != FDS_SUCCESS) {
        PRINTF("*Storage restore* close failed with error: %d\r\n", error);
        return error;
    }
    error = fds_record_delete(&record_desc);
    if (error != FDS_SUCCESS) {
        PRINTF("*Storage restore* delete failed with error: %d\r\n", error);
        return error;
    }
    return 0;
}

} // namespace utils
} // namespace iot
