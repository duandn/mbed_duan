#include <utils/utilsrandom.h>
#include <utils/utilsprint.h>
#ifdef NRF51
#include <nrf51.h>
#include <nrf51_bitfields.h>
#endif
#ifdef NRF52
#include <nrf52.h>
#include <nrf52_bitfields.h>
#endif
namespace iot {
namespace utils {

uint32_t random::generate()
{
//    NRF_RNG->EVENTS_VALRDY = 0;
//    NRF_RNG->TASKS_START = 1;
//    while (NRF_RNG->EVENTS_VALRDY == 0) {
//        // Do nothing.
//    }
//    uint32_t value = NRF_RNG->VALUE;
//    NRF_RNG->TASKS_STOP = 1;
    return 0xFFFFFFFF;
}

} // namespace utils
} // namespace iot
