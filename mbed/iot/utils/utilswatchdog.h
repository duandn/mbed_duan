#ifndef WATCHDOG_H
#define WATCHDOG_H
#include <stdint.h>
namespace iot {
namespace utils {

class Watchdog
{
public:
    /**
     * @brief start start the watchdog timer
     * @param timeout timeout in second
     */
    static void start(uint32_t timeout);

    /**
     * @brief reset reset the watchdog timer
     */
    static void reset();

    static void stop();
};
} // namespace utils
} // namespace iot

#endif // WATCHDOG_H
