#ifndef UTILSENDIAN_H
#define UTILSENDIAN_H
#include <mbed.h>
#include <stdint.h>
namespace iot {
namespace utils {
namespace endian {

template <class T>
static __INLINE T toBig(const T &value)
{
    T result = 0;
    const uint8_t *source = (const uint8_t *) &value;
    uint8_t *target = (uint8_t *)&result;

    for (uint32_t index = 0; index < sizeof (T); index++)
        target[sizeof (T) - index - 1] = source[index];

    return result;
}

template <class T>
static __INLINE T toLittle(const T &value)
{
    T result = 0;
    const uint8_t *source = (const uint8_t *) &value;
    uint8_t *target = (uint8_t *)&result;

    for (uint32_t index = 0; index < sizeof (T); index++)
        target[sizeof (T) - index - 1] = source[index];

    return result;
}
static __INLINE float littleToBig(float little)
{
    uint8_t *littleBytes = (uint8_t *) &little;
    float big;
    uint8_t *bigBytes = (uint8_t *) &big;
    bigBytes[0] = littleBytes[3];
    bigBytes[1] = littleBytes[2];
    bigBytes[2] = littleBytes[1];
    bigBytes[3] = littleBytes[0];
    return big;
}
} // endian
} // utils
} // iot

#endif // UTILSENDIAN_H
