#ifndef FSTORAGE_H
#define FSTORAGE_H
#include <stdint.h>
extern "C"
{
#include <fstorage.h>
}
namespace iot {
namespace utils {

class fstorage
{
public:
    static void init();

    static uint8_t read();

    static uint8_t write(uint8_t data);
private:

};
} // namespace utils
} // namespace iot

#endif // FSTORAGE_H
