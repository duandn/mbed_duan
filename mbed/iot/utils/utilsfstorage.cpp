#include <utils/utilsprint.h>
#include <utils/utilsfstorage.h>

namespace iot {
namespace utils {

static fs_config_t *p;
uint32_t start;
uint32_t stop;

static void fs_evt_handler(fs_evt_t const * const evt, fs_ret_t result)
{
    if (result != FS_SUCCESS) {
        PRINTF("Handle error\n\r");
    } else {
        if(evt->id == FS_EVT_STORE) {
            PRINTF("Storage command successfully\n\r");
        } else {
            PRINTF("Erase command successfully\n\r");
        }
    }
}

void fstorage::init()
{
    FS_REGISTER_CFG(fs_config_t fs_config) = {
            &start,
            &stop,
            fs_evt_handler, // Function for event callbacks.
            1,      // Number of physical flash pages required.
            0xFE};            // Priority for flash usage.
    p = &fs_config;
    fs_ret_t ret = fs_init();
    if (ret != FS_SUCCESS) {
        PRINTF("Init error\n\r");
    }
}

uint8_t fstorage::read()
{
    uint32_t flash_data;
    PRINTF("Reading from flash address 0x%X (hex): ",
           (uint32_t)(*p).p_start_addr);
    flash_data = *((*p).p_start_addr);
    PRINTF("%d \r\n", flash_data);

    return (uint8_t)flash_data;
}
uint8_t fstorage::write(uint8_t data)
{

    uint32_t newData = (uint32_t) data & 0x000000FF;
    static uint32_t data_temp;
    if (newData == data_temp) return 1;
    else data_temp = newData;
    // Erase one page (page 0).
    fs_ret_t ret = fs_erase(p, (*p).p_start_addr, 1);
    if (ret != FS_SUCCESS) {
        PRINTF("Store error\n\r");

    }

    PRINTF("Writing data %D to address 0x%X\r\n", data_temp, (uint32_t)(*p).p_start_addr);
    ret = fs_store(p, (*p).p_start_addr, &data_temp, 1);  //Write data to memory address 0x0003F00. Check it with command: nrfjprog --memrd 0x0003F000 --n 16
    if (ret != FS_SUCCESS) {
        PRINTF("Store error\n\r");
    }
    return 0;
}

} // namespace utils
} // namespace iot
