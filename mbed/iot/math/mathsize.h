#ifndef MATHSIZE_H
#define MATHSIZE_H

namespace iot {
namespace math {
template <typename Value>
class Size
{
public:
    Size(const Size<Value> &size):
        _width(size.width()),
        _height(size.height()) {

    }

    Size(const Value &width = 0, const Value &height = 0):
        _width(width),
        _height(height) {

    }

    inline const Value &width() const {
        return _width;
    }

    inline const Value &height() const {
        return _height;
    }

    inline void setWidth(const Value &width) {
        _width = width;
    }

    inline void setHeight(const Value &height) {
        _height = height;
    }

    Size<Value> &operator = (const Size<Value> &size) {
        _width = size.width();
        _height = size.height();
        return *this;
    }

    Size<Value> &operator *= (const Value &value) {
        _width *= value;
        _height *= value;
        return *this;
    }

    Size<Value> &operator /= (const Value &value) {
        _width /= value;
        _height /= value;
        return *this;
    }

private:
    Value _width;
    Value _height;
};

} // namespace math
} // namespace iot

#endif // MATHSIZE_H
