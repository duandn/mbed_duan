#ifndef MATHRECT_H
#define MATHRECT_H

#include <math/mathpoint.h>
#include <math/mathsize.h>
namespace iot {
namespace math {
template<typename Value>
class Rect
{
public:
    Rect(const Value &column,
         const Value &row,
         const Value &width,
         const Value &height):
        _position(column, row),
        _size(width, height) {

    }

    Rect(const Point<Value> &position = Point<Value>(),
         const Size<Value> &size = Size<Value>()):
        _position(position),
        _size(size) {

    }

    inline const Point<Value> &position() const {
        return _position;
    }

    inline const Value &column() const {
        return _position.column();
    }

    inline void setColumn(const Value &column) {
        _position.setColumn(column);
    }

    inline const Value &row() const {
        return  _position.row();
    }
    inline void setRow(const Value &row) {
        _position.setRow(row);
    }

    inline void setPosition(const Point<Value> &position) {
        _position = position;
    }

    inline void setPosition(const Value &column, const Value &row) {
        _position.setColumn(column);
        _position.setRow(row);
    }

    inline Rect<Value> &operator = (const Rect<Value> &rect) {
        _size = rect.size();
        _position = rect.position();
        return *this;
    }

    inline const Size<Value> &size() const {
        return _size;
    }


    inline void setSize(const Size<Value> &size) {
        _size = size;
    }

    inline void setSize(const Value &width, const Value &height) {
        _size.setWidth(width);
        _size.setHeight(height);
    }

    inline void setWidth(const Value &width) {
        _size.setWidth(width);
    }

    inline const Value &width() const {
        return _size.width();
    }

    inline void ssetHeight(const Value &height) {
        _size.setHeight(height);
    }

    inline const Value &height() const {
        return _size.height();
    }
private:
    Point<Value> _position;
    Size<Value> _size;
};

} // namespace math
} // namespace iot

#endif // MATHRECT_H
