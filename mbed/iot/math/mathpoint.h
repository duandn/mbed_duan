#ifndef COREPOINT_H
#define COREPOINT_H

namespace iot {
namespace math {

template <typename Value>
class Point
{
public:
    Point(const Point<Value> &point):
        _column(point.column()),
        _row(point.row()) {

    }

    Point(const Value &column = 0, const Value &row = 0):
        _column(column),
        _row(row) {

    }

    inline const Value &column() const {
        return _column;
    }

    inline const Value &row() const {
        return _row;
    }

    inline void setColumn(const Value &column) {
        _column = column;
    }

    inline void setRow(const Value &row) {
        _row = row;
    }

    inline Point<Value> &operator = (const Point<Value> &point) {
        _column = point.column();
        _row = point.row();
        return *this;
    }

    inline Point<Value> &operator *= (const Value &value) {
        _column *= value;
        _row *= value;
        return *this;
    }

    inline Point<Value> &operator += (const Value &value) {
        _column += value;
        _row += value;
        return *this;
    }

    inline Point<Value> &operator /= (const Value &value) {
        _column /= value;
        _row /= value;
        return *this;
    }

    inline Point<Value> &operator -= (const Value &value) {
        _column -= value;
        _row -= value;
        return *this;
    }

    Point<Value> operator + (const Point<Value> &point) const {
        return Point<Value>(_column + point.column(), _row + point.row());
    }

private:
    Value _column;
    Value _row;
};

} // namespace core
} // namespace iot
#endif // MATHPOINT_H
