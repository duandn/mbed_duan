#ifndef CORECOMMAND_H
#define CORECOMMAND_H

namespace iot {
namespace core {

/**
 * @brief The Command class
 */
class Command
{
public:
    typedef int Id;

    Command():
        _id(0) {

    }

    virtual ~Command() {

    }

    inline const Id &id() const {
        return _id;
    }

    inline void setId(const Id &id) {
        _id = id;
    }

    virtual void execute() = 0;

private:
    Id _id;
};

} // namespace core
} // namespace iot

#endif // CORECOMMAND_H
