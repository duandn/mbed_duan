#ifndef COREAPPLICATION_H
#define COREAPPLICATION_H

namespace iot {
namespace core {

class Command;
class System;
class Status;
class Application
{
public:

    Application();

    virtual ~Application();

    int exec();

    int store();

    int schedule(Command *command);

    int schedule(Command *command, int period);

    void cancel(int id);

    void exit();

    static Application *instance();

public:
    virtual int initialize() = 0;

    virtual int systems() const = 0;

    virtual System *system(int system) = 0;

    virtual void release() = 0;

private:

    virtual float watchdog() const;

    virtual Status &status() = 0;

    virtual void notify(const Status &status) = 0;

private:
    friend class Private;
    class Private;
    Private *_private;
};

} // namespace core
} // namespace iot

#endif // COREAPPLICATION_H
