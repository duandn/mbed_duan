#ifndef CORESTATUS_H
#define CORESTATUS_H
#include <stdint.h>
namespace iot {
namespace core {

class Status
{
public:
    enum Devices {
        Unspecificed = 0xFFFF,
        Scanner = 0x0000,
        Advertiser = 0x0001,
        AccessDoor = 0x0011,
        AccessButton = 0x0012,
        AccessReader = 0x1200,
        AccessKeypad = 0x1100,
        LightButton = 0x2100,
        LightSensor = 0x2200,
        LightLamp = 0x0021,
        LightDimmer = 0x0022,
        ActivePir = 0x4400,
        ActiveThermometer = 0x4500,
        Opt3100 = 0x3000,
        ToolAdvertiser = 0x0002
    };

    typedef float Period;
    typedef uint16_t Device;
    typedef uint8_t Energy;
    typedef uint32_t Resets;
    typedef uint32_t Token;
    typedef float Timestamp;


    Status(const Status::Resets &resets = 0);

    virtual ~Status();

    int update();

    void setTimestamp(const Timestamp &timestamp);

    const Timestamp &timestamp() const;

    void setResets(const Resets &resets);

    const Resets &resets() const;

    uint8_t size() const;

    void fill(uint8_t *details) const;

    int parse(const uint8_t *details, uint8_t size);

public:
    virtual Period period() const;

    virtual Device device() const;

    virtual const char *descrition() const;

private:
    virtual uint8_t _size() const;

    virtual void _fill(uint8_t *details) const;

    virtual int _update();

    virtual int _parse(const uint8_t *details, uint8_t size);
private:
    friend class Private;
    class Private;
    Private *_private;

};

} // namespace core
} // namespace iot

#endif // CORESTATUS_H
