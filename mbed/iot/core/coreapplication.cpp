#include <core/coreapplication.h>
#include <core/corecommand.h>
#include <core/corestatus.h>
#include <core/coresystem.h>
#include <utils/utilsprint.h>
#include <utils/utilsstorage.h>
#include <utils/utilswatchdog.h>
#include <events/EventQueue.h>
#include <mbed.h>
#include <BLE.h>
#ifdef BLE_DUMP
#include <nrf_ble.h>
#include <nrf_soc.h>
#endif
#define STATUS_KEY 0x2017
#define WATCHDOG_TIMEOUT 5.0f

using namespace iot::utils;
namespace iot {
namespace core {
class Application::Private
{
public:
    Private(Application *instance):
        _running(false),
        _queue(EVENTS_QUEUE_SIZE) {
        _instance = instance;
    }

    void watchdog() {
//        PRINTF("*core application* schedule reset watchdog\r\n");
        _queue.call(Callback<void()>(this, &Private::onWatchdog));
    }

    void onWatchdog() {
        utils::Watchdog::reset();
//        PRINTF("*core application* reset watchdog\r\n");
    }

    void update() {
        _queue.call(Callback<void()>(this, &Private::onUpdate));
    }

    void onUpdate() {
        Status &status = _instance->status();
        status.update();
        _instance->notify(_instance->status());
    }

    int store() {
        if (!Storage::initialized())
            return -1;
        Status &status = _instance->status();
        Storage::Record record;
        record.resize(status.size());
        status.fill(record.values());
        Storage::store(STATUS_KEY, record.values(), record.size());
        return 0;
    }

    void completed(Storage::Operation operation,
                   const Storage::Result &result) {
        PRINTF("*core application* storage completed with operation: %d"
               " and result: %d\r\n",
               operation,
               result);

        if (result != 0) {
            return;
        }

        if (operation == Storage::Store) {
            return;
        }

        if (operation == Storage::Remove) {
            return;
        }

        Storage::Record record;
        Status &status = _instance->status();
        status.setResets(1);

        if (Storage::restore(STATUS_KEY, record) == 0) {
            PRINTF("*core application* parsing..");
            int error = status.parse(record.values(), record.size());
            if (error == 0) {
                status.setResets(status.resets() + 1);
                PRINTF("completed\r\n");
            } else {
                PRINTF("failed with error: %d\r\n", error);
            }
        }

        PRINTF("*core application* storage resets: %d\r\n", (int)status.resets());
        record.resize(status.size());
        status.fill(record.values());
        Storage::store(STATUS_KEY, record.values(), record.size());
    }

    int exec() {
        _running = true;

        int error = _instance->initialize();
        if (error != 0)
            return error;

        float notify = _instance->status().period();
        if (notify > 0) {
            _notify.attach(Callback<void()>(this, &Private::update),
                           notify);
        }

        float watchdog= _instance->watchdog();
        if (watchdog > 0) {
            _watchdog.attach(Callback<void()>(this, &Private::watchdog),
                             watchdog * 0.5);
            utils::Watchdog::start(watchdog);
        }

        Storage::initialize(Storage::Completed(this, &Private::completed));

        while (_running) {
            _queue.dispatch();
        }
        if (watchdog > 0)
            _watchdog.detach();
        PRINTF("*core application* exit\r\n");
        utils::Watchdog::reset();
        return 0;
    }

    int schedule(Command *command) {
        int id = _queue.call(command, &Command::execute);
        command->setId(id);
        return id;
    }

    int schedule(Command *command, int period) {

        return _queue.call_every(period, command, &Command::execute);
    }

    void cancel(int id) {
        _queue.cancel(id);
    }

    void exit() {
        _running = false;
        _queue.break_dispatch();
    }

    static Application *instance() {
        return _instance;
    }

private:
    bool _running;
    events::EventQueue _queue;
    Ticker _watchdog;
    Ticker _notify;
    static Application *_instance;
};

Application *Application::Private::_instance = NULL;
/**
 * @brief Application::Application
 */
Application::Application():
    _private(NULL)
{

}

Application::~Application()
{

}

int Application::exec()
{
    if (_private != NULL)
        return -1;
    _private = new Private(this);
    int error;
    int systems = this->systems();
    for(int index = 0; index < systems; index ++) {

        System *subsystem = this->system(index);
        error = subsystem->initialize();
        if (error == 0)
            continue;

        for(int previous = 0; previous < index; previous++)
            this->system(previous)->release();

        delete _private;
        _private = NULL;

        return error;
    }


    error = _private->exec();
    release();
    for(int index = 0; index < systems; index++)
        system(index)->release();

    delete _private;
    _private = NULL;
    return error;
}

int Application::schedule(Command *command)
{
    return _private->schedule(command);
}

int Application::store()
{
    return _private->store();
}

int Application::schedule(Command *command, int period)
{
    return _private->schedule(command, period);
}

void Application::cancel(int id)
{
    _private->cancel(id);
}

void Application::exit()
{
    _private->exit();
}

float Application::watchdog() const
{
    return WATCHDOG_TIMEOUT;
}


Application *Application::instance()
{
    return Private::instance();
}
} // namespace core
} // namespace iot

#ifdef TWI_LOG
#ifdef __cplusplus
extern "C" {
#endif

void nrf_drv_twi_xfer_log()
{
    PRINTF("nrf_drv_twi_xfer_log\r\n");
}

void nrf_drv_twi_interrupt_log()
{
    PRINTF("nrf_drv_twi_interrupt_log\r\n");
}

void SPI0_TWI0_IRQHandler_log()
{
    PRINTF("SPI0_TWI0_IRQHandler_log\r\n");
}

#ifdef __cplusplus
}
#endif
#endif // TWI_LOG

#ifdef LOWPOWER_DUMP
#ifdef __cplusplus
extern "C" {
#endif

//void common_rtc_irq_handler_dump()
//{
//    PRINTF("common_rtc_irq_handler\r\n");
//}

//void os_tick_irqack_dump()
//{
//    PRINTF("os_tick_irqack_dump\r\n");
//}

#ifdef __cplusplus
}
#endif
#endif //LOWPOWER_DUMP

#ifdef EQUEUE_DUMP
#ifdef __cplusplus
extern "C" {
#endif
extern uint64_t volatile hal_sleep_count;

void equeue_dispatch_wait_dump()
{
    PRINTF("*equeue dispatch* wait sleep count: %ld\r\n", hal_sleep_count);
}

void equeue_dispatch_dispatch_events_dump()
{
    PRINTF("*equeue dispatch* dispatch event\r\n");
}

void equeue_dispatch_signaled_dump()
{
    PRINTF("*equeue dispatch* signaled sleep count: %ld\r\n", hal_sleep_count);
}

#ifdef __cplusplus
}
#endif
#endif //EQUEUE_DUMP

#ifdef BLE_DUMP
#ifdef __cplusplus
extern "C" {
#endif
void softdevice_events_execute_event_dump(uint32_t event_id)
{
    switch(event_id) {
    case NRF_EVT_HFCLKSTARTED:                         /**< Event indicating that the HFCLK has started. */
        PRINTF("*softdevice events execute* event: NRF_EVT_HFCLKSTARTED\r\n");
        break;

    case NRF_EVT_POWER_FAILURE_WARNING:                /**< Event indicating that a power failure warning has occurred. */
        PRINTF("*softdevice events execute* event: NRF_EVT_POWER_FAILURE_WARNING\r\n");
        break;
    case NRF_EVT_FLASH_OPERATION_SUCCESS:              /**< Event indicating that the ongoing flash operation has completed successfully. */
        PRINTF("*softdevice events execute* event: NRF_EVT_FLASH_OPERATION_SUCCESS\r\n");
        break;
    case NRF_EVT_FLASH_OPERATION_ERROR:                /**< Event indicating that the ongoing flash operation has timed out with an error. */
        PRINTF("*softdevice events execute* event: NRF_EVT_FLASH_OPERATION_ERROR\r\n");
        break;
    case NRF_EVT_RADIO_BLOCKED:                        /**< Event indicating that a radio timeslot was blocked. */
        PRINTF("*softdevice events execute* event: NRF_EVT_RADIO_BLOCKED\r\n");
        break;
    case NRF_EVT_RADIO_CANCELED:                       /**< Event indicating that a radio timeslot was canceled by SoftDevice. */
        PRINTF("*softdevice events execute* event: NRF_EVT_RADIO_CANCELED\r\n");
        break;
    case NRF_EVT_RADIO_SIGNAL_CALLBACK_INVALID_RETURN: /**< Event indicating that a radio timeslot signal callback handler return was invalid. */
        PRINTF("*softdevice events execute* event: NRF_EVT_RADIO_SIGNAL_CALLBACK_INVALID_RETURN\r\n");
        break;
    case NRF_EVT_RADIO_SESSION_IDLE:                   /**< Event indicating that a radio timeslot session is idle. */
        PRINTF("*softdevice events execute* event: NRF_EVT_RADIO_SESSION_IDLE\r\n");
        break;
    case NRF_EVT_RADIO_SESSION_CLOSED:                 /**< Event indicating that a radio timeslot session is closed. */
        PRINTF("*softdevice events execute* event: NRF_EVT_RADIO_SESSION_CLOSED\r\n");
        break;
    }

}

void softdevice_events_execute_ble_event_dump(ble_evt_t *event)
{
    PRINTF("*softdevice events execute* ble event: %d\r\n", event->header.evt_id);
}

#ifdef ANT_STACK_SUPPORT_REQD
void softdevice_events_execute_ant_event_dump(ant_evt_t *event)
{
}
#endif

#ifdef __cplusplus
}
#endif
#endif //BLE_DUMP
