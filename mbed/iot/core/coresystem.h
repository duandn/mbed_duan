#ifndef CORESYSTEM_H
#define CORESYSTEM_H

namespace iot {
namespace core {
class System
{
public:
    virtual ~System() {}
    virtual int initialize() = 0;
    virtual void release() = 0;
};
} // namespace core
} // namespace iot
#endif // CORESYSTEM_H
