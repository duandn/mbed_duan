#include <core/corestatus.h>
#include <std/stdvector.h>
#include <utils/utilsendian.h>
#include <utils/utilsprint.h>
#include <utils/utilsrandom.h>

using namespace iot::utils;

namespace iot {
namespace core {

#define STATUS_HEADER_SIZE (sizeof(Status::Device) + \
                            sizeof(Status::Token) + \
                            sizeof(Status::Energy) + \
                            sizeof(Status::Resets))

/**
 * @brief The Status::Private class
 */
class Status::Private
{
public:
    typedef std::Vector<uint8_t, uint8_t> Record;
    Private(const Status::Resets &resets):
        _token(0),
        _resets(resets),
        _timestamp(0) {

    }

    inline const Status::Token &token() const {
        return _token;
    }

    inline void setTimestamp(const Status::Timestamp &timestamp) {
        _timestamp = timestamp;
    }

    inline const Status::Timestamp &timestamp() {
        return _timestamp;
    }

    inline void setResets(const Status::Resets &resets) {
        _resets = resets;
    }

    const Status::Resets &resets() const {
        return _resets;
    }

    int update(Status &status) {
        _token = random::generate();
        return status._update();
    }

    void fill(uint8_t *details, const Status &status) const {

        Status::Device device = endian::toBig<Status::Device>(status.device());
        memcpy(details, &device, sizeof(Status::Device));

        Status::Token token = endian::toBig<Status::Token>(_token);
        memcpy(details + sizeof(Status::Device),
               &token,
               sizeof(Status::Token));

        Status::Energy energy = 0xFF;

        memcpy(details +
               sizeof(Status::Device) +
               sizeof(Status::Token),
               &energy,
               sizeof(Status::Energy));

        Status::Resets resets = endian::toBig<Status::Resets>(_resets);

        memcpy(details +
               sizeof(Status::Device) +
               sizeof(Status::Token) +
               sizeof(Status::Energy),
               &resets,
               sizeof(Status::Resets));

        status._fill(details + STATUS_HEADER_SIZE);
    }

    int parse(const uint8_t *details, uint8_t size, Status &status) {

        if (size < STATUS_HEADER_SIZE)
            return -1;

        int error = status._parse(details + STATUS_HEADER_SIZE,
                                  size + STATUS_HEADER_SIZE);
        if (error != 0)
            return error;

        Status::Device device;
        memcpy(&device, details, sizeof(Status::Device));
        if (endian::toLittle(device) != status.device())
            return -1;

        memcpy(&_token,
               details + sizeof(Status::Device),
               sizeof(Status::Token));

        _token = endian::toLittle(_token);

        Status::Energy energy;
        memcpy(&energy,
               details +
               sizeof(Status::Device) +
               sizeof(Status::Token),
               sizeof(Status::Energy));

        memcpy(&_resets,
               details +
               sizeof(Status::Device) +
               sizeof(Status::Token) +
               sizeof(Status::Energy),
               sizeof(Status::Resets));
        _resets = endian::toLittle(_resets);
        return 0;
    }

private:
    Status::Token _token;
    Status::Resets _resets;
    Status::Timestamp _timestamp;

    Record _record;
};

/**
 * @brief Status::Status
 */
Status::Status(const Status::Resets &resets):
    _private(new Private(resets))
{

}

Status::~Status()
{
    delete _private;
}

void Status::setTimestamp(const Timestamp &timestamp)
{
    _private->setTimestamp(timestamp);
}

const Status::Timestamp &Status::timestamp() const
{
    return _private->timestamp();
}

int Status::update()
{
    return _private->update(*this);
}

void Status::setResets(const Resets &resets)
{
    _private->setResets(resets);
}

const Status::Resets &Status::resets() const
{
    return _private->resets();
}

Status::Period Status::period() const
{
    return 0.0f;
}

Status::Device Status::device() const
{
    return Status::Unspecificed;
}

const char *Status::descrition() const
{
    return NULL;
}

uint8_t Status::size() const
{
    return STATUS_HEADER_SIZE + _size();
}

void Status::fill(uint8_t *details) const
{
    _private->fill(details, *this);
}


int Status::parse(const uint8_t *details, uint8_t size)
{
    return _private->parse(details, size, *this);
}

uint8_t Status::_size() const
{
    return 0;
}

void Status::_fill(uint8_t *details) const
{

}

int Status::_update()
{
    return 0;
}

int Status::_parse(const uint8_t *details, uint8_t size)
{
    return 0;
}

} // namespace core
} // namespace iot
