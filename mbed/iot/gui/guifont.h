#ifndef GUIFONT_H
#define GUIFONT_H

#include <mbed.h>
#include <gui/guiimage.h>

namespace iot {
namespace gui {

/** Font class.
 *  Used to access font data in an external table.
 */
class Font
{
public:
    typedef unsigned int Color;
    typedef short Size;
    /** Create a white Font object
     *
     * @param table Pointer to the font table.
     */
    Font(const char* table);

    /** Create a Font object with the specified color
     *
     * @param table Pointer to the font table.
     * @param color Color for the font as a 32-bit ARGB value.
     */
    Font(const char* table, unsigned int color);

    /** Get the glyph Image for the specified character
     *
     * @returns The character glyph Image.
     */
    bitmap::Image glyph(char charater) const;

    /** Get the current Font color
     *
     * @returns The current Font color as a 32-bit ARGB value.
     */
    inline const Color &color() {
        return _color;
    }

    /** Set the Font color
     *
     * @param c The new Font color as a 32-bit ARGB value.
     */
    inline void setColor(const Color &color) {
        _color = color;
    }

    /** Get the height of the Font glyphs
     *
     * @returns The height of the Font glyphs.
     */
    inline Size height() const {
        return _fonts[1];

    }

    /** Measures the width of a string in pixels if drawn with this font
     *
     * @param string Pointer to the string to measure.
     *
     * @returns The width of the string in pixels.
     */
    Size measureString(const char *string) const;

    /** Measures the width of the next word
     *  in a string (separated by whitespace) in pixels if drawn with this font
     *
     * @param str Pointer to the string to measure.
     *
     * @returns The width of the next word in pixels.
     */
    Size measureWord(const char* str) const;

protected:
    const char *_fonts;
    unsigned int _color;
};

} // namespace gui
} // namespace iot

#endif // GUIFONT_H
