#ifndef GUIIMAGE_H
#define GUIIMAGE_H

#include "mbed.h"

namespace iot {
namespace gui {

/** Image abstract class.
 *  Used as a base class for image objects.
 */
class Image
{
public:
    typedef unsigned int Color;
    typedef short Size;
    /** Create an Image object with the specified width and height
     *
     * @param width The image width.
     * @param height The image height.
     */
    Image(Size width, Size height);
    virtual ~Image();

    /** Get the pixel at the specified coordinates
    *
    * @param column The X coordinate.
    * @param row The Y coordinate.
    *
    * @returns The color of the pixel as a 32-bit ARGB value.
    */
    virtual Color pixel(Size column, Size row) const = 0;

    /** Get the image width
     *
     * @returns The image width.
     */
    inline Size width() const {
        return _width;
    }

    /** Get the image height
     *
     * @returns The image height.
     */
    inline Size height() const {
        return _height;
    }

protected:
    //The image width/height
    Size _width;
    Size _height;
};

namespace bitmap {
/** BitmapImage class.
 *  Used to access a monochrome bitmap stored in an external table.
 */
class Image : public gui::Image
{
public:
    /** Create a BitmapImage object from the specified image table with
     *  a white foreground, and an alpha background
     *
     * @param bitmap Pointer to the image table.
     */
    Image(const char *bitmap);

    /** Create a BitmapImage object from the specified image table with
     *  the specified foreground, and an alpha background
     *
     * @param bitmap Pointer to the image table.
     * @param foreground The foreground color as a 32-bit ARGB value.
     */
    Image(const char *bitmap, const gui::Image::Color &foreground);

    /** Create a BitmapImage object from the specified image table with
     *  the specified foreground and background colors
     *
     * @param bitmap Pointer to the image table.
     * @param foreground The foreground color as a 32-bit ARGB value.
     * @param background The background color as a 32-bit ARGB value.
     */
    Image(const char *bitmap,
          const gui::Image::Color &foreground,
          const gui::Image::Color &background);

    /** Get the pixel at the specified coordinates
    *
    * @param x The X coordinate.
    * @param y The Y coordinate.
    *
    * @returns The color of the pixel as a 32-bit ARGB value.
    */
    virtual gui::Image::Color pixel(Size x, Size y) const;

    /** Get the foreground color
    *
    * @returns The foreground color as a 32-bit ARGB value.
    */
    inline const gui::Image::Color &foreground() const {
        return _foreground;
    }

    /** Set the foreground color
    *
    * @param c The new foreground color as a 32-bit ARGB value.
    */
    inline void setForeground(unsigned int foreground) {
        _foreground = foreground;
    }

    /** Get the background color
    *
    * @returns The background color as a 32-bit ARGB value.
    */
     inline const gui::Image::Color &background() const {
         return _background;
     }

    /** Set the background color
    *
    * @param c The new background color as a 32-bit ARGB value.
    */
    inline void setBackground(const gui::Image::Color &background) {
        _background = background;
    }

protected:
    //Pointer to the image table
    const char *_bitmap;

    //The foreground color
    gui::Image::Color _foreground;

    //The background color
    gui::Image::Color _background;

    Size bytesPerLine() const;
};

}

} // namespace gui
} // namespace iot

#endif
