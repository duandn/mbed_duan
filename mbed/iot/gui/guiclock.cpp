#include <gui/guiclock.h>
#include <gui/guidigits.h>
#include <gui/guipaint.h>
#include <utils/utilsprint.h>
namespace iot {
namespace gui {
#define HOUR_MAXIMUM 100
#define MINUTE_MAXIMUM 60
#define SECOND_MAXIMUM 60
#define SECOND_PER_MINUTE 60
#define SECOND_PER_HOUR 3600
class Clock::Private
{
public:
    Private(Clock *clock):
        _selected(Clock::Minute),
        _time(5 * SECOND_PER_MINUTE),
        _gap(8),
        _hours(2, Rect(), clock),
        _minutes(2, Rect(), clock),
        _seconds(2, Rect(), clock),
        _background(0xFF000000),
        _foreground(0xFFFFFFFF)
    {
        Clock::Time hours = _time / SECOND_PER_HOUR;
        Clock::Time minutes = (_time % SECOND_PER_HOUR) / SECOND_PER_MINUTE;
        Clock::Time seconds = _time % SECOND_PER_MINUTE;
        PRINTF("Selected: %d, Time: %d (%d:%d:%d)\r\n",
               (int) _selected,
               (int) _time,
               hours,
               minutes,
               seconds);
        _hours.setValue(hours);
        _minutes.setValue(minutes);
        _seconds.setValue(seconds);
    }

    void setSelected(const Clock::Selected &selected) {
        switch (selected) {
        case Clock::Hour:

            _hours.setBackground(_foreground);
            _hours.setForeground(_background);

            _minutes.setBackground(_background);
            _minutes.setForeground(_foreground);

            _seconds.setBackground(_background);
            _seconds.setForeground(_foreground);
            break;
        case Clock::Minute:
            _hours.setBackground(_background);
            _hours.setForeground(_foreground);

            _minutes.setBackground(_foreground);
            _minutes.setForeground(_background);

            _seconds.setBackground(_background);
            _seconds.setForeground(_foreground);
            break;
        case Clock::Second:
            _hours.setBackground(_background);
            _hours.setForeground(_foreground);

            _minutes.setBackground(_background);
            _minutes.setForeground(_foreground);

            _seconds.setBackground(_foreground);
            _seconds.setForeground(_background);
            break;
        case Clock::None:
            _hours.setBackground(_background);
            _hours.setForeground(_foreground);

            _minutes.setBackground(_background);
            _minutes.setForeground(_foreground);

            _seconds.setBackground(_background);
            _seconds.setForeground(_foreground);
        }
        _selected = selected;
    }

    inline const Clock::Selected &selected() const {
        return _selected;
    }

    inline void setTime(const Clock::Time &time) {
        _time = time;
        Clock::Time hours = _time / SECOND_PER_HOUR;
        Clock::Time minutes = (_time % SECOND_PER_HOUR) / SECOND_PER_MINUTE;
        Clock::Time seconds = _time % SECOND_PER_MINUTE;
        _hours.setValue(hours);
        _minutes.setValue(minutes);
        _seconds.setValue(seconds);
    }

    const Clock::Time &time() const {
        return _time;
    }

    void reset() {
        _time = 5 * SECOND_PER_MINUTE;
        _hours.setValue(0);
        _minutes.setValue(5);
        _seconds.setValue(0);
    }

    int increase() {
        Clock::Time hours = _time / SECOND_PER_HOUR;
        Clock::Time minutes = (_time % SECOND_PER_HOUR) / SECOND_PER_MINUTE;
        Clock::Time seconds = _time % SECOND_PER_MINUTE;
        PRINTF("Selected: %d, Time: %d (%d:%d:%d)\r\n",
               (int) _selected,
               (int) _time,
               hours,
               minutes,
               seconds);
        switch (_selected) {
        case Clock::Hour:
            hours = (hours + 1) % HOUR_MAXIMUM;
            break;
        case Clock::Minute:
            minutes = (minutes + 1) % MINUTE_MAXIMUM;
            break;
        case Clock::Second:
            seconds = (seconds + 1) % SECOND_MAXIMUM;
            break;
        default:
            return -1;
        }
        _time = hours * SECOND_PER_HOUR + minutes * SECOND_PER_MINUTE + seconds;
        _hours.setValue(hours);
        _minutes.setValue(minutes);
        _seconds.setValue(seconds);
        return 0;
    }

    int decrease() {
        Clock::Time hours = _time / SECOND_PER_HOUR;
        Clock::Time minutes = (_time % SECOND_PER_HOUR) / SECOND_PER_MINUTE;
        Clock::Time seconds = _time % SECOND_PER_MINUTE;
        switch (_selected) {
        case Clock::Hour:
            hours = (hours + HOUR_MAXIMUM - 1) % HOUR_MAXIMUM;
            break;
        case Clock::Minute:
            minutes = (minutes + MINUTE_MAXIMUM - 1) % MINUTE_MAXIMUM;
            break;
        case Clock::Second:
            seconds = (seconds + SECOND_MAXIMUM - 1) % SECOND_MAXIMUM;
            break;
        default:
            return -1;
        }
        _time = hours * SECOND_PER_HOUR + minutes * SECOND_PER_MINUTE + seconds;
        _hours.setValue(hours);
        _minutes.setValue(minutes);
        _seconds.setValue(seconds);
        return 0;
    }

    inline void setGap(const Clock::Gap &gap) {
        _gap = gap;
    }

    inline const Clock::Gap &gap() const {
        return _gap;
    }

    void draw(const Rect &rect, Painter &painter) const {
        short column = rect.width() / 3 - 1;

        short row = rect.height() / 2 - 6;
        painter.fillRect(column, row, 3, 3, _foreground);

        row = rect.height() / 2 + 6;
        painter.fillRect(column, row, 3, 3, _foreground);

        column = rect.width() * 2 / 3;

        row = rect.height() / 2 - 6;
        painter.fillRect(column, row, 3, 3, _foreground);

        row = rect.height() / 2 + 6;
        painter.fillRect(column, row, 3, 3, _foreground);

        _hours.paint(painter);
        _minutes.paint(painter);
        _seconds.paint(painter);
    }

    void layout(const Rect &rect) {
        short column = 0;
        short row = 0;
        short width = (rect.width() - 2 * _gap) / 3;
        short height = rect.height();

        _hours.setRect(Rect(column, row, width, height));
        column += width + _gap;

        _minutes.setRect(Rect(column, row, width, height));
        column += width + _gap;

        _seconds.setRect(Rect(column, row, width, height));
    }

private:
    Clock::Selected _selected;
    Clock::Time _time;
    Clock::Gap _gap;
    Digits _hours;
    Digits _minutes;
    Digits _seconds;
    Color _background;
    Color _foreground;

};

/**
 * @brief Clock::Clock
 * @param rect
 * @param parent
 */
Clock::Clock(const Rect &rect, Widget *parent):
    Widget(rect, parent),
    _private(new Private(this))
{

}

Clock::~Clock()
{
    delete _private;
}

void Clock::setSelected(const Selected &selected)
{
    _private->setSelected(selected);
}

const Clock::Selected &Clock::selected() const
{
    return _private->selected();
}

void Clock::setTime(const Time &time)
{
    _private->setTime(time);
}

void Clock::reset()
{
    _private->reset();
}

int Clock::increase()
{
    return _private->increase();
}

int Clock::decrease()
{
    return _private->decrease();
}

const Clock::Time &Clock::time() const
{
    return _private->time();
}
const Clock::Gap &Clock::gap() const
{
    return _private->gap();
}

void Clock::setGap(const Gap &gap)
{
    _private->setGap(gap);
}

Clock::Selected Clock::next(const Clock::Selected &selected)
{
    switch (selected) {
    case Hour:
        return Minute;
    case Minute:
        return Second;
    case Second:
        return Hour;
    default:
        return Second;
    }
}

void Clock::draw(const Rect &rect, Painter &painter) const
{
    _private->draw(rect, painter);
}

void Clock::layout(const Rect &rect)
{
    _private->layout(rect);
}

} // namespace gui
} // namespace iot
