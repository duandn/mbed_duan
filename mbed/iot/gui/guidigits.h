#ifndef GUIDIGITS_H
#define GUIDIGITS_H
#include <gui/guiwidget.h>
namespace iot {
namespace gui {
class Digits: public Widget
{
public:
    typedef unsigned int Value;
    typedef unsigned short Cells;
    typedef unsigned short Gap;
    typedef unsigned short Thickness;

    Digits(Cells cells = 2, const Rect &rect = Rect(), Widget *parent = NULL);

    virtual ~Digits();

    void setValue(const Value &value);

    const Value &value() const;

    void setCells(const Cells &cells);

    const Cells &cells() const;

    void setGap(const Gap &gap);

    const Gap &gap() const;

    const Thickness &thickness() const;

    void setThickness(const Thickness &thickness);

    void setBackground(const Color &background);

    const Color &background() const;

    void setForeground(const Color &foreground);

    const Color &foreground() const;

private:
    virtual void draw(const Rect &rect, Painter &painter) const;
private:
    class Private;
    Private *_private;
};

} // namespace gui
} // namespace iot

#endif // GUIDIGITS_H
