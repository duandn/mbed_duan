#ifndef GUICLOCK_H
#define GUICLOCK_H
#include <gui/guiwidget.h>
namespace iot {
namespace gui {
class Clock: public Widget
{
public:
    typedef uint64_t Time;
    enum Selected {
        Hour,
        Minute,
        Second,
        None
    };

    typedef short Gap;
    /**
     * @brief Clock
     * @param rect
     * @param parent
     */
    Clock(const Rect &rect = Rect(), Widget *parent = NULL);

    /**
     * @brief setSelected
     * @param selected
     */
    void setSelected(const Selected &selected);

    /**
     * @brief selected
     * @return
     */
    const Selected &selected() const;

    /**
     * @brief setTime
     * @param time
     */
    void setTime(const Time &time);

    /**
     * @brief reset
     */
    void reset();

    /**
     * @brief increase
     */
    int increase();

    /**
     * @brief decrease
     */
    int decrease();

    /**
     * @brief time
     * @return
     */
    const Time &time() const;

    /**
     * @brief ~Clock
     */
    virtual ~Clock();

    /**
     * @brief gap
     * @return The gap
     */
    const Gap &gap() const;

    /**
     * @brief setGap
     * @param gap
     */
    void setGap(const Gap &gap);

    /**
     * @brief next
     * @param selected
     * @return
     */
    static Selected next(const Selected &selected);


private:
    /**
     * @brief draw
     * @param rect
     * @param painter
     */
    virtual void draw(const Rect &rect, Painter &painter) const;

    /**
     * @brief layout
     * @param rect
     */
    virtual void layout(const Rect &rect);
private:
    class Private;
    Private *_private;
};
} // namespace gui
} // namespace iot

#endif // GUICLOCK_H
