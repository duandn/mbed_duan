#include <gui/guiimage.h>
namespace iot {
namespace gui {

Image::Image(Size width, Size height):
    _width(width),
    _height(height)
{
}

Image::~Image()
{

}

namespace bitmap {

Image::Image(const char *bitmap):
    gui::Image(bitmap[0] * 0xFF + bitmap[1], bitmap[2] * 0xFF + bitmap[3]),
    _bitmap(bitmap),
    _foreground(0xFFFFFFFF),
    _background(0x00000000)

{
}

Image::Image(const char *bitmap,
             const Color &foreground):
    gui::Image(bitmap[0] * 0xFF + bitmap[1], bitmap[2] * 0xFF + bitmap[3]),
    _bitmap(bitmap),
    _foreground(foreground),
    _background(0x00000000)
{
}

Image::Image(const char* bitmap,
             const Color &foreground,
             const Color &background):
    gui::Image(bitmap[0] * 0xFF + bitmap[1], bitmap[2] * 0xFF + bitmap[3]),
    _bitmap(bitmap),
    _foreground(foreground),
    _background(background)
{
}

gui::Image::Color Image::pixel(Size column, Size row) const
{
    Size addr;
    Size mask;
    Size bpl;

    //Range check the pixel request
    if (column < 0 || row < 0 || column >= width() || row >= height())
        return 0x000000;

    //Get the pixel address
    bpl = bytesPerLine();
    mask = 0x80 >> (column % 8);
    addr = 4 + row * bpl + (column / 8);

    //Return the appropriate color
    if (_bitmap[addr] & mask)
        return _foreground;
    else
        return _background;
}

Image::Size Image::bytesPerLine() const
{
    Size bpl;

    //Determine the bytes per line
    bpl = width() / 8;
    if ((width() % 8) != 0) bpl++;

    //Return the bytes per line
    return bpl;
}

}
} // namespace gui
} // namespace iot
