#include <gui/guifont.h>

namespace iot {
namespace gui {
Font::Font(const char *fonts):
    _fonts(fonts),
    _color(0xFFFFFFFF)
{
}

Font::Font(const char *fonts, unsigned int color):
    _fonts(fonts),
    _color(color)
{
}

bitmap::Image Font::glyph(char charater) const
{
    char found = charater;
    while (1) {
        //Skip the height and numChars bytes
        const char *ptr = _fonts + 2;

        //Search for the character in the font table
        for(char index = 0; index < _fonts[0]; index++) {
            if (*ptr == found) {
                //Return the image
                return bitmap::Image(_fonts + ptr[1] * 0xFF + ptr[2], _color);
            }
            ptr += 3;
        }

        //The character wasn't found, replace it with a space
        found = ' ';
    }
}

Font::Size Font::measureString(const char *string) const
{
    Size index = 0;
    Size width= 0;

    //Find the length in pixels of the whole string
    while (string[index] != 0) {
        width += glyph(*string).width();
        index++;
    }

    return width;
}

Font::Size Font::measureWord(const char *string) const
{
    int index = 0;
    Size width = 0;

    //Find the length in pixels of the next word (separated by whitespace)
    while (string[index] > ' ' && string[index] <= 0x7E) {
        width += glyph(*string).width();
        index++;
    }

    return width;
}
} // namespace gui
} // namespace iot
