#ifndef GUISUBSYSTEM_H
#define GUISUBSYSTEM_H
#include <core/coresystem.h>
namespace iot {
namespace gui {

class Subsystem: public core::System
{
public:
    System();
    virtual ~System();
private:
    virtual int initialize();
    virtual void release();
};
} // namespace gui
} // namespace iot
#endif // GUISUBSYSTEM_H
