#ifndef GUIPAINT_H
#define GUIPAINT_H
#include <gui/guidefs.h>
namespace iot {
namespace gui {
class Font;
class Image;
class Painter
{
public:
    /**
     * @brief Create a painter object with the specified width and height
     * @param width The painter width.
     * @param height The painter height.
     */
    Painter(short width, short height);


    virtual ~Painter();
    /**
     * @brief Draw a single pixel at the specified coordinates
     * @param column The X coordinate.
     * @param row The Y coordinate.
     * @param color The color of the pixel as a 32-bit ARGB value.
     */
    virtual void drawPixel(short column, short row, const Color &color) = 0;

    /**
     * @brief Update content
     */
    virtual int flush() = 0;

    /**
     * @brief Fill the entire painter
     *  with the specified color using a filled rectangle
     * @param color The color to fill with as a 32-bit ARGB value (black by default).
     */
    virtual void clear(const Color &color = 0xFF000000);

    /**
     * @brief Draw a line between the specified coordinates using Bresenham's line algorithm
     * @param x0 The starting X coordinate.
     * @param y0 The starting Y coordinate.
     * @param x1 The ending X coordinate.
     * @param y1 The ending Y coordinate.
     * @param color The color of the line as a 32-bit ARGB value.
     */
    virtual void drawLine(short x0,
                          short y0,
                          short x1,
                          short y1,
                          const Color &color);

    /**
     * @brief Draw a line between the specified coordinates using Bresenham's line algorithm
     * @param first The first point.
     * @param second The second.
     * @param color The color of the line as a 32-bit ARGB value.
     */
    void drawLine(const Point &first,
                  const Point &second,
                  const Color &color);

    /**
     * @brief Draw a fast horizontal line of the specified width, at the specified coordinates
     * @param column The starting X coordinate.
     * @param row The starting Y coordinate.
     * @param width The width of the line.
     * @param color The color of the line as a 32-bit ARGB value.
     */
    virtual void drawHLine(short column,
                           short row,
                           short width,
                           const Color &color);

    /**
     * @brief Draw a fast vertical line of the specified height,
     *  at the specified coordinates
     * @param column The starting X coordinate.
     * @param row The starting Y coordinate.
     * @param height The height of the line.
     * @param color The color of the line as a 32-bit ARGB value.
     */
    virtual void drawVLine(short column,
                           short row,
                           short height,
                           const Color &color);

    /**
     * @brief Draw an unfilled rectangle of the specified width and height,
     *  at the specified coordinates using fast lines
     * @param column The starting X coordinate.
     * @param row The starting Y coordinate.
     * @param width The width of the rectangle.
     * @param height The height of the rectangle.
     * @param color The color of the rectangle as a 32-bit ARGB value.
     */
    virtual void drawRect(short column,
                          short row,
                          short width,
                          short height,
                          const Color &color);

    /**
     * @brief Draw an unfilled rectangle of the specified rect,
     *  at the specified coordinates using fast lines
     * @param rect The rect to draw.
     * @param color The color of the rectangle as a 32-bit ARGB value.
     */
    virtual void drawRect(const Rect &rect,  const Color &color);
    /**
     * @brief Draw a filled rectangle of the specified width and height,
     *  at the specified coordinates using fast lines
     * @param column The starting X coordinate.
     * @param row The starting Y coordinate.
     * @param width The width of the rectangle.
     * @param height The height of the rectangle.
     * @param color The color of the rectangle as a 32-bit ARGB value.
     */
    virtual void fillRect(short column,
                          short row,
                          short width,
                          short height,
                          const Color &color);

    /**
     * @brief Draw a filled rectangle of the specified rect,
     *  at the specified coordinates using fast lines
     * @param rect The rect to fill.
     * @param color The color of the rectangle as a 32-bit ARGB value.
     */
    virtual void fillRect(const Rect &rect,  const Color &color);

    /**
     * @brief Draw an unfilled triangle with the specified vertices using lines
     * @param x0 The first vertex X coordinate.
     * @param y0 The first vertex Y coordinate.
     * @param x1 The second vertex X coordinate.
     * @param y1 The second vertex Y coordinate.
     * @param x2 The third vertex X coordinate.
     * @param y2 The third vertex Y coordinate.
     * @param color The color of the triangle as a 32-bit ARGB value.
     */
    void drawTriangle(short x0,
                      short y0,
                      short x1,
                      short y1,
                      short x2,
                      short y2,
                      const Color &color);

    /**
     * @brief Draw a filled triangle with the specified vertices using Adafruit's algorithm
     * @param x0 The first vertex X coordinate.
     * @param y0 The first vertex Y coordinate.
     * @param x1 The second vertex X coordinate.
     * @param y1 The second vertex Y coordinate.
     * @param x2 The third vertex X coordinate.
     * @param y2 The third vertex Y coordinate.
     * @param color The color of the triangle as a 32-bit ARGB value.
     */
    void fillTriangle(short x0,
                      short y0,
                      short x1,
                      short y1,
                      short x2,
                      short y2,
                      const Color &color);

    /**
     * @brief Draw an unfilled circle of the specified radius,
     *  at the specified coordinates using the midpoSize circle algorithm
     * @param column The center X coordinate.
     * @param row The center Y coordinate.
     * @param radius The radius of the circle.
     * @param color The color of the circle as a 32-bit ARGB value.
     */
    void drawCircle(short column, short row, short r, const Color &color);

    /**
     * @brief Draw a filled circle of the specified radius,
     *  at the specified coordinates
     *
     * @param column The center X coordinate.
     * @param row The center Y coordinate.
     * @param radius The radius of the circle.
     * @param color The color of the circle as a 32-bit ARGB value.
     */
    void fillCircle(short column, short row, short radius, const Color &color);

    /**
     * @brief Draw an unfilled rounded rectangle of the specified width, height,
     *  and corner radius, at the specified coordinates
     * @param column The starting X coordinate.
     * @param row The starting Y coordinate.
     * @param width The width of the rectangle.
     * @param height The height of the rectangle.
     * @param radius The radius of the corners.
     * @param color The color of the rectangle as a 32-bit ARGB value.
     */
    void drawRoundRect(short column,
                       short row,
                       short width,
                       short height,
                       short radius,
                       const Color &color);

    /**
     * @brief Draw a filled rounded rectangle of the specified width, height,
     *  and corner radius, at the specified coordinates
     * @param column The starting X coordinate.
     * @param row The starting Y coordinate.
     * @param width The width of the rectangle.
     * @param height The height of the rectangle.
     * @param radius The radius of the corners.
     * @param color The color of the rectangle as a 32-bit ARGB value.
     */
    void fillRoundRect(short column,
                       short row,
                       short width,
                       short height,
                       short radius,
                       const Color &color);

    /**
     * @brief Draw an Image object at the specified coordinates
     * @param img Pointer to the image to draw.
     * @param column The starting X coordinate.
     * @param row The starting Y coordinate.
     */
    void drawImage(const Image &image, short column, short row);

    /**
     * @brief Draw a character at the specified coordinates without wrapping
     * @param character The character to draw.
     * @param font Pointer to the font to draw with.
     * @param column The starting X coordinate.
     * @param row The starting Y coordinate.
     *
     * @returns The width of the drawn character.
     */
    short drawChar(char character, const Font &font, short column, short row);

    /**
     * @brief Draw a string at the specified coordinates without wrapping
     * @param str Pointer to the string to draw.
     * @param fnt Pointer to the font to draw with.
     * @param column The starting X coordinate.
     * @param row The starting Y coordinate.
     */
    void drawString(const char *string,
                    const Font &font,
                    short column,
                    short row);

    /**
     * @brief Draw a string within the specified rectangle with wrapping
     * @param str Pointer to the string to draw.
     * @param fnt Pointer to the font to draw with.
     * @param column The starting X coordinate.
     * @param row The starting Y coordinate.
     * @param width The width of the bounding rectangle.
     * @param height The height of the bounding rectangle.
     */
    void drawString(const char* string,
                    const Font &font,
                    short column,
                    short row,
                    short width,
                    short height);

    /**
     * @brief Draw an unfilled canarium of the specified boundary,
     *  at the specified coordinates using fast lines
     * @param rect The boundary rect of the hexagon to draw.
     * @param color The color of the rectangle as a 32-bit ARGB value.
     */
    void fillCanarium(const Rect &rect, const Color &color);

    /**
     * @brief Draw an unfilled canarium of the specified boundary,
     *  at the specified coordinates using fast lines
     * @param column The starting X coordinate.
     * @param row The starting Y coordinate.
     * @param width The width of the rectangle.
     * @param height The height of the rectangle.
     * @param color The color of the rectangle as a 32-bit ARGB value.
     */
    virtual void fillCanarium(short column,
                             short row,
                             short width,
                             short height,
                             const Color &color);


    void fillTopRect(const Rect &rect, const Color &color);

    virtual void fillTopRect(short column,
                             short row,
                             short width,
                             short height,
                             const Color &color);

    void fillLeftRect(const Rect &rect, const Color &color);

    virtual void fillLeftRect(short column,
                              short row,
                              short width,
                              short height,
                              const Color &color);

    void fillBottomRect(const Rect &rect, const Color &color);

    virtual void fillBottomRect(short column,
                                short row,
                                short width,
                                short height,
                                const Color &color);

    void fillRightRect(const Rect &rect, const Color &color);

    virtual void fillRightRect(short column,
                               short row,
                               short width,
                               short height,
                               const Color &color);

    void drawDigit(char digit,
                   short thickness,
                   const Rect &rect,
                   const Color &color);

    virtual void drawDigit(char digit,
                           short thickness,
                           short column,
                           short row,
                           short width,
                           short height,
                           const Color &color);


    /**
     *@brief  Get the width of the painter
     * @returns The width of the painter.
     */
    inline short width() const {
        return _width;
    }

    /**
     * @brief Get the height of the painter
     * @returns The height of the painter.
     */
    inline short height() const {
        return _height;
    }
private:
    //Drawing helper functions
    void drawCircleHelper(short column,
                          short row,
                          short radius,
                          short corner,
                          const Color &color);
    void fillCircleHelper(short column,
                          short row,
                          short radius,
                          short corner,
                          short delta,
                          const Color &color);
private:
    short _width;
    short _height;
};

} // namespace gui
} // namespace iot

#endif // GUIPAINT_H
