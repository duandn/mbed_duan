#include <gui/guipaint.h>
#include <gui/guifont.h>
#include <gui/guiimage.h>
#include <utils/utilsprint.h>
#include <mbed.h>
namespace iot {
namespace gui {
#define abs(a) ((a) > 0? a: -(a))
Painter::Painter(short width, short height):
    _width(width),
    _height(height)
{

}

Painter::~Painter()
{

}

void Painter::clear(const Color &color)
{
    fillRect(0, 0,_width, _height, color);
}

void Painter::drawLine(short x0,
                       short y0,
                       short x1,
                       short y1,
                       const Color &color)
{
    short steep = abs(y1 - y0) > abs(x1 - x0);
    if (steep) {
        short temp = x0;
        x0 = y0;
        y0 = temp;

        temp = x1;
        x1 = y1;
        y1 = temp;
    }

    if (x0 > x1) {
        short temp = x0;
        x0 = x1;
        x1 = temp;

        temp = y0;
        y0 = y1;
        y1 = temp;
    }

    short dx, dy;
    dx = x1 - x0;
    dy = abs(y1 - y0);

    short err = dx / 2;
    short ystep;

    if (y0 < y1) {
        ystep = 1;
    } else {
        ystep = -1;
    }

    for (; x0 <= x1; x0++) {
        if (steep) {
            drawPixel(y0, x0, color);
        } else {
            drawPixel(x0, y0, color);
        }
        err -= dy;
        if (err < 0) {
            y0 += ystep;
            err += dx;
        }
    }
}

void Painter::drawLine(const Point &first,
                       const Point &second,
                       const Color &color)
{
    drawLine(first.column(),
             first.row(),
             second.column(),
             second.row(),
             color);
}

void Painter::drawHLine(short column,
                        short row,
                        short width,
                        const Color &color)
{
    drawLine(column, row, column + width - 1, row, color);
}

void Painter::drawVLine(short column, short row, short height, const Color &color)
{
    drawLine(column, row, column, row + height - 1, color);
}

void Painter::drawRect(short column,
                       short row,
                       short width,
                       short height,
                       const Color &color)
{
    drawHLine(column, row, width, color);
    drawHLine(column, row + height - 1, width, color);
    drawVLine(column, row, height, color);
    drawVLine(column + width - 1, row, height, color);
}

void Painter::drawRect(const Rect &rect,  const Color &color)
{
    const Point &position = rect.position();
    const iot::gui::Size &size = rect.size();
    drawRect(position.column(),
             position.row(),
             size.width(),
             size.height(),
             color);
}

void Painter::fillRect(short column,
                       short row,
                       short width,
                       short height,
                       const Color &color)
{
    for (short i = column; i < column + width; i++)
        drawVLine(i, row, height, color);
}

void Painter::fillRect(const Rect &rect,  const Color &color)
{
    const Point &position = rect.position();
    const iot::gui::Size &size = rect.size();
    fillRect(position.column(),
             position.row(),
             size.width(),
             size.height(),
             color);
}
void Painter::drawTriangle(short x0,
                           short y0,
                           short x1,
                           short y1,
                           short x2,
                           short y2,
                           const Color &color)
{
    drawLine(x0, y0, x1, y1, color);
    drawLine(x1, y1, x2, y2, color);
    drawLine(x2, y2, x0, y0, color);
}

void Painter::fillTriangle(short x0,
                           short y0,
                           short x1,
                           short y1,
                           short x2,
                           short y2,
                           const Color &color)
{
    short a, b, row, last;

    //Sort coordinates by Y order (y2 >= y1 >= y0)
    if (y0 > y1) {
        short temp = y0;
        y0 = y1;
        y1 = temp;

        temp = x0;
        x0 = x1;
        x1 = temp;
    }
    if (y1 > y2) {
        short temp = y1;
        x1 = y2;
        y2 = temp;

        temp = x1;
        x1 = x2;
        x2 = temp;
    }
    if (y0 > y1) {
        short temp = y0;
        y0 = y1;
        y1 = temp;

        temp = x0;
        x0 = x1;
        x1 = temp;
    }

    //Handle awkward all-on-same-line case as its own thing
    if(y0 == y2) {
        a = b = x0;
        if(x1 < a)
            a = x1;
        else if(x1 > b)
            b = x1;
        if(x2 < a)
            a = x2;
        else if(x2 > b)
            b = x2;

        drawHLine(a, y0, b - a + 1, color);

        return;
    }

    short dx01 = x1 - x0;
    short dy01 = y1 - y0;
    short dx02 = x2 - x0;
    short dy02 = y2 - y0;
    short dx12 = x2 - x1;
    short dy12 = y2 - y1;
    short sa = 0;
    short sb = 0;

    //For upper part of triangle, find scanline crossings for segments
    //0-1 and 0-2.  If y1=y2 (flat-bottomed triangle), the scanline y1
    //is included here (and second loop will be skipped, avoiding a /0
    //error there), otherwise scanline y1 is skipped here and handled
    //in the second loop...which also avoids a /0 error here if y0=y1
    //(flat-topped triangle).
    if(y1 == y2)
        last = y1;      //Include y1 scanline
    else
        last = y1 - 1;  //Skip it

    for(row = y0; row <= last; row++) {
        a = x0 + sa / dy01;
        b = x0 + sb / dy02;
        sa += dx01;
        sb += dx02;

        if(a > b) {
            short temp = a;
            a = b;
            b = temp;
        }
        drawHLine(a, row, b - a + 1, color);
    }

    //For lower part of triangle, find scanline crossings for segments
    //0-2 and 1-2.  This loop is skipped if y1=y2.
    sa = dx12 * (row - y1);
    sb = dx02 * (row - y0);
    for(; row <= y2; row++) {
        a = x1 + sa / dy12;
        b = x0 + sb / dy02;
        sa += dx12;
        sb += dx02;

        if(a > b) {
            short temp = a;
            a = b;
            b = temp;
        }
        drawHLine(a, row, b - a + 1, color);
    }
}

void Painter::drawCircle(short column, short row, short radius, const Color &color)
{
    short f = 1 - radius;
    short ddF_column = 1;
    short ddF_row = -2 * radius;
    short i = 0;
    short j = radius;

    drawPixel(column, row + radius, color);
    drawPixel(column, row - radius, color);
    drawPixel(column + radius, row, color);
    drawPixel(column - radius, row, color);

    while(i < j) {
        if(f >= 0) {
            j--;
            ddF_row += 2;
            f += ddF_row;
        }
        i++;
        ddF_column += 2;
        f += ddF_column;
        drawPixel(column + i, row + j, color);
        drawPixel(column - i, row + j, color);
        drawPixel(column + i, row - j, color);
        drawPixel(column - i, row - j, color);
        drawPixel(column + j, row + i, color);
        drawPixel(column - j, row + i, color);
        drawPixel(column + j, row - i, color);
        drawPixel(column - j, row - i, color);
    }
}

void Painter::fillCircle(short column, short row, short radius, const Color &color)
{
    drawVLine(column, row - radius, 2 * radius + 1, color);
    fillCircleHelper(column, row, radius, 3, 0, color);
}

void Painter::drawRoundRect(short column,
                            short row,
                            short width,
                            short height,
                            short radius,
                            const Color &color)
{
    //Draw the four lines
    drawHLine(column + radius,
              row, width - 2 * radius, color);            //Top
    drawHLine(column + radius,
              row + height - 1, width - 2 * radius, color);    //Bottom
    drawVLine(column, row + radius, height - 2 * radius, color); //Left
    drawVLine(column + width - 1,
              row + radius, height - 2 * radius, color); //Right

    //Draw the four corners
    drawCircleHelper(column + radius, row + radius, radius, 1, color);
    drawCircleHelper(column + width - radius - 1,
                     row + radius, radius, 2, color);
    drawCircleHelper(column + width - radius - 1,
                     row + height - radius - 1, radius, 4, color);
    drawCircleHelper(column + radius,
                     row + height - radius - 1,
                     radius, 8, color);
}

void Painter::fillRoundRect(short column,
                            short row,
                            short width,
                            short height,
                            short radius,
                            const Color &color)
{
    //Draw the body
    fillRect(column + radius, row, width - 2 * radius, height, color);

    //Draw the four corners
    fillCircleHelper(column + width - radius - 1,
                     row + radius, radius, 1,
                     height - 2 * radius - 1,
                     color);
    fillCircleHelper(column+radius,
                     row + radius,
                     radius,
                     2,
                     height - 2 * radius - 1,
                     color);
}

void Painter::drawImage(const Image &image, short column, short row)
{
    short height = image.height();
    short width = image.height();
    for (short j = 0; j < height; j++) {
        for (short i = 0; i < width; i++)
            drawPixel(i + column, j + row, image.pixel(i, j));
    }
}

short Painter::drawChar(char character,
                        const Font &font,
                        short column, short row)
{
    //Get the character glyph
    bitmap::Image glyph = font.glyph(character);

    //Draw the character glyph
    drawImage(glyph, column, row);

    //Return the width of the glyph
    return glyph.width();
}

void Painter::drawString(const char *string,
                         const Font &font,
                         short column,
                         short row)
{
    short cursX = 0;
    short cursY = 0;

    while(*string != '\0') {
        //Check for a new line character
        if (*string == '\n') {
            //Go to a new line
            cursX = 0;
            cursY += font.height();

            //We're done for this character
            string++;
            continue;
        }

        //Check for a carriage return character
        if (*string == '\r') {
            //Ignore it, we're done for this character
            string++;
            continue;
        }

        //Draw the character
        cursX += drawChar(*string++, font, column + cursX, row + cursY);
    }
}

void Painter::drawString(const char *string,
                         const Font &font,
                         short column,
                         short row,
                         short width,
                         short height)
{
    short cursX = 0;
    short cursY = 0;

    while(*string != '\0') {
        //Check for a new line character
        if (*string == '\n') {
            //Check if we can fit another line
            if ((cursY + 2 * font.height()) < height) {
                //Yes we can
                cursX = 0;
                cursY += font.height();

                //We're done for this character
                string++;
                continue;
            } else {
                //Nope, we can't prSize any more so return
                return;
            }
        }

        //Check for a carriage return character
        if (*string == '\r') {
            //Ignore it, we're done for this character
            string++;
            continue;
        }

        //Check for entire words first
        if ((*string > ' ') && (*string <= 0x7E)) {
            //Draw entire word on canvas with correct wrapping
            //Size i = 0;
            short wlen;

            //Determine the length of the next word
            wlen = font.measureWord(string);

            //Will the length of the next word exceed the margins?
            if ((wlen + cursX) > width) {
                //Only do a newline if the word will fit on it
                if (wlen <= width) {
                    //Check if we can fit another line
                    if ((cursY + 2 * font.height()) < height) {
                        //Yes we can
                        cursX = 0;
                        cursY += font.height();
                    } else {
                        //Nope, we can't prSize any more so return
                        return;
                    }
                }
            }

            //Put just the word characters on the display up
            // to the next non-whitespace character or the end of the string
            while ((*string > ' ') && (*string <= 0x7E)) {
                //Check if the character will fit on the screen
                if ((font.glyph(*string).width() + cursX) > width) {
                    //Check if we can fit another line
                    if ((cursY + 2 * font.height()) < height) {
                        //Yes we can
                        cursX = 0;
                        cursY += font.height();
                    } else {
                        //Nope, we can't prSize any more so return
                        return;
                    }
                }

                //Draw the character
                cursX += drawChar(*string++, font, column + cursX, row + cursY);
            }
        } else {
            //Check if the character will fit on the screen
            if ((font.glyph(*string).width() + cursX) > width) {
                //Check if we can fit another line
                if ((cursY + 2 * font.height()) < height) {
                    //Yes we can
                    cursX = 0;
                    cursY += font.height();
                } else {
                    //Nope, we can't prSize any more so return
                    return;
                }
            }

            //Draw the character
            cursX += drawChar(*string++, font, column + cursX, row + cursY);
        }
    }
}

void Painter::fillCanarium(const Rect &rect, const Color &color)
{

    const Point &position = rect.position();
    const Size &size = rect.size();
    fillCanarium(position.column(),
                 position.row(),
                 size.width(),
                 size.height(),
                 color);
}

void Painter::fillCanarium(short column,
                           short row,
                           short width,
                           short height,
                           const Color &color)
{
    if (width > height) {
        short half = height >> 1;
        for(short position = 0; position < half; position += 1) {
            short shift = half - position;
            drawHLine(column + shift,
                      row + position,
                      width - (shift << 1),
                      color);
        }

        for(short position = half; position < height; position += 1) {
            short shift = position - half;
            drawHLine(column + shift,
                      row + position,
                      width - (shift << 1),
                      color);
        }


    } else {
        short half = width >> 1;
        for(short position = 0; position < half; position += 1) {
            short shift = half - position;
            drawVLine(column + position,
                      row + shift,
                      height - (shift << 1),
                      color);
        }
        for(short position = half; position < width; position += 1) {
            short shift = position - half;
            drawVLine(column + position,
                      row + shift,
                      height - (shift << 1),
                      color);
        }
    }
}

void Painter::fillTopRect(const Rect &rect, const Color &color)
{
    const Point &position = rect.position();
    const Size &size = rect.size();
    fillTopRect(position.column(),
                position.row(),
                size.width(),
                size.height(),
                color);
}


void Painter::fillTopRect(short column,
                          short row,
                          short width,
                          short height,
                          const Color &color)
{
    if (height * 2 > width)
        return;
    for(short index = 0; index < height; index ++) {
        short left = column + index;
        short length = width - index * 2;
        drawHLine(left, row + index, length, color);
    }
}

void Painter::fillLeftRect(const Rect &rect, const Color &color)
{
    const Point &position = rect.position();
    const Size &size = rect.size();
    fillLeftRect(position.column(),
                 position.row(),
                 size.width(),
                 size.height(),
                 color);

}

void Painter::fillLeftRect(short column,
                           short row,
                           short width,
                           short height,
                           const Color &color)
{
    if (width * 2 > height)
        return;

    fillRect(column, row, width, height, color);

    for(short index = 0; index < width; index ++) {
        short top = row + (width - index);
        short length = height - (width - index) * 2;
        drawVLine(column + index, top, length, color);
    }
}

void Painter::fillBottomRect(const Rect &rect, const Color &color)
{
    const Point &position = rect.position();
    const Size &size = rect.size();
    fillBottomRect(position.column(),
                   position.row(),
                   size.width(),
                   size.height(),
                   color);
}

void Painter::fillBottomRect(short column,
                             short row,
                             short width,
                             short height,
                             const Color &color)
{
    if (height * 2 > width)
        return;

    for(short index = 0; index < height; index ++) {
        short left = column + height - index;
        short length = width - (height - index) * 2;
        drawHLine(left, row + index, length, color);
    }
}

void Painter::fillRightRect(const Rect &rect, const Color &color)
{
    const Point &position = rect.position();
    const Size &size = rect.size();
    fillRightRect(position.column(),
                  position.row(),
                  size.width(),
                  size.height(),
                  color);

}

void Painter::fillRightRect(short column,
                            short row,
                            short width,
                            short height,
                            const Color &color)
{
    if (width * 2 > height)
        return;

//    PRINTF("Rect: (%d, %d, %d, %d)\r\n", column, row, width, height);

    for(short index = 0; index < width; index ++) {
        short top = row + index;
        short length = height - index * 2;
        drawVLine(column + index, top, length, color);
    }
}

void Painter::drawDigit(char digit,
                        short thickness,
                        const Rect &rect,
                        const Color &color)
{
    drawDigit(digit,
              thickness,
              rect.column(),
              rect.row(),
              rect.width(),
              rect.height(),
              color);
}


void Painter::drawDigit(char digit,
                        short thickness,
                        short column,
                        short row,
                        short width,
                        short height,
                        const Color &color)
{
    short margin = thickness >> 2;

    Rect leftTopRect(column,
                     row + margin + 2,
                     thickness,
                     height / 2 - 2 * margin - 2);

    Rect leftBottomRect(column,
                        row + height / 2 + margin + 1,
                        thickness,
                        height / 2 - margin * 2 - 2);

    Rect rightTopRect(column + width - thickness,
                      row + margin + 2,
                      thickness,
                      height / 2 - 2 * margin - 2);

    Rect rightBottomRect(column + width - thickness,
                         row + height / 2 + margin + 1,
                         thickness,
                         height / 2 - 2 * margin - 2);

    Rect topRect(column + margin + 2,
                 row,
                 width - thickness - 1,
                 thickness);
    Rect centerRect(column + margin + 2,
                    row + height / 2 - margin - 1,
                    width - thickness - 1,
                    thickness);

    Rect bottomRect(column + margin + 3,
                    row + height - thickness,
                    width - thickness - 3,
                    thickness);

    switch (digit) {
    case '0':
        fillCanarium(leftTopRect, color);
        fillCanarium(leftBottomRect, color);

        fillCanarium(rightTopRect, color);
        fillCanarium(rightBottomRect, color);

        fillCanarium(topRect, color);
        fillCanarium(bottomRect, color);

        break;
    case '1':
        fillCanarium(rightTopRect, color);
        fillCanarium(rightBottomRect, color);
        break;
    case '2':

        fillCanarium(topRect, color);
        fillCanarium(rightTopRect, color);
        fillCanarium(centerRect, color);
        fillCanarium(leftBottomRect, color);
        fillCanarium(bottomRect, color);


        break;
    case '3':

        fillCanarium(topRect, color);
        fillCanarium(rightTopRect, color);
        fillCanarium(rightBottomRect, color);
        fillCanarium(centerRect, color);
        fillCanarium(bottomRect, color);
        break;
    case '4':

        fillCanarium(leftTopRect, color);
        fillCanarium(centerRect, color);
        fillCanarium(rightTopRect, color);
        fillCanarium(rightBottomRect, color);

        break;
    case '5':

        fillCanarium(topRect, color);
        fillCanarium(leftTopRect, color);
        fillCanarium(centerRect, color);
        fillCanarium(rightBottomRect, color);
        fillCanarium(bottomRect, color);
        break;
    case '6':

        fillCanarium(topRect, color);
        fillCanarium(leftTopRect, color);
        fillCanarium(leftBottomRect, color);
        fillCanarium(centerRect, color);
        fillCanarium(rightBottomRect, color);
        fillCanarium(bottomRect, color);

        break;
    case '7':

        fillCanarium(topRect, color);
        fillCanarium(rightTopRect, color);
        fillCanarium(rightBottomRect, color);

        break;
    case '8':

        fillCanarium(leftTopRect, color);
        fillCanarium(leftBottomRect, color);

        fillCanarium(rightTopRect, color);
        fillCanarium(rightBottomRect, color);

        fillCanarium(topRect, color);
        fillCanarium(centerRect, color);
        fillCanarium(bottomRect, color);

        break;
    case '9':

        fillCanarium(leftTopRect, color);
        fillCanarium(rightTopRect, color);
        fillCanarium(rightBottomRect, color);

        fillCanarium(topRect, color);
        fillCanarium(centerRect, color);
        fillCanarium(bottomRect, color);

        break;
    }
}

void Painter::drawCircleHelper(short column,
                               short row,
                               short radius,
                               short corner,
                               const Color &color)
{
    short f = 1 - radius;
    short ddF_column = 1;
    short ddF_row = -2 * radius;
    short i = 0;
    short j = radius;

    while (i < j) {
        if (f >= 0) {
            j--;
            ddF_row += 2;
            f += ddF_row;
        }
        i++;
        ddF_column += 2;
        f += ddF_column;
        if (corner & 0x4) {
            drawPixel(column + i, row + j, color);
            drawPixel(column + j, row + i, color);
        }
        if (corner & 0x2) {
            drawPixel(column + i, row - j, color);
            drawPixel(column + j, row - i, color);
        }
        if (corner & 0x8) {
            drawPixel(column - j, row + i, color);
            drawPixel(column - i, row + j, color);
        }
        if (corner & 0x1) {
            drawPixel(column - j, row - i, color);
            drawPixel(column - i, row - j, color);
        }
    }
} 

void Painter::fillCircleHelper(short column,
                               short row,
                               short radius,
                               short corner,
                               short delta,
                               const Color &color)
{
    short f = 1 - radius;
    short ddF_column = 1;
    short ddF_row = -2 * radius;
    short i = 0;
    short j = radius;

    while (i < j) {
        if (f >= 0) {
            j--;
            ddF_row += 2;
            f += ddF_row;
        }
        i++;
        ddF_column += 2;
        f += ddF_column;

        if (corner & 0x1) {
            drawVLine(column + i, row - j, 2 * j + 1 + delta, color);
            drawVLine(column + j, row - i, 2 * i + 1 + delta, color);
        }
        if (corner & 0x2) {
            drawVLine(column - i, row - j, 2 * j + 1 + delta, color);
            drawVLine(column - j, row - i, 2 * i + 1 + delta, color);
        }
    }
}


} // namespace gui
} // namespace iot
