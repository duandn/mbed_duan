#include <gui/guiwidget.h>
#include <utils/utilsprint.h>

namespace iot {
namespace gui {
class Widget::Private
{
public:
    Private(const Rect &rect, Widget *parent = NULL):
        _rect(rect),
        _shown(true),
        _parent(parent) {

    }

    inline Widget *parent() {
        return _parent;
    }

    inline const Rect &rect() const {
        return _rect;
    }

    inline void setRect(const Rect &rect) {
        _rect = rect;
    }

    inline bool shown() const {
        return _shown;
    }

    inline void setShown(bool shown) {
        _shown = shown;
    }

private:
    Rect _rect;
    bool _shown;
    Widget *_parent;
};


Widget::Widget(const Rect &rect, Widget *parent):
    _private(new Private(rect, parent))
{

}

Widget::~Widget()
{
    delete _private;
}

Widget *Widget::parent()
{
    return _private->parent();
}

void Widget::paint(Painter &painter) const
{
    draw(_private->rect(), painter);
}

const Rect &Widget::rect() const
{
    return _private->rect();
}
void Widget::setRect(const Rect &rect)
{
    _private->setRect(rect);
    layout(rect);
}

bool Widget::shown() const
{
    return _private->shown();
}

void Widget::setShown(bool shown)
{
    _private->setShown(shown);
}

Point Widget::absolutePosition() const
{
    Widget *parent = _private->parent();
    const Point &position = _private->rect().position();
    if (parent == NULL)
        return position;
    Point point = parent->absolutePosition() + position;
    return parent->absolutePosition() + position;
}

void Widget::draw(const Rect &rect, Painter &painter) const
{

}

void Widget::layout(const Rect &rect)
{

}

} // namespace gui
} // namespace iot
