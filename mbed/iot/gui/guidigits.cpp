#include <gui/guidigits.h>
#include <gui/guipaint.h>
#include <utils/utilsprint.h>
namespace iot {
namespace gui {

class Digits::Private
{
public:
    Private(Digits::Cells cells):
        _cells(cells),
        _gap(2),
        _thickness(3),
        _background(0xFF000000),
        _foreground(0xFFFFFFFF),
        _value(0) {

    }

    inline void setValue(const Digits::Value &value) {
        _value = value;
    }

    inline const Digits::Value &value() const {
        return _value;
    }

    inline void setCells(const Digits::Cells &cells) {
        _cells = cells;
    }

    inline const Digits::Cells &cells() const {
        return _cells;
    }

    inline void setGap(const Digits::Gap &gap) {
        _gap = gap;
    }

    inline const Digits::Gap &gap() const {
        return _gap;
    }

    inline void setThickness(const Digits::Thickness &thickness) {
        _thickness = thickness;
    }

    inline const Digits::Thickness &thickness() const {
        return _thickness;
    }

    inline void setBackground(const Color &background) {
        _background = background;
    }

    inline const Color &background() const {
        return _background;
    }

    inline void setForeground(const Color &foreground) {
        _foreground = foreground;
    }

    inline const Color &foreground() const {
        return _foreground;
    }

    void draw(const Digits &widget,
              const Rect &rect,
              Painter &painter) const {

        if (_cells == 0)
            return;

        painter.fillRect(rect, _background);
        static const char digists[] = "0123456789";
        short width = rect.width();

        short cellRow = rect.row() + 1;
        short cellHeight = rect.height() - 2;
        short cellWidth = (width - (_cells - 1) * _gap - 2) / _cells;
        short cellColumn = width - cellWidth + rect.column() - 1;

        Digits::Value value = _value;
        for(Digits::Cells cell = 0; cell < _cells; cell++ ) {
            painter.drawDigit(digists[value % 10],
                              _thickness,
                              Rect(cellColumn,
                                   cellRow,
                                   cellWidth,
                                   cellHeight),
                                _foreground);
            cellColumn -= cellWidth + _gap;
            value /= 10;
        }
    }
private:
    Digits::Cells _cells;
    Digits::Gap _gap;
    Digits::Thickness _thickness;
    Color _background;
    Color _foreground;
    Digits::Value _value;
};
Digits::Digits(Cells cells, const Rect &rect, Widget *parent):
    Widget(rect, parent),
    _private(new Private(cells))
{

}

Digits::~Digits()
{
    delete _private;
}

void Digits::setValue(const Value &value)
{
    _private->setValue(value);
}

const Digits::Value &Digits::value() const
{
    return _private->value();
}

void Digits::setCells(const Cells &cells)
{
    _private->setCells(cells);
}

const Digits::Cells &Digits::cells() const
{
    return _private->cells();
}

void Digits::setGap(const Gap &gap)
{
    _private->setGap(gap);
}

const Digits::Gap &Digits::gap() const
{
    return _private->gap();
}

const Digits::Thickness &Digits::thickness() const
{
    return _private->thickness();
}

void Digits::setThickness(const Thickness &thickness)
{
    _private->setThickness(thickness);
}

const Color &Digits::background() const
{
    return _private->background();
}

void Digits::setBackground(const Color &background)
{
    _private->setBackground(background);
}

const Color &Digits::foreground() const
{
    return _private->foreground();
}

void Digits::setForeground(const Color &foreground)
{
    _private->setForeground(foreground);
}

void Digits::draw(const Rect &rect, Painter &painter) const
{
    _private->draw(*this, rect, painter);
}

} // namespace gui
} // namespace iot
