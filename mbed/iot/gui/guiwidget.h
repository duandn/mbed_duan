#ifndef GUICONTROL_H
#define GUICONTROL_H
#include <gui/guidefs.h>
#include <stddef.h>
namespace iot {
namespace gui {

class Painter;
class Widget
{
public:
    Widget(const Rect &rect = Rect(), Widget *parent = NULL);
    virtual ~Widget();
public:
    void paint(Painter &paint) const;

    Widget *parent();

    const Rect &rect() const;

    void setRect(const Rect &rect);

    bool shown() const;

    void setShown(bool shown);

    Point absolutePosition() const;

private:
    virtual void draw(const Rect &rect, Painter &painter) const;

    virtual void layout(const Rect &rect);
private:
    class Private;
    Private *_private;
};

} // namespace gui
} // namespace iot

#endif // GUICONTROL_H
