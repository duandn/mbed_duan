#ifndef GUIDEFS_H
#define GUIDEFS_H
#include <math/mathrect.h>
#include <stdint.h>
namespace iot {
namespace gui {

typedef math::Point<int16_t> Point;
typedef math::Size<int16_t> Size;
typedef math::Rect<int16_t> Rect;
typedef unsigned int Color;

} // namespace gui
} // namespace iot

#endif // GUIDEFS_H
