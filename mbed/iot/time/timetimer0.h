#ifndef TIMETIMER0_H
#define TIMETIMER0_H
#include <stdint.h>
namespace iot {
namespace time {
class Timer0
{
public:
    static void init();

    static void start(uint32_t period); // microseconds

    static uint32_t ticks();

    static void stop();
};

} // namespace time
} // namespace iot

#endif // TIMETIMER0_H
