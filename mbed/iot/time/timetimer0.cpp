#include <time/timetimer0.h>
#ifdef NRF51
#include <nrf51.h>
#include <nrf51_bitfields.h>
#endif
#ifdef NRF52
#include <nrf52.h>
#include <nrf52_bitfields.h>
#endif
namespace iot {
namespace time {
static uint32_t started;

void Timer0::init()
{
    NRF_TIMER0->PRESCALER = 4;
    NRF_TIMER0->TASKS_STOP = 1;
}

void Timer0::start(uint32_t period)
{
    NRF_TIMER0->TASKS_START = 1;
    NRF_TIMER0->TASKS_CLEAR = 1;
    NRF_TIMER0->EVENTS_COMPARE[0] = 0;
    /* set interrupt*/
    NRF_TIMER0->INTENSET = (1 << (TIMER_INTENSET_COMPARE0_Pos));
    NVIC_EnableIRQ(TIMER0_IRQn);

    NRF_TIMER0->CC[0] = period;
}

uint32_t Timer0::ticks()
{
    return NRF_TIMER0->TASKS_COUNT - started;
}

void Timer0::stop()
{
    NRF_TIMER0->TASKS_STOP = 1;
    NRF_TIMER0->INTENCLR = (1 << (TIMER_INTENCLR_COMPARE0_Pos));
}

} // namespace Time
} // namespace VNG
