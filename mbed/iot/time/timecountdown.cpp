#include <time/timecountdown.h>
#include <utils/utilsprint.h>
#include <core/coreapplication.h>
#include <core/corecommand.h>

namespace iot {
namespace time {
class Countdowner::Private
{
public:
    /**
     * @brief The Update class
     */
    class Down: public core::Command
    {
    public:
        Down(Countdowner::Down &down,
             const Countdowner::Duration &duration):
            _down(down),
            _duration(duration) {

        }

        virtual void execute() {
            _down.call(_duration);
            delete this;
        }

    private:
        Countdowner::Down &_down;
        Countdowner::Duration _duration;
    };

    /**
     * @brief The Finish class
     */
    class Finish: public core::Command
    {
    public:
        Finish(Countdowner::Finish &finish):
            _finish(finish) {

        }

        virtual void execute() {
            _finish.call();
            delete this;
        }

    private:
        Countdowner::Finish &_finish;
    };

    void down() {
        if (_duration >= _period) {
            _duration -= _period;
            Down *down = new Down(_down, _duration);
            if (core::Application::instance()->schedule(down) == 0)
                delete down;

            return;
        }
        _duration = 0;
        Finish *finish = new Finish(_finish);
        if (core::Application::instance()->schedule(finish) == 0)
            delete finish;
    }


    Private(const Countdowner::Duration &duration,
            const Countdowner::Period &period,
            const Countdowner::Down &down,
            const Countdowner::Finish &finish):
        _duration(duration),
        _period(period),
        _finish(finish),
        _down(down) {
        _ticker.attach(Callback<void()>(this, &Private::down), period);
    }

    virtual ~Private() {

    }


private:
    Ticker _ticker;
    Countdowner::Duration _duration;
    Countdowner::Period _period;
    Countdowner::Finish _finish;
    Countdowner::Down _down;
};

Countdowner::Countdowner():
    _private(NULL)
{

}

Countdowner::~Countdowner()
{
}

int Countdowner::start(const Duration &duration,
                         const Period &period,
                         const Down &down,
                         const Finish &finish)
{

    if (_private != NULL)
        return -1;
    _private = new Private(duration, period, down, finish);
    return 0;
}

bool Countdowner::running() const
{
    return _private != NULL;
}
void Countdowner::stop()
{
    if (_private == NULL)
        return;
    delete _private;
    _private = NULL;
}

} // namespace time
} // namespace iot
