#ifndef TIMECOUNTDOWN_H
#define TIMECOUNTDOWN_H
#include <mbed.h>
namespace iot {
namespace time {
class Countdowner
{
public:
    typedef uint64_t Duration;
    typedef uint32_t Period;

    typedef Callback<void()> Finish;
    typedef Callback<void(const Countdowner::Duration &duration)> Down;

    Countdowner();
    virtual ~Countdowner();
    int start(const Duration &duration,
              const Period &period,
              const Down &down,
              const Finish &finish);
    bool running() const;
    void stop();

private:
    class Private;
    Private *_private;
};

} // namespace time
} // namespace iot

#endif // TIMECOUNTDOWN_H
