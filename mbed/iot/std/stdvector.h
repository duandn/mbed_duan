#ifndef STDVECTOR_H
#define STDVECTOR_H
#include <stdint.h>

#ifndef NULL

#endif
namespace iot {
namespace std {
template<typename Value, typename Size = uint32_t>
class Vector
{
public:
    Vector(const Value *values, const Size &size):
        _size(size),
        _values(new Value[size]),
        _allocated(size) {
        for(Size index = 0; index < size; index++)
            _values[index] = values[index];
    }

    Vector(const Vector<Value> &vector):
        _size(vector.size()),
        _values(new Value[vector.size()]),
        _allocated(size) {
        for(Size index = 0; index < _size; index++)
            _values[index] = values[index];
    }

    Vector(const Size &size):
        _size(size),
        _values(new Value[size]),
        _allocated(size) {

    }

    Vector():
        _size(0),
        _values(0),
        _allocated(0) {

    }

    virtual ~Vector() {
        if (_values != 0) {
            _size = 0;
            _allocated = 0;
            delete []_values;
        }
    }

    void resize(const Size &size) {
        if (size <= _allocated) {
            _size = size;
            return;
        }

        Value *values = _values;
        _values = new Value[size];

        if (values != 0) {
            for(Size index = 0; index < _size; index++)
                _values[index] = values[index];
            delete []values;
        }
        _size = size;
        _allocated = size;
    }

    Vector<Value> &operator = (const Vector<Value> &vector) {
        copy(vector.values(), vector.size());
        return *this;
    }

    inline const Value &operator [] (const Size &index) const {
        return _values[index];
    }

    inline Value &operator [] (const Size &index) {
        return _values[index];
    }

    inline const Size &size() const {
        return _size;
    }

    inline const Value *values() const {
        return _values;
    }


    inline Value *values() {
        return _values;
    }

    void copy(const Value *values, const Size &size) {
        resize(size);
        for(Size index = 0; index < size; index ++)
            _values[index] = values[index];
    }

    void enqueue(const Value &value) {
        Size size = _size;
        resize(_size + 1);
        _values[size] = value;
    }

    Value pop() {
        Value value = _values[0];
        for(Size index = 1; index < _size; index++)
            _values[index - 1] = _values[index];
        return value;
    }

    Value dequeue() {
        Value value = _values[_size - 1];
        _size -= 1;
        return value;
    }

    void clear() {
        _size = 0;
    }

private:
    Size _size;
    Value *_values;
    Size _allocated;
};

template<typename Value, typename Size = uint32_t>
class ConstVector
{
public:
    ConstVector(const Value *values, const Size &size):
        _size(size),
        _values(values) {
    }

    ConstVector(const ConstVector<Value> &vector):
        _size(vector.size()),
        _values(vector.values()) {
    }

    virtual ~ConstVector() {
    }

    ConstVector<Value> &operator = (const ConstVector<Value> &vector) {
        _values = vector.values();
        _size = vector.size();
        return *this;
    }

    inline const Value &operator [] (const Size &index) const {
        return _values[index];
    }



    inline const Size &size() const {
        return _size;
    }

    inline const Value *values() const {
        return _values;
    }



private:
    Size _size;
    const Value *_values;
};

} // namespace std
} // namespace iot

#endif // STDVECTOR_H
