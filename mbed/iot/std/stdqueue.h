#ifndef STDQUEUE_H
#define STDQUEUE_H
#include <stdint.h>
#include <stddef.h>
namespace iot {
namespace std {
template<typename Value, typename Size = uint32_t>
class Queue
{
private:
    class Node
    {
    public:
        Node(const Value &value):
            _value(value),
            _next(NULL) {

        }

        inline Node *next() const {
            return _next;
        }

        inline void setNext(Node *next) {
            _next = next;
        }


        inline Value &value() {
            return _value;
        }

    private:
        Value _value;
        Node *_next;
    };

public:
    class Iterator
    {
    private:
        Iterator(Queue *owner, Node *node = NULL):
            _owner(owner),
            _node(node) {

        }
    public:
        Iterator():
            _owner(NULL),
            _node(NULL) {

        }

        Iterator(const Iterator &iterator):
            _owner(iterator._owner),
            _node(iterator._node) {

        }

        inline Iterator &operator ++ () {
            _node = _node->next();
            return *this;
        }

        inline Iterator &operator ++ (int) {
            _node = _node->next();
            return *this;
        }

        inline Value &operator *() {
            return _node->value();
        }

        inline bool operator ==(const Iterator &iterator) const {
            return _node == iterator._node && _owner == iterator._owner;
        }

        inline bool operator != (const Iterator &iterator) const {
            return _node != iterator._node || _owner != iterator._owner;
        }
        inline Iterator &operator = (const Iterator &iterator) {
            _node = iterator._node;
            _owner = iterator._owner;
            return *this;
        }
    private:
        Queue *_owner;
        Node *_node;
        friend class Queue;
    };

    /**
     * @brief Queue
     */
    Queue():
        _head(NULL),
        _tail(NULL),
        _size(0) {

    }

    virtual ~Queue() {

    }

    Iterator begin() const {
        return Iterator(this, _head);
    }

    Iterator begin() {
        return Iterator(this, _head);
    }

    Iterator end() {
        return Iterator(this, NULL);
    }

    Iterator end() const {
        return Iterator(this, NULL);
    }

    void remove(const Iterator &iterator) {
        remove(iterator._node);
        delete iterator._node;
    }

    Iterator enqueue(const Value &item) {
        Node *node = new Node(item);
        if (_head == NULL && _tail == NULL) {
            _head = node;
            _tail = node;
        } else {
            _tail->setNext(node);
            _tail = node;
        }
        _size += 1;
        return Iterator(this, node);
    }


    Value dequeue() {
        Node *head = _head;
        if(_tail == _head) {
            _tail = NULL;
            _head = NULL;
        } else {
            _head = _head->next();
        }
        Value item = head->value();
        delete head;
        _size -= 1;
        return item;
    }

    Value head() {
        return _head->value();
    }

    const Value &head() const {
        return _head->value();
    }

    inline const Size &size() const {
        return _size;
    }

    void clear() {
        while (_head != NULL) {
            Node *head = _head;
            _head = _head->next();
            delete head;
        }
        _tail = NULL;
        _size = 0;
    }
private:
    Node *_head;
    Node *_tail;
    Size _size;
    friend class Iterator;

};
} // namespace std
} // namespace iot

#endif // STDQUEUE_H
