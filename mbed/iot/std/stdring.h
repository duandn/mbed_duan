/*
 * utilsring.h
 *
 *  Created on: Jul 22, 2016
 *      Author: trungbq
 */

#ifndef UTILSRING_H
#define UTILSRING_H

namespace iot {
namespace std {

/** Templated Ring class
 */
template<typename T, typename CounterType = uint32_t>
class Ring {
public:
    Ring(CounterType capacity) :
        _capacity(capacity),
        _pool(new T[capacity]),
        _head(0),
        _tail(0),
        _fulled(false) {
    }

    ~Ring() {
        delete []_pool;
        _pool = NULL;
    }

    /** Push the transaction to the buffer. This overwrites the buffer if it's
     *  full
     *
     * @param data Data to be pushed to the buffer
     */
    void push(const T&data) {
        if (fulled()) {
            _tail++;
            _tail %= _capacity;
        }

        _pool[_head++] = data;
        _head %= _capacity;

        if (_head == _tail) {
            _fulled = true;
        }
    }

    /** Pop the transaction from the buffer
     *
     * @param data Data to be pushed to the buffer
     * @return True if the buffer is not empty and data contains a transaction, false otherwise
     */
    bool pop(T &data) {
        if (!empty()) {
            data = _pool[_tail++];
            _tail %= _capacity;
            _fulled = false;
            return true;
        }
        return false;
    }

    /** Check if the buffer is empty
     *
     * @return True if the buffer is empty, false if not
     */
    bool empty() {
        return (_head == _tail) && !_fulled;
    }

    /** Check if the buffer is full
     *
     * @return True if the buffer is full, false if not
     */
    bool fulled() {
        return _fulled;
    }

    /** Reset the buffer
     *
     */
    void reset() {
        _head = 0;
        _tail = 0;
        _fulled = false;
    }

    CounterType size() const {
        if (_head == _tail)
            return _fulled? _capacity: 0;
        CounterType tail = _tail;
        if (tail < _head)
            tail += _capacity;
        return tail - _head;
    }

    const T &operator [](CounterType index) {
        return _pool[(index + _head) % _capacity];
    }
private:
    CounterType _capacity;
    T *_pool;
    volatile CounterType _head;
    volatile CounterType _tail;
    volatile bool _fulled;
};

} // namespace std
} // namespace iot

#endif /* UTILS_UTILSRING_H_ */
