#ifndef BLESYSTEM_H
#define BLESYSTEM_H
#include <core/coresystem.h>
#include <mbed.h>
class BLE;
namespace iot {
namespace ble {

class Event;
class System: public core::System
{

public:

    typedef uint8_t Rssi;

    typedef Callback<void(Rssi rssi,
                          const uint8_t *address,
                          const uint8_t *details,
                          uint8_t size)> Received;

    typedef uint16_t Id;

    typedef Callback<void(const Id &id, bool rejected)> Advertised;

    System();

    virtual ~System();

    BLE &ble();

    int start(uint16_t timeout, /*in second*/
              const Received &received,
              uint16_t size,
              const Advertised &advertised);

    int start(uint16_t size, const Advertised &advertised);

    int advertise(const uint8_t *payload, uint8_t size, uint8_t times, Id &id);

    int advertise(const Event &event, uint8_t times, Id &id);

    void stop();

    const uint8_t *address() const;

private:
    virtual int initialize();

    virtual void release();
private:
    virtual int initialize(BLE &ble);

    virtual void release(BLE &ble);

private:
    class Private;
    Private *_private;
};

} // namespace ble
} // namespace iot

#endif // BLESYSTEM_H
