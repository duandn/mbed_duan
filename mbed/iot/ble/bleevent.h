#ifndef BLEEVENT_H
#define BLEEVENT_H
#include <ble/blemessage.h>
namespace iot {
namespace ble {

/**
 * @brief The Event class
 */
class Event: public Message
{
public:

    Event();

    virtual ~Event();

    uint8_t size() const;

    void fill(Message::Sequence sequence, uint8_t *payload) const;

public:

    virtual const char *descrition() const = 0;

    virtual Message::Role role() const;

private:

    virtual uint8_t _size() const = 0;

    virtual void _fill(uint8_t *details) const = 0;
};

} // namespace ble
} // namespace iot

#endif // BLEEVENT_H
