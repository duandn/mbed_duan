#ifndef BLEAPPLICATION_H
#define BLEAPPLICATION_H
#include <core/coreapplication.h>
#include <std/stdvector.h>

namespace iot {
namespace ble {

class System;
class Application: public core::Application
{
public:

    Application();

    virtual ~Application();
public:
    virtual int initialize();

    virtual int systems() const;

    virtual core::System *system(int system);

    virtual void release();

private:

    virtual int initialize(System &system);

    virtual void release(System &system);

    virtual void notify(const core::Status &status);

private:
    class Private;
    friend class Private;
    Private *_private;
};

} // namespace ble
} // namespace iot

#endif // BLEAPPLICATION_H
