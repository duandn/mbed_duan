#include <ble/bleapplication.h>
#include <ble/bleevent.h>
#include <ble/blesystem.h>
#include <core/corecommand.h>
#include <core/corestatus.h>
#include <std/stdvector.h>
#include <utils/utilsprint.h>
#include <utils/utilsstorage.h>

#include <mbed.h>
namespace iot {
namespace ble {

class Application::Private
{
public:

    class Event: public ble::Event
    {
    public:

        Event(const core::Status &status):
            _status(status) {

        }

        virtual ~Event() {

        }

        virtual ble::Message::Command command() const {
            return ble::Message::Status;
        }

        virtual const char *descrition() const {
            return _status.descrition();
        }
    private:

        virtual uint8_t _size() const {
            return _status.size();
        }

        virtual void _fill(uint8_t *details) const {
            _status.fill(details);
        }

    private:
        const core::Status &_status;
    };

    Private() {

    }

    virtual ~Private() {
    }

    int initialize(Application &app) {
        return app.initialize(_system);
    }

    int systems() const {
        return 1;
    }

    core::System *system(int system) {
        return system == 0? &_system: NULL;
    }

    void release(Application &app) {
        app.release(_system);
    }

    void notify(const core::Status &status) {
        Event event(status);
        System::Id id;
        _system.advertise(event, 7, id);
    }

private:
    iot::ble::System _system;
};


Application::Application():
    _private(new Private)
{

}

Application::~Application()
{
    delete _private;
}

int Application::initialize()
{
    return _private->initialize(*this);
}

int Application::systems() const
{
    return _private->systems();
}

core::System *Application::system(int system)
{
    return _private->system(system);
}

int Application::initialize(System &system)
{
    return 0;
}

void Application::release(System &system)
{

}

void Application::notify(const core::Status &status)
{
    return _private->notify(status);
}

void Application::release()
{
    _private->release(*this);
}

} // namespace ble
} // namespace iot
