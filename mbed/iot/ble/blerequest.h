#ifndef BLEREQUEST_H
#define BLEREQUEST_H

#include <ble/blemessage.h>
#include <ble/bledefs.h>

namespace iot {
namespace ble {

class Request: public Message
{
public:

    Request();

    virtual ~Request();

    int parse(const uint8_t *packet, uint8_t size);

    Message::Sequence sequence() const;

    const uint8_t *address() const;

    virtual Message::Role role() const;

    virtual bool accepted(Message::Role role) const;

private:

    virtual int parseRequest(const uint8_t *payload, uint8_t size) = 0;

private:
    class Private;
    Private *_private;
};

} // namespace ble
} // namespace iot

#endif // BLEREQUEST_H
