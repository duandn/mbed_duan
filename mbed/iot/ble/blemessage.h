#ifndef BLEMESSAGE_H
#define BLEMESSAGE_H
#include <stdint.h>
#define BLE_MESSAGE_HEADER_SIZE (sizeof(::iot::ble::Message::Sequence) + \
                                 sizeof(::iot::ble::Message::Command) + \
                                 sizeof(uint8_t))

namespace iot {
namespace ble {

/**
 * @brief The Message class
 */
class Message
{
public:
    enum Commands {
        AccessButton = 0x1400,
        AccessKeypad = 0x1100,
        AccessReader = 0x1200,
        AccessDoor = 0x0011,
        LightButton = 0x2100,
        LightSensor = 0x2200,
        PirSensor = 0x2300,
        LightLamp = 0x0021,
        LightDimmer = 0x0022,
        LightGroupId = 0x002F,
        HumiditySensor = 0x3100,
        TagSensor = 0x3200,
        TemperatureSensor = 0x3300,
        MCTemperatureSensor = 0x3400,
        ActivePir = 0x4400,
        ActiveThermometer = 0x4500,
        Status = 0xFF00
    };

    enum Roles {
        Device = 0x00,
        Gateway = 0x01,
        Mobile = 0x03
    };

    typedef uint16_t Sequence;
    typedef uint16_t Command;
    typedef uint8_t Role;

    Message();

    virtual ~Message();

    virtual Command command() const = 0;

    virtual Role role() const = 0;

    static Sequence sequence(const uint8_t *payload,
                             uint8_t size,
                             int &error);

    static Command command(const uint8_t *payload,
                           uint8_t size,
                           int &error);

    static Role role(const uint8_t *payload, uint8_t size, int &error);

};

} // namespace ble
} // namespace iot

#endif // BLEMESSAGE_H
