#include <ble/bleevent.h>
#include <utils/utilsendian.h>

using namespace iot::utils::endian;

namespace iot {
namespace ble {

/**
 * @brief Event::Event
 */

Event::Event()
{
}

Event::~Event()
{
}

uint8_t Event::size() const
{
    return BLE_MESSAGE_HEADER_SIZE + _size();
}

void Event::fill(Message::Sequence sequence,
                 uint8_t *payload) const
{
    Message::Sequence bigSeq = toBig<Message::Sequence>(sequence);
    memcpy(payload, &bigSeq, sizeof(Message::Sequence));

    Message::Sequence command = toBig<Message::Sequence>(this->command());
    memcpy(payload + sizeof(Message::Sequence),
           &command,
           sizeof(Message::Command));

    Message::Role role = this->role();
    memcpy(payload + sizeof(Message::Sequence) + sizeof(Message::Command),
           &role,
           sizeof(uint8_t));

    _fill(payload + BLE_MESSAGE_HEADER_SIZE);
}

Message::Role Event::role() const
{
    return Message::Device;
}

} // namespace ble
} // namespace iot
