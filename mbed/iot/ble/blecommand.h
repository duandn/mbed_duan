#ifndef BLECOMMAND_H
#define BLECOMMAND_H

#include <ble/bleevent.h>
#include <ble/bledefs.h>

namespace iot {
namespace ble {

class Command: public Event
{

public:

    Command(const uint8_t *target);

    virtual ~Command();

    inline const uint8_t *target() const {
        return &_target[0];
    }

    virtual Message::Role role() const;
private:

    virtual uint8_t _size() const;

    virtual void _fill(uint8_t *details) const;

private:

    virtual uint8_t _commandSize() const = 0;

    virtual void _commandFill(uint8_t *details) const = 0;

private:

    Address _target;
};

} // namespace ble
} // namespace iot

#endif // BLECOMMAND_H
