#include <ble/blerequest.h>
#include <utils/utilsprint.h>
#include <utils/utilsendian.h>
using namespace iot::utils::endian;
namespace iot {
namespace ble {

class Request::Private
{
public:
    Private() {

    }

    int parse(const uint8_t *payload, uint8_t size, const Request &request) {

        if (size < BLE_MESSAGE_HEADER_SIZE + BLE_ADDRESS_SIZE)
            return -1;
        int error;

        _role = Message::role(payload, size, error);

        if (error != 0)
            return error;

        if (!request.accepted(_role))
            return -2;

        memcpy(&_sequence, payload, sizeof(Message::Sequence));
        _sequence = toLittle<Message::Sequence>(_sequence);

        Message::Command command;

        memcpy(&command,
               payload + sizeof(Message::Sequence),
               sizeof(Message::Command));

        command = toLittle<Message::Command>(command);

        if (command != request.command()) {
            PRINTF("*ble request* command: 0x%04X\r\n", command);
            return -3;
        }

        memcpy(&_address[0],
                payload + BLE_MESSAGE_HEADER_SIZE,
                BLE_ADDRESS_SIZE);
        return 0;
    }

    inline Message::Role role() const {
        return _role;
    }

    inline Message::Sequence sequence() const {
        return _sequence;
    }

    const uint8_t *address() const {
        return &_address[0];
    }

private:
    Message::Sequence _sequence;
    Address _address;
    Message::Role _role;
};

/**
 * @brief Request::Request
 */
Request::Request():
    _private(new Private)
{
}

Request::~Request()
{
    delete _private;
}

Message::Sequence Request::sequence() const
{
    return _private->sequence();
}

const uint8_t *Request::address() const
{
    return _private->address();
}

int Request::parse(const uint8_t *payload, uint8_t size)
{
    int error = _private->parse(payload, size, *this);
    if (error != 0)
        return error;

    return parseRequest(payload + BLE_MESSAGE_HEADER_SIZE + BLE_ADDRESS_SIZE,
                        size - + BLE_MESSAGE_HEADER_SIZE + BLE_ADDRESS_SIZE);
}

Message::Role Request::role() const
{
    return _private->role();
}

bool Request::accepted(Message::Role role) const
{
    return role == Message::Gateway;
}

} // namespace ble
} // namespace iot
