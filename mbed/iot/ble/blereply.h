#ifndef BLEREPLY_H
#define BLEREPLY_H
#include <ble/bleevent.h>
namespace iot {
namespace ble {
class Request;
class Reply: public Event
{
public:

    Reply(const Request &request);

public:

    virtual Message::Command command() const;

private:
    virtual void _fill(uint8_t *details) const;
private:
    virtual void fill(const Request &request, uint8_t *details) const = 0;
private:
    const Request &_request;
};
} // namespace ble
} // namespace iot


#endif // BLEREPLY_H
