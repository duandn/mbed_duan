#include <ble/blereply.h>
#include <ble/blerequest.h>

namespace iot {
namespace ble {

Reply::Reply(const Request &request):
    _request(request)
{

}

Message::Command Reply::command() const
{
    return _request.command();
}

void Reply::_fill(uint8_t *details) const
{
    fill(_request, details);
}

} // namespace ble
} // namespace iot

