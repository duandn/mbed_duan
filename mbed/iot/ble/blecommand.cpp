#include <ble/blecommand.h>
#include <string.h>
namespace iot {
namespace ble {

Command::Command(const uint8_t *target)
{
    memcpy(&_target[0], target, BLE_ADDRESS_SIZE);
}

Command::~Command()
{

}

Message::Role Command::role() const
{
    return Message::Gateway;
}

uint8_t Command::_size() const
{
    return BLE_ADDRESS_SIZE + _commandSize();
}

void Command::_fill(uint8_t *details) const
{
    memcpy(details, &_target[0], BLE_ADDRESS_SIZE);
    _commandFill(details + BLE_ADDRESS_SIZE);
}

} // namespace ble
} // namespace iot
