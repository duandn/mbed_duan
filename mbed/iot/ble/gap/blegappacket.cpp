#include <ble/gap/blegappacket.h>
#include <utils/utilsprint.h>
#include <string.h>

namespace iot {
namespace ble {
namespace gap {

class Packet::Private
{
public:
    Private():
        _length(0) {
    }

    int setPacket(const ble::Packet &payload) {
        return setPacket(&payload[0], BLE_PAYLOAD_SIZE);
    }

    int setPacket(const uint8_t *payload, uint8_t size) {


        for(_length = 0; _length < size - 2 &&
                         _length < BLE_PAYLOAD_SIZE - 2;) {

            struct Packet::_Field *source = (struct Packet::_Field *)
                                            (payload + _length);


            if (source->type == 0)
                return 0;


            if (source->length < 1)
                return -1;


            if (_length + source->length + 1 > size)
                return -1;


            Packet::Field target = (Packet::Field)(&_payload[_length]);
            target->type = source->type;
            target->length = source->length;

            if (source->length > 1) {
                memcpy(&target->payload[0],
                       &source->payload[0],
                       source->length - 1);
            }

            _length += source->length + 1;
        }

        if (_length < BLE_PAYLOAD_SIZE)
            memset(&_payload[_length], 0, BLE_PAYLOAD_SIZE - _length);
        return 0;
    }


    int setField(uint8_t type, const uint8_t *payload, uint8_t length) {


        Packet::Field field = Packet::field(_payload, _length, type);
        if (field != NULL)
            remove(field);

        if (_length + length + 2 > BLE_PAYLOAD_SIZE) {
            PRINTF("*ble gap packet* set field failed because length: %d"
                   " too long\r\n",
                   (int) length);

            return -1;
        }
        field = (Packet::Field)&_payload[_length];
        field->length = length + 1;
        field->type = type;
        memcpy(&field->payload[0], payload, length);
        _length += field->length + 1;
        if (_length < BLE_PAYLOAD_SIZE)
            _payload[_length] = 0;
        return 0;
    }

    void remove(Packet::Field field) {
        uint8_t *payload = (uint8_t *)field;
        uint8_t start = (payload - &_payload[0]);
        uint8_t length = _length - start - field->length - 1;

        _length -= field->length + 1;
        uint8_t shifted = field->length + 1;
        for(uint8_t index = 0; index < length; index++)
            payload[index] = payload[index + shifted];

        payload[start + length] = 0;
    }


    void remove(uint8_t type) {
        Packet::Field field = Packet::field(_payload, _length, type);
        if (field == NULL)
            return;
        remove(field);
    }

    struct Packet::_Field * field(uint8_t type) {
        return Packet::field(&_payload[0], _length, type);
    }

    const struct Packet::_Field * field(uint8_t type) const {
        return Packet::field(&_payload[0], _length, type);
    }

    inline const uint8_t *payload() const {
        return &_payload[0];
    }

    inline uint8_t length(void) const {
        return _length;
    }

private:
    ble::Packet _payload;
    uint8_t _length;
};

Packet::Packet():
    _private(new Private)
{

}


Packet::~Packet()
{
    delete _private;
}

int Packet::setPacket(const ble::Packet &payload)
{
    return _private->setPacket(payload);
}

int Packet::setPacket(const uint8_t *payload, uint8_t size)
{
    return _private->setPacket(payload, size);
}


int Packet::setField(uint8_t type, const uint8_t *payload, uint8_t length)
{
    return _private->setField(type, payload, length);
}

void Packet::remove(uint8_t type)
{
    _private->remove(type);
}

Packet::Field Packet::field(uint8_t type)
{
    return _private->field(type);
}

const uint8_t *Packet::payload() const
{
    return _private->payload();
}

uint8_t Packet::length(void) const
{
    return _private->length();
}

const struct Packet::_Field *Packet::field(const uint8_t* packet,
                                           uint8_t length,
                                           uint8_t type)
{
    for (uint8_t position = 0; position + 2 < length;) {

        const struct _Field *field = (const struct _Field *) &packet[position];

        if (field->type == 0)
            return NULL;

        if (field->length > length - position - 1)
            return NULL;

        if (field->type == type)
            return field;

        position += field->length + 1;
    }
    return NULL;
}

struct Packet::_Field *Packet::field(uint8_t* packet,
                                     uint8_t length,
                                     uint8_t type)
{
    for (uint8_t position = 0; position + 2 < length;) {

        struct _Field *field = (struct _Field *) &packet[position];

        if (field->type == 0)
            return NULL;

        if (field->length > length - position - 1)
            return NULL;

        if (field->type == type)
            return field;

        position += field->length + 1;
    }
    return NULL;
}

} // namespace gap
} // namespace ble
} // namespace iot
