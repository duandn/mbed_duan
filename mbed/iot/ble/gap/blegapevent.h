#ifndef BLEGAPCOMMAND_H
#define BLEGAPCOMMAND_H
#include <stdint.h>


#define EVENT_ACCESS_BUTTON         0x1400
#define EVENT_ACCESS_KEYPAD         0x1100
#define EVENT_ACCESS_READER         0x1200
#define EVENT_ACCESS_REED           0x1300
#define EVENT_ACCESS_DOOR           0x0011
#define EVENT_LIGHT_BUTTON          0x2100
#define EVENT_LIGHT_SENSOR          0x2200
#define EVENT_PIR_SENSOR            0x2300
#define EVENT_LIGHT_LAMP            0x0021
#define EVENT_LIGHT_DIMMER          0x0022
#define EVENT_HUMIDITY_SENSOR       0x3100
#define EVENT_TAG_SENSOR            0x3200
#define EVENT_TEMPERATURE_SENSOR	0x3300
#define EVENT_MCTEMPERATURE_SENSOR	0x3400
#define EVENT_SURVIVE_SIGNAL        0xFF00

namespace iot {
namespace ble {
namespace gap {
typedef uint16_t CompanyId;
typedef uint16_t SequenceId;
typedef uint16_t CommandId;
struct _Event {
    CompanyId companyId;
    uint16_t sequenceId;
    uint16_t commandId;
    uint8_t gateway;
    uint8_t payload[0];
} __attribute__((__packed__));

} // namespace gap
} // namespace ble
} // namespace iot

#endif // BLEGAPCOMMAND_H
