#ifndef BLEGAPFIELD_H
#define BLEGAPFIELD_H
#include <std/stdvector.h>

namespace iot {
namespace ble {
namespace gap {

class Field
{
public:
    typedef uint8_t Type;
    typedef uint8_t Size;

    virtual ~Field();

    virtual Type type() const = 0;

    virtual Size size() const = 0;

    virtual const uint8_t *payload() const = 0;
};

namespace manufacturer {
class Field: public gap::Field
{
public:
    typedef uint8_t Type;
    typedef uint8_t Size;

    Field();

    virtual ~Field();

    virtual Type type() const;

    virtual Size size() const;

    virtual const uint8_t *payload() const;
private:
    uint8_t *_payload;
    uint8_t _size;
};

}
} // namespace gap
} // namespace ble
} // namespace iot

#endif // BLEGAPFIELD_H
