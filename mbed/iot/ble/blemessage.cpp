#include <ble/blemessage.h>
#include <utils/utilsendian.h>
#include <mbed.h>

using namespace iot::utils::endian;
namespace iot {
namespace ble {

Message::Message()
{

}

Message::~Message()
{

}

Message::Role Message::role(const uint8_t *payload, uint8_t size, int &error)
{

    if (BLE_MESSAGE_HEADER_SIZE > size) {
        error = -1;
        return 0xFF;
    }

    error = 0;
    return *(payload + sizeof(Message::Sequence) + sizeof(Message::Command));
}

Message::Sequence Message::sequence(const uint8_t *payload,
                                    uint8_t size,
                                    int &error)
{
    if (BLE_MESSAGE_HEADER_SIZE > size) {
        error = -1;
        return 0;
    }
    Message::Sequence sequence;
    memcpy(&sequence, payload, sizeof(Message::Sequence));
    sequence = toLittle<Message::Sequence>(sequence);
    error = 0;
    return sequence;
}

Message::Command Message::command(const uint8_t *payload,
                                  uint8_t size,
                                  int &error)
{
    if (BLE_MESSAGE_HEADER_SIZE > size) {
        error = -1;
        return 0;
    }

    Message::Command command;
    memcpy(&command,
           payload + sizeof(Message::Sequence),
           sizeof(Message::Command));

    command = toLittle<Message::Sequence>(command);
    error = 0;
    return command;
}

} // namespace ble
} // namespace iot
