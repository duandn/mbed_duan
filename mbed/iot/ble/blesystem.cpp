#include <core/coreapplication.h>
#include <core/corecommand.h>
#include <ble/bleevent.h>
#include <ble/gap/blegappacket.h>
#include <ble/bledefs.h>
#include <ble/blesystem.h>
#include <radio/radiocontrol.h>
#include <std/stdvector.h>
#include <utils/utilsendian.h>
#include <utils/utilsprint.h>
#include <BLE.h>

#define SCAN_INTERVAL_NORMALL 50 // milliseconds
#define SCAN_WINDOW_NORMAL 25 // milliseconds
#define ADVERTISE_INTERVAL 20000 // microseconds

using namespace iot::ble::gap;
using namespace iot::radio;
using namespace iot::utils::endian;
namespace iot {
namespace ble {
class System::Private
{
public:
    typedef std::Vector<uint8_t, uint8_t> Payload;
    /**
     * @brief The Events class
     */
    class Events: public core::Command
    {
    public:
        Events(Private &priv):
            _private(priv) {

        }

        virtual ~Events() {

        }

        virtual void execute() {
            _private.events();
        }

    private:
        Private &_private;
    };

    /**
     * @brief Private
     * @param ble
     */
    Private(BLE &ble):
        _ble(ble),
        _scanning(false),
        _events(NULL),
        _sequence(0) {

    }

    virtual ~Private() {
    }

    void received(const Gap::AdvertisementCallbackParams_t *params) {
        PRINTF("*ble system* received\r\n");

        if (params->type !=
            GapAdvertisingParams::ADV_NON_CONNECTABLE_UNDIRECTED) {
            PRINTF("*ble system* received connected or directed\r\n");
            return;
        }

        if (params->advertisingDataLen > BLE_PAYLOAD_SIZE) {
            PRINTF("*ble system* received field not failed\r\n");
            return;
        }

        const gap::Packet::_Field *field;
        field = gap::Packet::field(params->advertisingData,
                                   params->advertisingDataLen,
                                   gap::Packet::TypeManufacturerSpecificData);
        if (field == NULL) {
            PRINTF("*ble system* received field not failed\r\n");
            return;
        }


        if (field->length < sizeof(uint16_t) + BLE_MESSAGE_HEADER_SIZE + 1) {
            PRINTF("*ble system* received length too short\r\n");
            return;
        }

        uint16_t company;
        memcpy(&company, field->payload, sizeof(uint16_t));
        if (company != toBig<uint16_t>(BLE_COMPANYID_VNG)) {
            PRINTF("*ble system* received company: 0x%04X not matched:\r\n",
                   company);
            return;
        }


        const uint8_t *payload = field->payload + sizeof(uint16_t);
        uint8_t size = field->length - sizeof(uint16_t) - 1;
        _received.call(params->rssi, params->peerAddr, payload, size);
    }

    void onDisconnection(const Gap::DisconnectionCallbackParams_t *params) {
        PRINTF("*system* disconnection with reason: %d\r\n",
               (int) params->reason);

        if (!_scanning)
            return;
        int error = (int) _ble.gap().startScan(this, &Private::received);
        if (error == 0)
            return;
        PRINTF("*system* disconnection rescan failed with error: %d\r\n", error);
    }

    void onTimeout(Gap::TimeoutSource_t timeout) {
        PRINTF("*system* time out: %d\r\n", (int) timeout);
        if (!_scanning)
            return;
        int error = (int) _ble.gap().startScan(this, &Private::received);
        if (error == 0)
            return;
        PRINTF("*system* timeout rescan failed with error: %d\r\n", error);
    }

    void advertised(const Controller::Id &id, bool rejected) {
        _advertised.call(id, rejected);
    }

    int start(uint16_t timeout,
              const System::Received &received,
              uint16_t size,
              const System::Advertised &advertised) {

        _received = received;

        Gap &gap = _ble.gap();
        gap.setScanParams(SCAN_INTERVAL_NORMALL, SCAN_WINDOW_NORMAL, timeout);
        gap.onDisconnection(this, &Private::onDisconnection);
        gap.onTimeout(Gap::TimeoutEventCallback_t(this, &Private::onTimeout));

        _error = (int) gap.startScan(this, &Private::received);
        if (_error != BLE_ERROR_NONE) {
            PRINTF("*ble system* start scan failed with error: %d\r\n", (int) error);
            return (int)_error;
        }


        _error = start(size, advertised);
        if (_error != 0) {
            PRINTF("*ble system* start advertise failed with error: %d\r\n", (int) _error);
            gap.stopScan();
            return (int)_error;
        }

        _scanTimeout = timeout;
        _scanning = true;
        return 0;
    }


    int start(uint16_t size, const System::Advertised &advertised) {
        radio::Controller &controller = radio::Controller::instance();
        Controller::Advertised adverted(this, &Private::advertised);
        int error = controller.initialize(_address,
                                          Controller::All,
                                          size,
                                          ADVERTISE_INTERVAL,
                                          adverted);
        if (error != 0)
            return error;
        _advertised = advertised;
        return 0;
    }


    int advertise(const uint8_t *payload,
                  uint8_t size,
                  uint8_t times,
                  System::Id &id) {
        int error;
        radio::Controller &controller = radio::Controller::instance();
        PRINTF("*advertise* controller advertise\r\n");

        error = controller.advertise(payload, size, times, id);

        if (error != 0) {
            PRINTF("*advertise* controller advertise failed with error: %d\r\n", error);
            return error;
        }

        return 0;
    }

    int advertise(const Event &event, uint8_t times, System::Id &id) {
        const char *description = event.descrition();

        PRINTF("*system* advertisedescription = %s\r\n",description);

        _packet.remove(gap::Packet::TypeCompleteLocalName);

        if (description != NULL) {
            int error = _packet.setField(gap::Packet::TypeCompleteLocalName,
                                         (const uint8_t *)description,
                                         strlen(description));
            if (error != 0) {
                PRINTF("*system* advertise set local"
                       " name failed with error: %d\r\n",
                       error);
                return error;
            }
        }

        _payload.resize(event.size() + sizeof(uint16_t));
        uint16_t company = toBig<uint16_t>(BLE_COMPANYID_VNG);
        uint8_t *values = _payload.values();
        memcpy(values, &company, sizeof(uint16_t));

        PRINTF("*system* advertise event fill \r\n");

        event.fill(_sequence, values + sizeof(uint16_t));

        int error = _packet.setField(gap::Packet::TypeManufacturerSpecificData,
                                     values,
                                     _payload.size());
        if (error != 0) {
            PRINTF("*system* set advertise manufature specific data"
                   " failed with error: %d\r\n",
                   error);
            return error;
        }
        PRINTF("*system* advertise advertise advertise 5555555555555555555 \r\n");

        PRINTF("*system* advertise _packet.length() = %d  \r\n",_packet.length());
        error = advertise(_packet.payload(), _packet.length(), times, id);
        if (error != 0) {
            PRINTF("*system* advertise failed with error: %d\r\n", error);
            return error;
        }
        PRINTF("*system* advertise advertise advertise 666666666666666666666 \r\n");

        _sequence += 1;
        return 0;
    }

    void stop() {
        if (_scanning) {
            _scanning = false;
            _error = _ble.gap().stopScan();
        }
        radio::Controller::instance().release();
    }

    inline const uint8_t *address() const {
        return &_address[0];
    }

    void onInitComplete(BLE::InitializationCompleteCallbackContext *params) {
        _error = (int) params->error;
    }

    void events() {
//        PRINTF("*ble system* events\r\n");
        _ble.processEvents();
    }

    void onEvents(BLE::OnEventsToProcessCallbackContext* context) {
        core::Application::instance()->schedule(_events);
    }


    int initialize() {

        uint8_t flag = 0x04;
        int error = _packet.setField(gap::Packet::TypeFlags, &flag, sizeof(flag));
        if (error != 0) {
            PRINTF("*applications* advertise set flags failed with error:"
                   " %d\r\n",
                   error);
            return error;
        }

        _events = new Events(*this);

        BLEProtocol::AddressType_t type;

        _ble.onEventsToProcess(BLE::OnEventsToProcessCallback_t
                               (this, &Private::onEvents));

        _ble.init(this, &Private::onInitComplete);

        while (!_ble.hasInitialized());

        _error = _ble.gap().getAddress(&type, _address);

        PRINTF("*ble system* address type: %d\r\n", type);

        if (_error != BLE_ERROR_NONE) {
            delete _events;
            _events = NULL;

            return (int) _error;
        }

        return int(_error);
    }

    void release() {
        _ble.shutdown();
        delete _events;
        _events = NULL;
    }

private:
    BLE &_ble;
    int _error;
    BLEProtocol::AddressBytes_t _address;

    System::Received _received;
    System::Advertised _advertised;
    bool _scanning;
    uint16_t _scanTimeout;
    Events *_events;
    gap::Packet _packet;
    Payload _payload;
    Timeout _timeout;
    Message::Sequence _sequence;
};

System::System():
    _private(NULL)
{

}

System::~System()
{

}

int System::initialize()
{
    if (_private != NULL)
        return -1;

    BLE &ble = BLE::Instance();
    _private =  new Private(ble);
    int error = _private->initialize();
    if (error != 0) {
        delete _private;
        _private = 0;
        return error;
    }
    error = initialize(ble);
    if (error != 0) {
        delete _private;
        _private->release();
        _private = 0;
        return error;
    }
    return 0;
}

int System::start(uint16_t timeout,
                  const Received &received,
                  uint16_t size,
                  const Advertised &advertised)
{
   return _private->start(timeout, received, size, advertised);
}

int System::start(uint16_t size, const Advertised &advertised)
{
    return _private->start(size, advertised);
}

int System::advertise(const uint8_t *payload,
                      uint8_t size,
                      uint8_t times,
                      Id &id)
{
    return _private->advertise(payload, size, times, id);
}

int System::advertise(const Event &event, uint8_t times, Id &id)
{
    return _private->advertise(event, times, id);
}

void System::stop()
{
    _private->stop();
}

int System::initialize(BLE &/*ble*/)
{
    return 0;
}

void System::release(BLE &/*ble*/)
{

}

BLE &System::ble()
{
    return BLE::Instance();
}

const uint8_t *System::address() const
{
    return _private->address();
}

void System::release()
{
    release(BLE::Instance());
    if (_private == NULL)
        return;
    _private->release();
    delete _private;
    _private = NULL;
}


} // namespace core
} // namespace iot
