#ifndef RADIOPERIPHERAL_H
#define RADIOPERIPHERAL_H
#include <stdint.h>
namespace iot {
namespace radio {
class Peripheral
{
public:
    static void initialize();
    static bool disabled();
    static void enterSend(uint8_t *packet, uint8_t channel);
    static void exitSend();
    static void enterWait();
    static void exitWait();
    static void release();
};
} // namespace radio
} // namespace iot


#endif // RADIOPERIPHERAL_H
