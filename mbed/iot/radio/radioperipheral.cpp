#include <radio/radioperipheral.h>
#include <utils/utilsprint.h>
#include <nrf_soc.h>
#include <nrf_assert.h>
#ifdef NRF51
#include <nrf51.h>
#include <nrf51_bitfields.h>
#endif
#ifdef NRF52
#include <nrf52.h>
#include <nrf52_bitfields.h>
#endif

#include <stddef.h>
namespace iot {
namespace radio {
#define RADIO_DEFAULT_ADDRESS           (0x8E89BED6UL)
static const uint8_t freqBins[] = {2, 26, 80};
static uint8_t *packet = NULL;
void Peripheral::initialize()
{
    /* Reset all states in the radio peripheral */
    NRF_RADIO->POWER = 1;

    NRF_RADIO->EVENTS_DISABLED = 0;

    /* Set radio configuration parameters */
    NRF_RADIO->MODE = RADIO_MODE_MODE_Ble_1Mbit << RADIO_MODE_MODE_Pos;
    NRF_RADIO->TXPOWER = RADIO_TXPOWER_TXPOWER_Pos4dBm;
    NRF_RADIO->TXADDRESS = 0;
    NRF_RADIO->PREFIX0 = ((RADIO_DEFAULT_ADDRESS >> 24) & RADIO_PREFIX0_AP0_Msk);
    NRF_RADIO->BASE0 = (RADIO_DEFAULT_ADDRESS << 8);
    NRF_RADIO->RXADDRESSES = 0;
    NRF_RADIO->PCNF0 = (1 << RADIO_PCNF0_S0LEN_Pos) |
                       (8 << RADIO_PCNF0_LFLEN_Pos);

    NRF_RADIO->PCNF1 = (RADIO_PCNF1_WHITEEN_Enabled <<
                        RADIO_PCNF1_WHITEEN_Pos)|
                        (0xFF << RADIO_PCNF1_MAXLEN_Pos) |
                        (3 << RADIO_PCNF1_BALEN_Pos);

    NRF_RADIO->CRCCNF = (RADIO_CRCCNF_LEN_Three << RADIO_CRCCNF_LEN_Pos) |
                        (1 << RADIO_CRCCNF_SKIP_ADDR_Pos);

    NRF_RADIO->CRCINIT = 0x00555555UL;
    NRF_RADIO->CRCPOLY = 0x0100065BUL;
    NRF_RADIO->FREQUENCY = 2; // Frequency bin 80, 2480MHz, channel 39.
    NRF_RADIO->DATAWHITEIV = 37; // NOTE: This value needs to correspond to the frequency being used

    /* Lock interframe spacing, so that the radio won't send too soon / start RX too early */
    NRF_RADIO->TIFS = 0;
    /* Recover packet pointer */
    NRF_RADIO->PACKETPTR = (uint32_t) &packet[0];

    /* Enable radio interrupt propagation */
    NVIC_EnableIRQ(RADIO_IRQn);
}

void Peripheral::enterSend(uint8_t *packet, uint8_t channel)
{
    if (channel <= 10)
        NRF_RADIO->FREQUENCY = 4 + channel * 2;
    else if (channel <= 36)
        NRF_RADIO->FREQUENCY = 6 + channel * 2;
    else if (channel == 37)
        NRF_RADIO->FREQUENCY = 2;
    else if (channel == 38)
        NRF_RADIO->FREQUENCY = 26;
    else if (channel == 39)
        NRF_RADIO->FREQUENCY = 80;
    else {
        PRINTF("*radio peripheral* transmit channel: %d not compatibled\r\n",
               channel);
        return;
    }


    NRF_RADIO->DATAWHITEIV = channel & RADIO_DATAWHITEIV_DATAWHITEIV_Msk;
    NRF_RADIO->SHORTS = RADIO_SHORTS_READY_START_Msk |
                        RADIO_SHORTS_END_DISABLE_Msk;
    NRF_RADIO->PACKETPTR = (uint32_t) packet;
    NRF_RADIO->INTENSET = RADIO_INTENSET_DISABLED_Msk;
    NRF_RADIO->EVENTS_DISABLED = 0;

    NRF_RADIO->TASKS_TXEN = 1;
}
void Peripheral::exitSend()
{
    NRF_RADIO->EVENTS_DISABLED = 0;
    NRF_RADIO->INTENCLR = RADIO_INTENCLR_DISABLED_Msk;
}

bool Peripheral::disabled()
{
    return NRF_RADIO->EVENTS_DISABLED != 0;
}

void Peripheral::enterWait()
{
    NRF_RADIO->INTENSET = RADIO_INTENSET_DISABLED_Msk;
    NRF_RADIO->SHORTS = 0;
    NRF_RADIO->TASKS_DISABLE = 1;
}

void Peripheral::exitWait()
{
    NRF_RADIO->INTENCLR = RADIO_INTENCLR_DISABLED_Msk;
}

void Peripheral::release()
{
    NRF_RADIO->INTENCLR = 0xFFFFFFFF;
    NRF_RADIO->SHORTS = 0;
    NRF_RADIO->TASKS_DISABLE = 1;
}
} // namespace radio
} // namespace iot
