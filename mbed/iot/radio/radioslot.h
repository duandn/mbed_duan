#ifndef RADIOSLOT_H
#define RADIOSLOT_H
#include <stdint.h>
namespace iot {
namespace radio {
class Slot
{
public:
    typedef uint32_t Interval;

    virtual ~Slot() {}

    virtual Interval interval() const = 0;

    virtual bool finish() const = 0;

    virtual int enter() = 0;

    virtual int begin() = 0; // begin interval

    virtual int signal() = 0;

    virtual void end() = 0; // end interval interval

    virtual void exit(int error) = 0;

private:
    Slot *_successor;
};

} // namespace radio
} // namespace iot

#endif // RADIOSLOT_H
