#include <radio/radiocontrol.h>
#include <radio/radioschedule.h>
#include <radio/radioperipheral.h>
#include <radio/radioslot.h>
#include <std/stdqueue.h>
#include <std/stdlist.h>
#include <utils/utilsprint.h>
#include <nrf_assert.h>
#include <nrf_sdm.h>
#include <nrf_soc.h>
#ifdef NRF51
#include <nrf51.h>
#include <nrf51_bitfields.h>
#endif
#ifdef NRF52
#include <nrf52.h>
#include <nrf52_bitfields.h>
#endif

#include <stddef.h>
#include <string.h>

namespace iot {
namespace radio {

#define BLE_ADVERTISE_TYPE_OFFSET (0)
#define BLE_ADVERTISE_SIZE_OFFSET (1)
#define BLE_ADVERTISE_ADDRESS_OFFSET (2)

#define BLE_ADVERTISE_PAYLOAD_OFFSET (BLE_ADVERTISE_ADDRESS_OFFSET +\
                                      CONTROL_ADDRESS_SIZE)
#define BLE_ADVERTISE_PAYLOAD_SIZE (31)

#define BLE_ADVERTISE_PACKET_SIZE (BLE_ADVERTISE_PAYLOAD_OFFSET + \
                                   BLE_ADVERTISE_PAYLOAD_SIZE)

class Controller::Private: public Slot
{
private:
    typedef Controller::Address Address;
    typedef Controller::Advertised Advertised;
    typedef uint8_t Channel;
    typedef Controller::Id Id;
    typedef Controller::Map Map;
    typedef Controller::Size Size;
    typedef Controller::Times Times;
    class Queue;

    /**
     * @brief The Advertising class
     */
    class Advertise
    {
    public:

        typedef uint8_t Packet[BLE_ADVERTISE_PACKET_SIZE];

        Advertise(const Address &address):
            _remains(0) {

            _packet[BLE_ADVERTISE_TYPE_OFFSET] = 0x42;
            setAddress(address);
        }

        virtual ~Advertise() {

        }


        void setAddress(const Address &address) {
            _packet[BLE_ADVERTISE_ADDRESS_OFFSET + 0] = address[5];
            _packet[BLE_ADVERTISE_ADDRESS_OFFSET + 1] = address[4];
            _packet[BLE_ADVERTISE_ADDRESS_OFFSET + 2] = address[3];
            _packet[BLE_ADVERTISE_ADDRESS_OFFSET + 3] = address[2];
            _packet[BLE_ADVERTISE_ADDRESS_OFFSET + 4] = address[1];
            _packet[BLE_ADVERTISE_ADDRESS_OFFSET + 5] = address[0];
        }

        int fill(const Queue &queue) {
            struct Queue::Header header;
            Size size = queue.size();
            Size head = queue.head();
            const uint8_t *source = queue.payload();
            for(Size index = 0; index < sizeof(header); index++) {
                ((uint8_t *)&header)[index] = source[head];
                head = (head + 1) % size;
            }
            _id = header.id;
            _size = header.size;
            _remains = header.times;
            uint8_t *packet = &_packet[0] + BLE_ADVERTISE_PAYLOAD_OFFSET;
            for(Size index = 0; index < header.size; index++) {
                packet[index] = source[head];
                head = (head + 1) % size;
            }

            _packet[BLE_ADVERTISE_SIZE_OFFSET] = BLE_ADVERTISE_PAYLOAD_OFFSET +
                                                 header.size;

            if (BLE_ADVERTISE_PACKET_SIZE <= BLE_ADVERTISE_PAYLOAD_OFFSET +
                                             header.size)
                return header.size;

            memset(&_packet[0] + BLE_ADVERTISE_PAYLOAD_OFFSET + header.size,
                    0,
                    BLE_ADVERTISE_PACKET_SIZE - BLE_ADVERTISE_PAYLOAD_OFFSET -
                    header.size);

            return header.size;
        }

        inline const uint8_t *payload() const {
            return &_packet[0] + BLE_ADVERTISE_PAYLOAD_OFFSET;
        }

        inline uint8_t size() const {
            return _size;
        }

        inline uint8_t times() const {
            return _times;
        }

        inline uint8_t remains() const {
            return _remains;
        }

        inline void begin(const Controller::Map &map) {
            _channel = 36;
            next(map);
        }

        inline void next(const Controller::Map &map) {
            while (((map & (1 << (++_channel - 37))) == 0) && _channel < 40);
        }

        inline bool ended() const {
            return _channel >= 40;
        }

        void send() {
            Peripheral::enterSend(&_packet[0], _channel);
        }

        inline void complete() {
            _remains -= 1;
//            PRINTF("*radio control* complete\r\n");
        }

        inline bool finished() const {
            return _remains == 0;
        }

        inline const Id &id() const {
            return _id;
        }

    private:
        Id _id;
        uint8_t _size;
        Controller::Times _times;
        Controller::Times _remains;
        Channel _channel;
        Packet _packet;
    };

    typedef std::List<Advertise *> Advertises;
    /**
     * @brief The Queue class
     */
    class Queue
    {
    public:
        struct Header {
            Id id;
            Times times;
            uint8_t size;
        } __attribute__((__packed__));

        Queue():
            _id(0),
            _payload(NULL),
            _size(0),
            _head(0),
            _tail(0),
            _remains(0) {

        }

        virtual ~Queue() {
            release();
        }


        int initialize(const Size &size) {
            if (_payload != NULL)
                delete _payload;
            _payload =(uint8_t *)malloc(size);
            if (_payload == NULL)
                return -8;
            _size = size;
            _remains = size;
            _head = 0;
            _tail = 0;
            return 0;
        }

        int enqueue(const uint8_t *payload,
                    const Size &size,
                    const Times &times,
                    Id &id) {
            if (_remains < size + sizeof(struct Header))
                return -1;

            struct Header header;
            id = _id ++;
            header.times = times;
            header.size = size;
            header.id = id;

            Size tail = _tail;
            for(Size index = 0; index < sizeof(header); index++) {
                _payload[tail] = ((uint8_t *)&header)[index];
                tail = (tail + 1) % _size;
            }

            for(Size index = 0; index < size; index++) {
                _payload[tail] = payload[index];
                tail = (tail + 1) % _size;
            }

            _tail = tail;
            _remains -= sizeof(struct Header) + size;
            return 0;
        }


        int reenqueue(const uint8_t *payload,
                      const Size &size,
                      const Times &times,
                      const Id &id) {
            if (_remains < size + sizeof(struct Header))
                return -1;

            struct Header header;
            header.times = times;
            header.size = size;
            header.id = id;

            Size tail = _tail;
            for(Size index = 0; index < sizeof(header); index++) {
                _payload[tail] = ((uint8_t *)&header)[index];
                tail = (tail + 1) % _size;
            }

            for(Size index = 0; index < size; index++) {
                _payload[tail] = payload[index];
                tail = (tail + 1) % _size;
            }

            _tail = tail;
            _remains -= sizeof(struct Header) + size;
            return 0;
        }


        int dequeue(Advertise &advertise, const Controller::Map &map) {
            if (_remains >= _size)
                return -2;

            Size size = advertise.fill(*this);
            _head = (_head + size + sizeof(struct Header)) % _size;
            _remains += size + sizeof(struct Header);
            advertise.begin(map);
            return 0;
        }


        int skip(Id &id) {
            if (_remains >= _size)
                return -2;

            struct Queue::Header header;
            for(Size index = 0; index < sizeof(header); index++) {
                ((uint8_t *)&header)[index] = _payload[_head];
                _head = (_head + 1) % _size;
            }

            id = header.id;
            _head = (_head + header.size) % _size;
            _remains += sizeof(header) + header.size;

            return 0;
        }

        inline bool emptied() const {
            return _size <= _remains;
        }

        inline bool enough(const Size &size) const {
            return _remains > sizeof(struct Header) + size;
        }

        inline const Size &size() const {
            return _size;
        }

        inline const Size &head() const {
            return _head;
        }

        inline const Size &tail() const {
            return _tail;
        }

        inline const uint8_t *payload() const {
            return _payload;
        }

        void release() {
            if (_payload == NULL)
                return;
            free(_payload);
            _payload = 0;
            _size = 0;
            _remains = 0;
        }

    private:
        Id _id;
        uint8_t *_payload;
        Size _size;
        Size _head;
        Size _tail;
        Size _remains;
    };

public:
    enum State {
        Sending,
        Waiting,
        Idle,
    };

    /**
     * @brief Private
     * @param address
     * @param map
     * @param advertised
     */
    Private(const Address &address,
            const Map &map,
            const Controller::Interval &interval,
            const Advertised &advertised):
        _map(map),
        _interval(interval),
        _advertised(advertised),
        _state(Idle) {
        memcpy(&_address[0], &address[0], sizeof(Address));
    }

    virtual ~Private() {
    }

    int initialize(const Size &size) {
        int error = _scheduler.initialize();
        if (error != 0)
            return error;
        error = _queue.initialize(size);
        if (error != 0) {
            _scheduler.release();
            return error;
        }
        return 0;
    }

    void release() {
        _queue.release();
    }

    virtual Interval interval() const {
        return _interval;
    }

    virtual bool finish() const {
        return  _queue.emptied() &&
                _sendings.size() == 0 &&
                _pendings.size() == 0;
    }

    virtual int enter() {
        Peripheral::initialize();
        return 0;
    }

    virtual int begin() {
        return _state != Idle? 0: send();
    }

    virtual int signal() {

        if (_sendings.size() == 0)
            return -2;

        if (_state == Sending) {
            Peripheral::exitSend();
            Peripheral::enterWait();
            _state = Waiting;
            return 0;
        }

        if (_state != Waiting)
            return -8;

        Peripheral::exitWait();
        Advertise *sending = _sendings.head();
        sending->next(_map);
        if (sending->ended() != true) {
            sending->send();
            _state = Sending;
            return 0;
        }

        sending->complete();
        _sendings.dequeue();
        if (sending->finished()) {
            _frees.enqueue(sending);
            _advertised.call(sending->id(), false);
        } else {
            sending->begin(_map);
            _pendings.enqueue(sending);
        }

        return send();
    }

    virtual void end() {
        while (_pendings.size() > 0) {
            Advertise *advertise = _pendings.dequeue();
            advertise->begin(_map);
            _sendings.enqueue(advertise);
        }

    }

    virtual void exit(int error) {
        _state = Idle;
//        PRINTF("*radio control* finished\r\n");
    }


    void setAddress(const Address &address) {
        memcpy(&_address[0], &address[0], CONTROL_ADDRESS_SIZE);
    }

    int advertise(const uint8_t *payload,
                  const Size &size,
                  const Times &times,
                  Id &id) {

        if (size > BLE_ADVERTISE_PAYLOAD_SIZE)
            return -2;

        int error;

        while (_queue.enqueue(payload, size, times, id) != 0) {
            Id id;
            error = _queue.skip(id);
            if (error != 0)
                return error;

            _advertised.call(id, true);
        }

        if (_sendings.size() > 0 || _pendings.size() > 0)
            return 0;
        PRINTF("*radiocontrol*  111111111111111111111. \r\n");

        error = _scheduler.schedule(this);
        PRINTF("*radiocontrol*  22222222222222222. \r\n");

        if (error != 0) {
            Id id;
            _queue.skip(id);
            return error;
        }

        return 0;
    }
private:
    int send() {

        Advertise *sending;

        if (_sendings.size() > 0) {
            sending = _sendings.head();
        } else {
            if (_queue.emptied()) {
                _state = Idle;
                return 0;
            }
            sending = _frees.size() > 0? _frees.dequeue():
                                         new Advertise(_address);
            int error = _queue.dequeue(*sending, _map);
            if (error != 0) {
                _frees.enqueue(sending);
                _state = Idle;
                return error;
            }
            _sendings.enqueue(sending);
        }
        _state = Sending;
        sending->send();
        return 0;
    }

private:
    Controller::Map _map;
    Channel _channel;
    Controller::Interval _interval;
    Advertised _advertised;
    Queue _queue;

    Advertises _sendings;
    Advertises _pendings;
    Advertises _frees;
    Address _address;
    State _state;
    Scheduler _scheduler;

};

/**
 * @brief Controller::Controller
 */
Controller::Controller():
    _private(NULL)
{

}

Controller::~Controller()
{

}

int Controller::initialize(const Address &address,
                           const Map &map,
                           const Size &size,
                           const Interval &interval,
                           const Advertised &advertised)
{
    if (_private != NULL)
        return -1;

    if ((map & All) == 0)
        return -4;

    _private = new Private( address, map, interval, advertised);
    int error = _private->initialize(size);
    if (error != 0) {
        delete _private;
        _private = NULL;
        return error;
    }
    return 0;
}

void Controller::setAddress(const Address &address)
{
    _private->setAddress(address);
}

int Controller::advertise(const uint8_t *payload,
                          const Size &size,
                          const Times &times,
                          Id &id)
{
    return _private->advertise(payload, size, times, id);
}

void Controller::release()
{
    if (_private == NULL)
        return;

    _private->release();
    delete _private;
    _private = NULL;
}

Controller &Controller::instance()
{
    static Controller _instance;
    return _instance;
}
} // namespace radio
} // namespace iot

