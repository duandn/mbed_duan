#include <radio/radioschedule.h>
#include <radio/radioslot.h>
#include <radio/radioperipheral.h>
#include <time/timetimer0.h>
#include <std/stdqueue.h>
#include <utils/utilsprint.h>
#include <nrf_soc.h>
#ifdef NRF51
#include <nrf51.h>
#endif
extern "C" {
#include <softdevice_handler.h>
//#include <pstorage.h>
}

namespace iot {
namespace radio {
#define SLOT_EXTENED_MARGIN 1000
#define SLOT_MAIN_MARGIN 3000
#define SLOT_LENGTH_LIMIT (10000000UL) /**< The upper limit for timeslot extensions. */

class Scheduler::Private
{
public:
    typedef uint32_t Duration;
    /**
     * @brief SchedulePrivate
     */
    Private():
        _length(0) {
        Private::_instance = this;
    }

    virtual ~Private() {
        Private::_instance = NULL;
    }

    int initialize() {
        int error =  sd_radio_session_open(&callback);
        if (error != 0)
            return error;
        _earliest.request_type = NRF_RADIO_REQ_TYPE_EARLIEST;
        _earliest.params.earliest.hfclk = NRF_RADIO_HFCLK_CFG_XTAL_GUARANTEED;
        _earliest.params.earliest.priority = NRF_RADIO_PRIORITY_NORMAL;
        _earliest.params.earliest.timeout_us = NRF_RADIO_EARLIEST_TIMEOUT_MAX_US;

        _normal.request_type = NRF_RADIO_REQ_TYPE_NORMAL;
        _normal.params.normal.hfclk = NRF_RADIO_HFCLK_CFG_XTAL_GUARANTEED;
        _normal.params.normal.priority = NRF_RADIO_PRIORITY_NORMAL;
        _normal.params.normal.distance_us = NRF_RADIO_DISTANCE_MAX_US;
        _length = 0;
        return 0;
    }

    int schedule(Slot *slot) {
        _slots.enqueue(slot);
        if (_slots.size() > 1) {
            PRINTF("*radio scheduler* schedule not empty\r\n");
            return 0;
        }

        _length = 0;
        _earliest.params.earliest.length_us = slot->interval();
        uint32_t error = sd_radio_request(&_earliest);
        if (error != 0) {
            _slots.dequeue();
            PRINTF("*radio scheduler* schedule request"
                   " failed with error: %d\r\n",
                   error);
            return error;
        }
        return 0;
    }

    void release() {
        sd_radio_session_close();
        while (_slots.size() > 0) {
            Slot *slot = _slots.dequeue();
            delete slot;
        }
    }

private:
    /**
     * @brief callback
     * @param signal
     * @return
     */
    static
    nrf_radio_signal_callback_return_param_t *
    callback(uint8_t signal) {
        return _instance->signal(signal);
    }

    /**
     * @brief signal
     * @param signal
     * @return
     */
    nrf_radio_signal_callback_return_param_t *signal(uint8_t signal) {

        switch (signal) {
        case NRF_RADIO_CALLBACK_SIGNAL_TYPE_START:
            return signalStart();
        case NRF_RADIO_CALLBACK_SIGNAL_TYPE_RADIO:
            return signalRadio();
        case NRF_RADIO_CALLBACK_SIGNAL_TYPE_TIMER0:
           return signalTimeout();
        case NRF_RADIO_CALLBACK_SIGNAL_TYPE_EXTEND_FAILED:
            return signalExtendFailed();
        case NRF_RADIO_CALLBACK_SIGNAL_TYPE_EXTEND_SUCCEEDED:
            return signalExtendSuccessed();
        }
        return &_param;
    }

    nrf_radio_signal_callback_return_param_t *signalStart() {

        time::Timer0::init();
        while(_slots.size() > 0) {
            Slot *slot = _slots.head();
            if (slot->finish()) {
                slot->exit(0);
                _slots.dequeue();
                break;
            }

            int error = slot->enter();
            if (error != 0) {
                _slots.dequeue();
                break;
            }

            time::Timer0::start(slot->interval() - SLOT_MAIN_MARGIN);
            error = slot->begin();
            if (error != 0) {
                _slots.dequeue();
                break;
            }
            return none();
        }

        return _slots.size() == 0? end(): requestAndEnd();
    }

    nrf_radio_signal_callback_return_param_t *signalRadio() {

        if (Peripheral::disabled() != true)
            return none();

        if (_slots.size() == 0)
            return end();

        Slot *slot = _slots.head();
        int error = slot->signal();
        if (error == 0) {
            if (!slot->finish())
                return none();
        }

        slot->exit(error);
        _slots.dequeue();
        if (_slots.size() == 0) {
            time::Timer0::stop();
            return end();
        }

        return requestAndEnd();
    }

    nrf_radio_signal_callback_return_param_t *signalTimeout() {
        if (_slots.size() == 0)
            return end();

        Slot *slot = _slots.head();
        slot->end();
        Duration interval = slot->interval();
        if (_length + interval >= SLOT_LENGTH_LIMIT) {
            _length = 0;
            return requestAndEnd();
        }
        _length += interval;
        return extend(interval);
    }

    nrf_radio_signal_callback_return_param_t *signalExtendFailed() {
        return requestAndEnd();
    }

    nrf_radio_signal_callback_return_param_t *signalExtendSuccessed() {
        if (_slots.size() == 0)
            return end();
        Slot *slot = _slots.head();
        int error = slot->begin();
        if (error == 0) {
            time::Timer0::start(slot->interval() - SLOT_EXTENED_MARGIN);
            return none();
        }
        _slots.dequeue();
        return _slots.size() == 0? end(): requestAndEnd();
    }

    nrf_radio_signal_callback_return_param_t *requestAndEnd() {
        Slot *slot = _slots.head();
        _earliest.params.earliest.length_us = slot->interval();
        uint8_t action = NRF_RADIO_SIGNAL_CALLBACK_ACTION_REQUEST_AND_END;
        _param.callback_action = action;
        _param.params.request.p_next = &_earliest;
        return &_param;
    }

    nrf_radio_signal_callback_return_param_t *none() {
        _param.callback_action = NRF_RADIO_SIGNAL_CALLBACK_ACTION_NONE;
        return &_param;
    }

    nrf_radio_signal_callback_return_param_t *extend(Duration length) {
        _param.callback_action = NRF_RADIO_SIGNAL_CALLBACK_ACTION_EXTEND;
        _param.params.extend.length_us = length;
        return &_param;
    }

    nrf_radio_signal_callback_return_param_t *end() {
        _param.callback_action = NRF_RADIO_SIGNAL_CALLBACK_ACTION_END;
        return &_param;
    }


private:
    nrf_radio_signal_callback_return_param_t _param;
    nrf_radio_request_t _earliest;
    nrf_radio_request_t _normal;
    Duration _length;
    typedef std::Queue<Slot *> Slots;
    Slots _slots;

private:
    static Private *_instance;
};

Scheduler::Private *Scheduler::Private::_instance = NULL;

Scheduler::Scheduler():
    _private(NULL)
{

}

Scheduler::~Scheduler()
{

}

int Scheduler::initialize()
{
    if (_private != NULL)
        return -1;
    _private = new Private();
    int error = _private->initialize();
    if (error != 0) {
        delete _private;
        _private = NULL;
        return error;
    }
    return 0;
}

int Scheduler::schedule(Slot *slot)
{
    return _private->schedule(slot);
}

void Scheduler::release()
{
    if (_private != NULL)
        return;
    _private->release();
    delete _private;
    _private = NULL;
}


} // namespace radio
} // namespace iot


