#ifndef RADIOSCHEDULE_H
#define RADIOSCHEDULE_H
#include <stdint.h>
namespace iot {
namespace radio {
class Slot;
class Scheduler
{
public:
    typedef uint32_t Interval;

    Scheduler();

    virtual ~Scheduler();

    int initialize();

    int schedule(Slot *slot);

    void release();

private:
    class Private;
    Private *_private;
};

} // namespace radio
} // namespace iot
#endif // RADIOSCHEDULE_H
