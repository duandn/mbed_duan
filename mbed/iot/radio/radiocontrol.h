#ifndef RADIOCOMTROL_H
#define RADIOCOMTROL_H
#include <stdint.h>
#include <mbed.h>
namespace iot {
namespace radio {
#define CONTROL_ADDRESS_SIZE (6)
class Controller
{
private:
    Controller();
    virtual ~Controller();
public:
    typedef uint16_t Size;
    typedef uint32_t Interval;
    typedef uint32_t Times;
    typedef uint16_t Id;
    typedef uint8_t Address[CONTROL_ADDRESS_SIZE];

    enum Map {
        Reversed = 0x00,
        Index37 = 0x01,
        Index38 = 0x02,
        Index3738 = 0x03,
        Index39 = 0x04,
        Index3739 = 0x05,
        Index3839 = 0x06,
        All = 0x07
    };

    enum Policy {
        Any = 0,
        Level1 = 1,
        Level2 = 2,
        WhiteListed = 3
    };

    typedef Callback<void(const Id &id, bool rejected)> Advertised;

    int initialize(const Address &address,
                   const Map &map,
                   const Size &size,
                   const Interval &interval, /*in micro seconds*/
                   const Advertised &advertised);

    void setAddress(const Address &address);

    int advertise(const uint8_t *payload,
                  const Size &size,
                  const Times &times,
                  Id &id);

    void release();
public:
    static Controller &instance();
private:
    class Private;
    Private *_private;
};
} // namespace radio
} // namespace iot
#endif // BLERADIOCOMTROL_H
