#include <devices/devicesprotocol.h>
#include <utils/utilscrc16.h>
#include <utils/utilsprint.h>
#include <utils/utilsendian.h>
#include <stdio.h>
using namespace iot::utils;
using namespace iot::utils::endian;
namespace iot {
namespace devices {


Protocol::~Protocol()
{

}

namespace slbp {

class Protocol::Private
{
public:
    Private():
        _newline(false),
        _value(0),
        _position(0) {
    }

    virtual ~Private() {

    }

    int encode(const uint8_t *packet,
               const devices::Protocol::Size &size,
               devices::Protocol::Packet &encoded) {

        devices::Protocol::Size encodedSize = size * 2 +
                sizeof(CRC16::CRC) * 2
                + 2;
        CRC16::CRC crc = toBig<CRC16::CRC>(CRC16::calculate(packet, size));
        encoded.resize(encodedSize);
        uint8_t *output = encoded.values();
        devices::Protocol::Size position = 0;
        char text[16];
        for(Size index = 0; index < size; index++) {
            sprintf(text, "%02X", packet[index]);
            output[position++] = text[0];
            output[position++] = text[1];
        }
        const uint8_t *crcs = (const uint8_t *)&crc;
        for(Size index = 0; index < sizeof(CRC16::CRC); index++) {
            sprintf(text, "%02X", crcs[index]);
            output[position++] = text[0];
            output[position++] = text[1];
        }
        output[position++] = '\r';
        output[position++] = '\n';
        return 0;
    }

    int decode(const uint8_t *packet,
               const devices::Protocol::Size &size,
               const devices::Protocol::Decoded &decoded) {

        for(devices::Protocol::Size index = 0; index < size; index++) {
            uint8_t character = packet[index];
            char value;
            if (character == '\r') {
                if (!_newline)
                    _newline = true;
                else
                    reset();
                continue;
            }

            if (character == '\n') {
                if (_newline && _position == 0)
                    finish(decoded);
                reset();
                continue;
            }

            if ('0' <= character && character <= '9') {
                value = character - '0';
            } else if ('A' <= character && character <= 'F') {
                value = character - 'A' + 0x0A;
            } else {
                reset();
                continue;
            }

            _value += value << (4 * (1 - _position));
            _position = (_position + 1) % 2;
            if (_position == 0) {
                _decoded.enqueue(_value);
                _value = 0;
            }
        }
        return 0;
    }

    void finish(const devices::Protocol::Decoded &decoded) {
        const uint8_t *values = _decoded.values();
        devices::Protocol::Size fullSize = _decoded.size();
        if (fullSize < sizeof(CRC16::CRC)) {
            _decoded.clear();
            return;
        }

        devices::Protocol::Size size = fullSize - sizeof(CRC16::CRC);

        CRC16::CRC sourceCrc;
        memcpy(&sourceCrc, values + size, sizeof(CRC16::CRC));
        sourceCrc = toLittle<CRC16::CRC>(sourceCrc);
        CRC16::CRC calculatedCrc = CRC16::calculate(values, size);
        if (sourceCrc != calculatedCrc)
            return;

        decoded.call(values, size);

    }

    void reset() {
        _decoded.clear();
        _position = 0;
        _value = 0;
        _newline = false;
    }
private:
    bool _newline;
    uint8_t _value;
    devices::Protocol::Size _position;
    devices::Protocol::Packet _decoded;
};

Protocol::Protocol():
    _private(new Private)
{

}

Protocol::~Protocol()
{
    delete _private;
}


int Protocol::encode(const uint8_t *packet,
                     const devices::Protocol::Size &size,
                     devices::Protocol::Packet &encoded)
{
    return _private->encode(packet, size, encoded);
}

int Protocol::decode(const uint8_t *packet,
                     const devices::Protocol::Size &size,
                     const devices::Protocol::Decoded &decoded)
{
    return _private->decode(packet, size, decoded);
}

uint8_t Protocol::match() const
{
    return '\n';
}

} // namespace slbp

namespace keypad {

class Protocol::Private
{
public:

    Private(uint8_t end, const devices::Protocol::Size &limit):
        _end(end),
        _limit(limit),
        _packet(limit) {
        _packet.clear();
    }

    inline uint8_t end() const {
        return _end;
    }

    int encode(const uint8_t *packet,
               const devices::Protocol::Size &size,
               devices::Protocol::Packet &encoded) {
        encoded.resize(size + 1);
        uint8_t *values = encoded.values();
        for(devices::Protocol::Size index = 0; index < size; index++)
            values[index] = packet[index];
        values[size] = _end;
        return 0;
    }

    int decode(const uint8_t *packet,
               const devices::Protocol::Size &size,
               const devices::Protocol::Decoded &decoded) {
        for(devices::Protocol::Size index = 0; index < size; index++) {
            uint8_t value = packet[index];
            if (value == _end) {
                if (_packet.size() == 0)
                    continue;
                decoded.call(_packet.values(), _packet.size());
                _packet.clear();
                continue;
            }
            _packet.enqueue(value);
            if (_packet.size() < _limit)
                continue;
            decoded.call(_packet.values(), _packet.size());
            _packet.clear();
        }
        return 0;
    }

private:
    uint8_t _end;
    devices::Protocol::Size _limit;
    devices::Protocol::Packet _packet;
};

Protocol::Protocol(uint8_t end, const Size &limit):
    _private(new Private(end, limit))
{

}

Protocol::~Protocol()
{
    delete _private;
}


int Protocol::encode(const uint8_t *packet, const Size &size, Packet &encoded)
{
    return _private->encode(packet, size, encoded);
}

int Protocol::decode(const uint8_t *packet,
                     const Size &size,
                     const Decoded &decoded)
{
    return _private->decode(packet, size, decoded);
}

uint8_t Protocol::match() const
{
    return _private->end();
}

} // namespace keypad

namespace slip {

#define SLIP_END             0xC0    /* indicates end of packet */
#define SLIP_ESC             0xDB    /* indicates byte stuffing */
#define SLIP_ESC_END         0xDC    /* ESC ESC_END means END data byte */
#define SLIP_ESC_ESC         0xDD    /* ESC ESC_ESC means ESC data byte */
#define SLIP_ESC_XON  0xDE /* ESC SLIP_ESC_XON means XON control byte */
#define SLIP_ESC_XOFF 0xDF /* ESC SLIP_ESC_XON means XOFF control byte */
#define XON 0x11 /* indicates XON charater */
#define XOFF 0x13 /* indicates XOFF charater */

class Protocol::Private
{
public:
    typedef devices::Protocol::Size Size;
    typedef devices::Protocol::Packet Packet;
    Private():
        _escaping(false) {
        _decoded.resize(64);
        _decoded.clear();
    }

    virtual ~Private() {

    }

    int encode(const uint8_t *packet, const Size &size, Packet &encoded) {
        CRC16::CRC crc = toBig<CRC16::CRC>(CRC16::calculate(packet, size));
        Size mainSize = Private::encodedSize(packet, size);
        Size crcSize = Private::encodedSize((const uint8_t *) &crc,
                                            sizeof(CRC16::CRC));

        encoded.resize(mainSize + crcSize + 1);

        uint8_t *values = encoded.values();


        encode(packet, size, values, mainSize);

        encode((const uint8_t *) &crc,
               sizeof(CRC16::CRC),
               values + mainSize,
               crcSize);
        values[mainSize + crcSize] = SLIP_END;
        return 0;
    }

    int decode(const uint8_t *packet,
               const devices::Protocol::Size &size,
               const devices::Protocol::Decoded &decoded) {
        for(Size index = 0; index < size; index ++) {
            uint8_t value = packet[index];

            if (_escaping) {

                if (value == SLIP_ESC_END)
                    _decoded.enqueue(SLIP_END);
                else if (value == SLIP_ESC_ESC)
                    _decoded.enqueue(SLIP_ESC);
                else if (value == SLIP_ESC_XON)
                    _decoded.enqueue(XON);
                else if (value == SLIP_ESC_XOFF)
                    _decoded.enqueue(XOFF);
                else
                    _decoded.enqueue(value);

                _escaping = false;
                continue;
            }
            if (value == SLIP_ESC) {
                _escaping = true;
                continue;
            }
            if (value != SLIP_END) {
                _decoded.enqueue(value);
                continue;
            }

            const uint8_t *values = _decoded.values();
            devices::Protocol::Size fullSize = _decoded.size();
            if (fullSize < sizeof(CRC16::CRC)) {
                reset();
                continue;
            }

            devices::Protocol::Size size = fullSize - sizeof(CRC16::CRC);

            CRC16::CRC sourceCrc = (values[size] << 8) + values[size + 1];

            CRC16::CRC calculatedCrc = CRC16::calculate(values, size);

            if (sourceCrc == calculatedCrc) {
//                PRINTF("*slip* crc matched \r\n");
                decoded.call(values, size);
            } else {
//                PRINTF("*slip* crc not matched "
//                       "calculated crc: 0x%04X, source crc: 0x%04X\r\n",
//                       calculatedCrc,
//                       sourceCrc);
            }


            reset();
        }
        return 0;
    }

    void reset() {
        _escaping = false;
        _decoded.clear();
    }
private:
    static Size encodedSize(const uint8_t *payload,
                            const Size &size) {

        Size position = 0;
        for(Size index = 0; index < size; index++) {
            uint8_t value = payload[index];
            if (value == SLIP_END) {
                position += 2;
            } else if (value == SLIP_ESC){
                position += 2;
            } else if (value == XON){
                position += 2;
            } else if (value == XOFF){
                position += 2;
            } else {
                position += 1;
            }
        }
        return position;
    }

    void encode(const uint8_t *packet,
                const Size &size,
                uint8_t *encoded,
                Size &encodedSize) {

        encodedSize = 0;
        for(Size index = 0; index < size; index++) {
            uint8_t value = packet[index];
            if (value == SLIP_END) {
                encoded[encodedSize++] = SLIP_ESC;
                encoded[encodedSize++] = SLIP_ESC_END;
            } else if (value == SLIP_ESC){
                encoded[encodedSize++] = SLIP_ESC;
                encoded[encodedSize++] = SLIP_ESC_ESC;
            } else if (value == XON){
                encoded[encodedSize++] = SLIP_ESC;
                encoded[encodedSize++] = SLIP_ESC_XON;
            } else if (value == XOFF){
                encoded[encodedSize++] = SLIP_ESC;
                encoded[encodedSize++] = SLIP_ESC_XOFF;
            } else {
                encoded[encodedSize++] = value;
            }
        }
    }


private:
    bool _escaping;
    Packet _decoded;
};

Protocol::Protocol():
    _private(new Private)
{

}

Protocol::~Protocol()
{
    delete _private;
}


int Protocol::encode(const uint8_t *packet,
                     const devices::Protocol::Size &size,
                     devices::Protocol::Packet &encoded)
{
    return _private->encode(packet, size, encoded);
}

int Protocol::decode(const uint8_t *packet,
                     const devices::Protocol::Size &size,
                     const devices::Protocol::Decoded &decoded)
{
    return _private->decode(packet, size, decoded);
}

uint8_t Protocol::match() const
{
    return SLIP_END;
}

} // namespace slip
} // namespace devices
} // namespace iot
