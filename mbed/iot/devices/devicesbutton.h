#ifndef DEVICESBUTTON_H
#define DEVICESBUTTON_H
#include <stdint.h>
#include <mbed.h>
namespace iot {
namespace devices {
class Settings;
class Button
{
public:
    class Private;
    Button();
    virtual ~Button();
public:
    typedef Callback<void()> Down;
    typedef Callback<void()> Up;
    int initialize(const Settings &settings, const Down &down, const Up &up);
    void release();
private:
    Private *_private;
};

} // namespace devices
} // namespace iot
#endif // DEVICESBUTTON_H
