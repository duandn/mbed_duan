#ifndef DEVICESTRIGGER_H
#define DEVICESTRIGGER_H
#include <mbed.h>

namespace iot {
namespace devices {

class Trigger
{
public:
    typedef Callback<void()> Trigged;

    virtual ~Trigger() {

    }

    virtual int start(const Trigged &trigger) = 0;

    virtual void stop() = 0;

};

namespace ticker {

class Trigger: public devices::Trigger
{
public:

    typedef float Duration;

    Trigger(const Duration &duration);

    virtual ~Trigger();

    const Duration &duration() const;

    virtual int start(const Trigged &trigged);

    virtual void stop();
private:
    class Private;
    Private *_private;
};
} // namespace ticker

namespace interrupt {

class Trigger: public devices::Trigger
{
public:

    Trigger(PinName pin);

    virtual ~Trigger();

    virtual int start(const Trigged &trigged);

    virtual void stop();
private:
    class Private;
    Private *_private;
};

} // namespace interrupt

} // namespace devices
} // namespace iot

#endif // DEVICESTRIGGER_H
