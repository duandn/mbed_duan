#include <devices/devicessettings.h>
namespace iot {
namespace devices {

Settings::~Settings()
{

}
namespace analog {

Settings::Settings(PinName pin):
    _pin(pin)
{

}

Settings::~Settings()
{

}

devices::Settings::Type Settings::type() const
{
    return devices::Settings::Analog;
}

} // namespace analog

namespace digital {

Settings::Settings(PinName pin):
    _pin(pin)
{

}

Settings::~Settings()
{

}

devices::Settings::Type Settings::type() const
{
    return devices::Settings::Analog;
}

} // namespace digital

namespace serial {

Settings::Settings(PinName transmit, PinName receive, const int &baud):
    _transmit(transmit),
    _receive(receive),
    _baud(baud)
{

}

Settings::~Settings()
{

}

devices::Settings::Type Settings::type() const
{
    return devices::Settings::Serial;
}

} // namespace serial

namespace interrupt {

Settings::Settings(PinName pin):
    _pin(pin)
{

}

Settings::~Settings()
{

}

devices::Settings::Type Settings::type() const
{
    return devices::Settings::Interrupt;
}

} // namespace interrupt

namespace i2c {

Settings::Settings(PinName data,
                   PinName clock,
                   int frequence,
                   int address):
    _data(data),
    _clock(clock),
    _frequence(frequence),
    _address(address) {

}

Settings::~Settings()
{

}

devices::Settings::Type Settings::type() const
{
    return devices::Settings::I2C;
}

} // namespace i2c

namespace spi {
Settings::Settings(PinName mosi, PinName miso, PinName clock, PinName ssel):
    _mosi(mosi),
    _miso(miso),
    _clock(clock),
    _select(ssel) {

}

Settings::~Settings()
{

}

devices::Settings::Type Settings::type() const
{
    return devices::Settings::SPI;
}

} // namespace spi
} // namespace device
} // namespace iot
