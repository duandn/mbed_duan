#ifndef DEVICESNFC_H
#define DEVICESNFC_H

#include <stdint.h>
#include <mbed.h>
#define I2C_ADDR (0x55 << 1)
#define MAX_NFC_LENGTH      17
#define BLOCK_SIZE      16
namespace iot {
namespace devices {
namespace i2c {
class Settings;
}
class NfcCom
{
public:
    NfcCom();
    virtual ~NfcCom();
    virtual int writeNFC(const char *buff);
    virtual int flush();
private:
    class Private;
    Private *_private;
};
namespace NT3H1201 {
class NfcCom :public devices::NfcCom
{
public:
    NfcCom();
    virtual ~NfcCom();
    int initialize(const devices::i2c::Settings &settings);
    virtual int writeNFC(const char *buff);
    virtual int flush();
    /**
     * @brief Release screen
     **/
    void release();
private:
    class Private;
    Private *_private;
};
} // namespace NT3H1201
} //namespace peripherals
} //namspace IoT

#endif // DEVICESNFC_H
