#ifndef DEVICESSERIAL_H
#define DEVICESSERIAL_H
#if DEVICE_SERIAL
#include <mbed.h>
namespace iot {
namespace devices {
class Protocol;

class Serial
{
public:
    Serial();
    virtual ~Serial();
public:
    typedef uint16_t Limited;
    typedef uint16_t Size;
    typedef uint16_t Id;
    typedef Callback<void(const uint8_t *packet, const Size &size)> Received;
    typedef Callback<void(const Id &id, bool rejected)> Sent;

    int open(mbed::Serial &serial,
             Protocol &protocol,
             uint8_t *memory,
             const Size &size,
             const Sent &sent);

    int open(mbed::Serial &serial,
             Protocol &protocol,
             const Received &received);

    int send(const uint8_t *packet, const Size &size, Id &id);

    void close();
private:
    class Private;
    Private *_private;
};

} // namespace devices
} // namespace iot
#endif

#endif // DEVICESSERIAL_H
