#ifndef DEVICESSENSOR_H
#define DEVICESSENSOR_H
#include <mbed.h>
#include <devices/devicesi2c.h>
namespace iot {
namespace devices {
namespace sensor {

/**
 * @brief The Event class
 */
class Event
{
public:
    enum Types {
        TypeStarted = 0x0000,
        TypeAnalog = 0x0001,
        TypeLight = 0x0002,
        TypeHumidity = 0x0003,
        TypeTemperature = 0x0004
    };

    typedef uint16_t Type;

    virtual ~Event() {}

    virtual Type type() const = 0;

};

namespace started {
/**
 * @brief The Started class
 */
class Event: public sensor::Event
{
public:

    Event(const int &error);

    virtual ~Event();

    virtual Event::Type type() const;

    inline const int &error() const {
        return _error;
    }

private:
    int _error;
};

} // namespace started


namespace analog {

/**
 * @brief The Event class
 */
class Event: public sensor::Event
{
public:
    typedef float Value;

    Event(const Value &value);

    virtual ~Event();

    virtual sensor::Event::Type type() const;

    inline const Value &value() const {
        return _value;
    }

private:
    Value _value;
};

} // namespace analog

namespace humidity {

class Event: public sensor::Event
{
public:
    typedef float Value;

    Event(const Value &value);

    virtual ~Event();

    virtual sensor::Event::Type type() const;

    inline const Value &value() const {
        return _value;
    }

private:
    Value _value;
};

} // namespace humidity

namespace temperature {

class Event: public sensor::Event
{
public:

    typedef float Value;

    Event(const Value &value);

    virtual ~Event();

    virtual sensor::Event::Type type() const;

    inline const Value &value() const {
        return _value;
    }

private:
    Value _value;
};

} // namespace humidity

namespace light {

class Event: public sensor::Event
{
public:
    typedef float Value;

    Event(const Value &value);

    virtual ~Event();

    virtual sensor::Event::Type type() const;

    inline const Value &value() const {
        return _value;
    }

private:
    Value _value;
};

} // namespace light
} // namespace sensor


class Trigger;
class Sensor
{
public:
    typedef float Wakeup;

    typedef Callback<void(const sensor::Event &event)> Evented;

    Sensor();

    virtual ~Sensor();

    void stop();

protected:

    int start(Trigger &trigger, const Evented &evented);

private:

    virtual int _start(const Evented &evented) = 0;

    virtual int _update() = 0;

    virtual void _stop() = 0;

private:

    friend class Private;
    class Private;
    Private *_private;
};

namespace i2c {
class Settings;
}


namespace opt3001 {

class Sensor: public devices::Sensor
{
public:
    typedef uint16_t ManufacturerId;
    typedef uint16_t DeviceId;
    typedef float Period;

    Sensor(const i2c::Settings &settings, const Period &period);

    virtual ~Sensor();

    const ManufacturerId &manufacturerId() const;

    const DeviceId &deviceId() const;

    int start(const Evented &updated);

private:

    virtual int _start(const Evented &evented);

    virtual int _update();

    virtual void _stop();

private:
    class Private;
    Private *_private;
};
} // namespace opt3001

namespace si7021a20 {

class Sensor: public devices::Sensor
{
public:
    typedef float Period;

    Sensor(PinName vdd,
           PinName data,
           PinName clock,
           I2C::Frequence frequence,
           const I2C::Address &address,
           const Period &period);

    virtual ~Sensor();

    int start(const Evented &evented);

private:

    virtual int _start(const Evented &evented);

    virtual int _update();

    virtual void _stop();

private:
    class Private;
    Private *_private;
};

} // namespace si7021a20

namespace analog {

class Settings;

class Sensor: public devices::Sensor
{
public:
    typedef float Period;

    Sensor(const Settings &settings, const Period &period);

    virtual ~Sensor();

    int start(const Evented &evented);

private:

    virtual int _start(const Evented &evented);

    virtual int _update();

    virtual void _stop();

private:
    class Private;
    Private *_private;
};

} // analog
} // namespace drivers
} // namespace iot
#endif // DEVICESSENSOR_H
