#ifndef DEVICESDISPLAY_H
#define DEVICESDISPLAY_H
#include <mbed.h>
namespace iot {
namespace gui {
class Painter;
}
namespace devices {
class Settings;
class Display
{
public:
    virtual ~Display();

    /**
     * @brief Turn the whole display on.  Used during initialisation.
     **/
    virtual int on() = 0;

    /**
     * @brief Turn the whole display off.
     * This will reset all configuration settings on
     * the controller to their defaults.
     **/
    virtual int off() = 0;

    /**
     * @brief Sends the display to sleep, but leaves RAM intact.
     **/
    virtual void sleep() = 0;

    /**
     * @brief Wakes up this display following a sleep() call.
     * @see sleep()
     **/
    virtual void wake() = 0;


    /**
     * @brief Get the painter of the display
     * @return The painter of the display
     **/
    virtual gui::Painter &painter() = 0;
};

namespace ssd1306 {


class Display
{
public:
    class Private;
    class Settings
    {
    public:
        enum VCC {
            External,
            SwitchCap
        };
        Settings(const devices::Settings &interface,
                 const VCC &vcc,
                 PinName reset);

        inline const devices::Settings &interface() const {
            return _interface;
        }

        inline const VCC &vcc() const {
            return _vcc;
        }

        inline void setVCC(const VCC &vcc) {
            _vcc = vcc;
        }

        inline PinName reset() const {
            return _reset;
        }

        inline void setReset(PinName reset) {
            _reset = reset;
        }

    private:
        const devices::Settings &_interface;
        VCC _vcc;
        PinName _reset;
    };
    /**
     * @brief Contruct the display
     */
    Display();

    /**
     * @brief Destruct the display
     */
    virtual ~Display();

    /**
     * @brief Initialise the display with settings.
     * @param settings The settings of the display
     **/
    int initialize(const Settings &settings);

    /**
     * @brief Turn the whole display on.  Used during initialisation.
     **/
    virtual int on();

    /**
     * @brief Turn the whole display off.
     * This will reset all configuration settings on
     * the controller to their defaults.
     **/
    virtual int off();

    /**
     * @brief Sends the display to sleep, but leaves RAM intact.
     **/
    virtual int sleep();

    /**
     * @brief Wakes up this display following a sleep() call.
     * @see sleep()
     **/
    virtual int wake();

    /**
     * @brief Get the painter of the display
     * @return The painter of the display
     **/
    virtual gui::Painter &painter();

    /**
     * @brief Release screen
     **/
    void release();


private:
    Private *_private;
};

} // namespace ssd1306
} // namespace device
} // namespace iot

#endif // DEVICESDISPLAY_H
