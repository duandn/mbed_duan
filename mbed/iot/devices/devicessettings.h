#ifndef DEVICESSETTINGS_H
#define DEVICESSETTINGS_H
#include <mbed.h>
namespace iot {
namespace devices {

class Settings
{
public:
    enum Type {
        Analog,
        Digital,
        Serial,
        Interrupt,
        I2C,
        SPI
    };
    virtual ~Settings();

    virtual Type type() const = 0;
};

namespace digital {

class Settings: public devices::Settings
{
public:
    Settings(PinName pin);

    virtual ~Settings();

    virtual devices::Settings::Type type() const;

    inline PinName pin() const {
        return _pin;
    }

    inline void setPin(PinName pin) {
        _pin = pin;
    }

private:
    PinName _pin;
};

} // namespace digital

namespace analog {

class Settings: public devices::Settings
{
public:
    Settings(PinName pin);

    virtual ~Settings();

    virtual devices::Settings::Type type() const;

    inline PinName pin() const {
        return _pin;
    }

    inline void setPin(PinName pin) {
        _pin = pin;
    }

private:
    PinName _pin;
};

} // namespace analog

namespace serial {

class Settings: public devices::Settings
{
public:
    Settings(PinName transmit, PinName receive, const int &baud);

    virtual ~Settings();

    virtual devices::Settings::Type type() const;

    inline PinName transmit() const {
        return _transmit;
    }

    inline void setTransmit(PinName transmit) {
        _transmit = transmit;
    }

    inline PinName receive() const {
        return _receive;
    }

    inline void setReceive(PinName receive) {
        _receive = receive;
    }

    inline const int &baud() const {
        return _baud;
    }
private:
    PinName _transmit;
    PinName _receive;
    int _baud;
};
} // namespace serial

namespace interrupt {

class Settings: public devices::Settings
{
public:
    Settings(PinName pin);

    virtual ~Settings();

    virtual devices::Settings::Type type() const;

    inline bool low() const {
        return _low;
    }

    void setLow(bool low) {
        _low = low;
    }

    inline PinName pin() const {
        return _pin;
    }

    inline void setPin(PinName pin) {
        _pin = pin;
    }

private:
    bool _low;
    PinName _pin;
};
} // namespace digital

namespace i2c {

class Settings: public devices::Settings
{
public:
    typedef int Frequence;
    typedef int Address;
    Settings(PinName data,
             PinName clock,
             int frequence,
             int address);

    virtual ~Settings();

    virtual devices::Settings::Type type() const;

    inline PinName data() const {
        return _data;
    }

    inline void setData(PinName data) {
        _data = data;
    }

    inline PinName clock() const {
        return _clock;
    }

    inline void setClock(PinName clock) {
        _clock = clock;
    }

    inline const Frequence &frequence() const {
        return _frequence;
    }

    inline void setFrequence(const Frequence &frequence) {
        _frequence = frequence;
    }

    inline const Address &address() const {
        return _address;
    }

    inline void setaddress(const Address &address) {
        _address = address;
    }

private:
    PinName _data;
    PinName _clock;
    Frequence _frequence;
    Address _address;
};
} // namespace i2c

namespace spi {
class Settings: public devices::Settings
{
public:
    Settings(PinName mosi, PinName miso, PinName clock, PinName select = NC);

    virtual ~Settings();

    virtual devices::Settings::Type type() const;

    inline PinName mosi() const {
        return _mosi;
    }

    void setMosi(PinName mosi) {
        _mosi = mosi;
    }

    inline PinName miso() const {
        return _miso;
    }

    void setMiso(PinName miso) {
        _miso = miso;
    }

    inline PinName clock() const {
        return _clock;
    }

    void setClock(PinName clock) {
        _clock = clock;
    }

    inline PinName select() const {
        return _select;
    }

    void setSelect(PinName select) {
        _select = select;
    }

private:
    PinName _mosi;
    PinName _miso;
    PinName _clock;
    PinName _select;
};

} // namespace spi
} // namespace device
} // namespace iot

#endif // DEVICESSETTINGS_H
