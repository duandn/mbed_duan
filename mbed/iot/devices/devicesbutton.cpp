#include <devices/devicesbutton.h>
#include <devices/devicessettings.h>
#include <core/coreapplication.h>
#include <core/corecommand.h>
#include <utils/utilsprint.h>
namespace iot {
namespace devices {

class Button::Private
{
public:
    /**
     * @brief The Down class
     */
    class Down: public core::Command
    {
    public:
        Down(const Button::Down &down):
            _down(down) {

        }

        virtual void execute() {
            _down.call();
        }

    private:
        Button::Down _down;
    };

    /**
     * @brief The Up class
     */
    class Up: public core::Command
    {
    public:
        Up(const Button::Up &down):
            _up(down) {

        }

        virtual void execute() {
            _up.call();
        }

    private:
        Button::Up _up;
    };

    Private(const Button::Down &down, const Button::Up &up):
        _down(down),
        _up(up) {
    }

    virtual ~Private() {

    }

    void down() {
        core::Application::instance()->schedule(&_down);
    }

    void up() {
        core::Application::instance()->schedule(&_up);
    }

private:
    Down _down;
    Up _up;
};

namespace interrupt {

class Private: public devices::Button::Private
{
public:
    Private(const Settings &settings,
            const Button::Down &down,
            const Button::Up &up):
        devices::Button::Private(down, up),
        _in(settings.pin()) {

        _in.rise(Callback<void()>(this, &Private::rise));
        _in.fall(Callback<void()>(this, &Private::fall));
    }

    void rise() {
        PRINTF("*button* rise\r\n");
        up();
    }

    void fall() {
        PRINTF("*button* fall\r\n");
        down();
    }

private:
    InterruptIn _in;
};

} // namespace interrupt

Button::Button():
    _private(NULL)
{

}

Button::~Button()
{

}

int Button::initialize(const Settings &settings,
                       const Button::Down &down,
                       const Button::Up &up)
{
    if (_private != NULL)
        return -1;

    if (settings.type() == Settings::Interrupt) {
        _private = new interrupt::Private((const interrupt::Settings &)
                                          settings,
                                          down,
                                          up);
        return 0;
    }

    return -2;
}

void Button::release()
{
    if (_private == NULL)
        return;
    delete _private;
    _private = NULL;
}

} // namespace device
} // namespace iot
