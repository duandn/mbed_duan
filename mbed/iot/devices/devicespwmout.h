#ifndef DEVICESPWMOUT_H
#define DEVICESPWMOUT_H
#include <mbed.h>
namespace iot {
namespace devices {

class PwmOut
{
public:
    typedef float Level;
    typedef float Period;

    PwmOut(PinName pin);

    virtual ~PwmOut();

public:

    void setLevel(const Level &level);

    void setPeriod(const Period &period);

private:
    class Private;
    Private *_private;
};

} // namespace device
} // namespace iot

#endif // DEVICESPWMOUT_H
