#ifndef DEVICEI2CNRF_H
#define DEVICEI2CNRF_H
#include <nrf_drv_twi.h>
#ifdef __cplusplus
extern "C" {
#endif

const nrf_drv_twi_t *devices_i2c_nrf_instance(int id);

#ifdef __cplusplus
}
#endif

#endif // DEVICEI2CNRF_H
