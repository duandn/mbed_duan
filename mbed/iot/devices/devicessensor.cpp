#include <devices/devicessensor.h>
#include <devices/devicessettings.h>
#include <devices/devicestrigger.h>
#include <devices/devicesi2c.h>
#include <core/coreapplication.h>
#include <core/corecommand.h>
#include <utils/utilsprint.h>
#include <mbed.h>
namespace iot {
namespace devices {
namespace sensor {
namespace started {
/**
 * @brief Started::Started
 * @param error
 */
Event::Event(const int &error):
    _error(error)
{

}

Event::~Event()
{

}

Event::Type Event::type() const
{
    return Event::TypeStarted;
}
} // namespace started


namespace analog {

Event::Event(const Value &value):
    _value(value)
{

}

Event::~Event()
{

}

sensor::Event::Type Event::type() const
{
    return sensor::Event::TypeAnalog;
}

}


namespace humidity {

Event::Event(const Value &value):
    _value(value)
{

}

Event::~Event()
{

}

sensor::Event::Type Event::type() const
{
    return sensor::Event::TypeHumidity;
}

}

namespace temperature {

Event::Event(const Value &value):
    _value(value)
{

}

Event::~Event()
{

}

sensor::Event::Type Event::type() const
{
    return sensor::Event::TypeTemperature;
}

} // namespace temperature

namespace light {

Event::Event(const Value &value):
    _value(value)
{

}
Event::~Event(){

}

sensor::Event::Type Event::type() const
{
    return sensor::Event::TypeLight;
}

} // namespace light
} // namespace sensor

class Sensor::Private
{
public:
    Private(Trigger &trigger, Sensor &sensor):
        _trigger(trigger),
        _sensor(sensor) {
    }

    virtual ~Private() {
    }

    void trigged() {
        _sensor._update();
    }

    int start() {
        return _trigger.start(Callback<void()>(this, &Private::trigged));
    }

    void stop() {
        _trigger.stop();
    }


private:
    Trigger &_trigger;
    Sensor &_sensor;
};

Sensor::Sensor():
    _private(NULL)
{

}

Sensor::~Sensor()
{

}

int Sensor::start(Trigger &trigger, const Evented &evented)
{
    if (_private != NULL)
        return 0;

    int error = _start(evented);
    if (error != 0)
        return error;

    _private = new Private(trigger, *this);
    error = _private->start();
    if (error != 0) {
        delete _private;
        _private = NULL;
        _stop();
        return error;
    }
    return 0;
}


void Sensor::stop()
{
    if (_private == NULL)
        return;

    _private->stop();
    delete _private;
    _private = NULL;
    _stop();
}


namespace opt3001 {

#define OPT3001_CONFIG_RN      0x00F0 /* [15..12] Range Number */
#define OPT3001_CONFIG_CT      0x0008 /* [11] Conversion Time */
#define OPT3001_CONFIG_M       0x0006 /* [10..9] Mode of Conversion */
#define OPT3001_CONFIG_OVF     0x0001 /* [8] Overflow */
#define OPT3001_CONFIG_CRF     0x8000 /* [7] Conversion Ready Field */
#define OPT3001_CONFIG_FH      0x4000 /* [6] Flag High */
#define OPT3001_CONFIG_FL      0x2000 /* [5] Flag Low */
#define OPT3001_CONFIG_L       0x1000 /* [4] Latch */
#define OPT3001_CONFIG_POL     0x0800 /* [3] Polarity */
#define OPT3001_CONFIG_ME      0x0400 /* [2] Mask Exponent */
#define OPT3001_CONFIG_FC      0x0300 /* [1..0] Fault Count */

/* Possible Values for CT */
#define OPT3001_CONFIG_CT_100      0x0000
#define OPT3001_CONFIG_CT_800      OPT3001_CONFIG_CT

/* Possible Values for M */
#define OPT3001_CONFIG_M_CONTI     0x0004
#define OPT3001_CONFIG_M_SINGLE    0x0002
#define OPT3001_CONFIG_M_SHUTDOWN  0x0000

/* Reset Value for the register 0xC810. All zeros except: */
#define OPT3001_CONFIG_RN_RESET    0x00C0
#define OPT3001_CONFIG_CT_RESET    OPT3001_CONFIG_CT_800
#define OPT3001_CONFIG_L_RESET     0x1000
#define OPT3001_CONFIG_DEFAULTS    (OPT3001_CONFIG_RN_RESET | OPT3001_CONFIG_CT_100 | OPT3001_CONFIG_L_RESET)

/* Enable / Disable */
#define OPT3001_CONFIG_ENABLE_CONTINUOUS  (OPT3001_CONFIG_M_CONTI | OPT3001_CONFIG_DEFAULTS)
#define OPT3001_CONFIG_ENABLE_SINGLE_SHOT (OPT3001_CONFIG_M_SINGLE | OPT3001_CONFIG_DEFAULTS)
#define OPT3001_CONFIG_DISABLE             OPT3001_CONFIG_DEFAULTS

class Sensor::Private
{
private:
#define LUX_MULTIPLIER 10.24f
    enum Command {
        Result		= 0x00,
        Config		= 0x01,
        LowLimit	= 0x02,
        HighLimit	= 0x03,
        ManufacturerId = 0x7e,
        DeviceId		= 0x7f
    };

public:
    Private(const i2c::Settings &settings, const Sensor::Period &period):
//        _i2c(settings.data(), settings.clock()),
        _address(settings.address()),
        _trigger(period) {
        DigitalIn *sda = new DigitalIn(settings.data(), PullUp);
        delete sda;
        DigitalIn *scl = new DigitalIn(settings.clock(), PullUp);
        delete scl;
//        _i2c.frequency(settings.frequence());
        PRINTF("*opt3001* contructor\r\n");
    }

    int start(const Sensor::Evented &evented) {
        /*
        char command[3];
        // Read device id
        PRINTF("*OPT3001* Manufacturer id: reading...\r\n");

        command[0] = ManufacturerId;

        int error = _i2c.write(_address, command, 1);
        if (error != 0) {
            PRINTF("*OPT3001* Manufacturer id: write failed with error:"
                   "%d.\r\n",
                   error);
            return error;
        }

        char result[2];
        error = _i2c.read(_address, result, 2);
        _manufacturerId = (((Sensor::DeviceId)result[0]) << 8) |
                (Sensor::DeviceId) result[1];

        PRINTF("*OPT3001* Manufacturer id: 0x%04X\r\n", _manufacturerId);

        // Read device id
        command[0] = DeviceId;
        error = _i2c.write(_address, command, 1);
        if (error != 0) {
            PRINTF("*OPT3001* Device id: write failed with error:"
                   "%d\r\n",
                   error);
            return error;
        }

        error = _i2c.read(_address, result, 2);
        if (error != 0) {
            PRINTF("*OPT3001* Device id: read failed with error: %d\r\n", error);
            return error;
        }
        _deviceId = (((Sensor::DeviceId)result[0]) << 8) |
                (Sensor::DeviceId) result[1];
        PRINTF("*OPT3001* Device id: 0x%04X\r\n", _deviceId);

        PRINTF("*OPT3001* configuring...");

        command[0] = Config; // C810h is the default content
        command[1] = (OPT3001_CONFIG_ENABLE_CONTINUOUS & 0x00FF);
        command[2] = (OPT3001_CONFIG_ENABLE_CONTINUOUS >> 8);
        error = _i2c.write(_address, command, 3);
        if (error != 0) {
            PRINTF("failed with error: %d\r\n", error);
            return error;
        }
        error = _i2c.read(_address, result, 2);
        if (error != 0) {
            PRINTF("failed with error: %d\r\n", error);
            return error;
        }
        PRINTF("successed with old config: 0x%02X%02C\r\n", result[0], result[1]);
        */
        return 0;
    }

    int update() {
//        char command[1] = {Result};

//        int error = _i2c.write(_address, command, 1);
//        if (error != 0) {
//            PRINTF("*OPT3001* update write failed with error: %d.\r\n", error);
//            return error;
//        }

//        char result[2];
//        error = _i2c.read(_address, result, 2);
//        if (error != 0) {
//            PRINTF("*OPT3001* update read failed with error: %d.\r\n", error);
//            return error;
//        }

//        uint16_t light = ((((uint16_t)result[0]) << 8) | result[1]) & 0x1FFF;
//        sensor::light::Event::Value lux = LUX_MULTIPLIER *
//                                          sensor::light::Event::Value(light);
//        sensor::light::Event value(lux);
//        _handler.call(value);
//        PRINTF("*OPT3001* light: %2.3f\r\n", lux);
        return 0;
    }

    inline const Sensor::ManufacturerId &manufacturerId() const {
        return _manufacturerId;
    }

    inline const Sensor::ManufacturerId &deviceId() const {
        return _deviceId;
    }

    inline ticker::Trigger &trigger() {
        return _trigger;
    }

private:
//    mbed::I2C _i2c;
    i2c::Settings::Address _address;
    ticker::Trigger _trigger;
    Sensor::Evented _handler;
    Sensor::ManufacturerId _manufacturerId;
    Sensor::ManufacturerId _deviceId;
};

/*
 * @brief Sensor::Sensor
 * @param settings
 * @param period
 */
Sensor::Sensor(const i2c::Settings &settings, const Period &period):
    _private(new Private(settings, period))
{


}

Sensor::~Sensor()
{
    delete _private;
}

int Sensor::start(const Evented &evented)
{
    return devices::Sensor::start(_private->trigger(), evented);
}


int Sensor::_start(const Evented &handler)
{
    return _private->start(handler);
}

int Sensor::_update()
{
    return _private->update();
}

void Sensor::_stop()
{
}

const Sensor::ManufacturerId &Sensor::manufacturerId() const
{
    return _private->manufacturerId();
}

const Sensor::DeviceId &Sensor::deviceId() const
{
    return _private->deviceId();
}

} // opt3001


namespace si7021a20 {

class Sensor::Private: public core::Command
{
public:

    /**
     * @brief The Ping class
     */
    class Ping: public core::Command
    {
    public:
        Ping(Private &app):
            _app(app) {

        }

        virtual ~Ping() {

        }

        virtual void execute() {
            _app.ping();
        }

    private:
        Private &_app;
    };

    /**
     * @brief The Humidity class
     */
    class Humidity: public core::Command
    {
    public:
        Humidity(Private &app):
            _app(app) {

        }

        virtual ~Humidity() {

        }

        virtual void execute() {
            _app.humidity();
        }

    private:
        Private &_app;
    };

    /**
     * @brief The Temperature class
     */
    class Temperature: public core::Command
    {
    public:
        Temperature(Private &app):
            _app(app) {

        }

        virtual ~Temperature() {

        }

        virtual void execute() {
            _app.temperature();
        }

    private:
        Private &_app;
    };

    /**
     * @brief The Done class
     */
    class Done: public core::Command
    {
    public:

        Done(Private &app):
            _app(app) {

        }

        virtual ~Done() {

        }

        virtual void execute() {
            _app.done();
        }

    private:
        Private &_app;
    };

    enum State {
        Uninitilized,
        Pinging,
        Idle,
        Humiditying,
        Temperaturing,
        Doning
    };

    Private(PinName vdd,
            PinName data,
            PinName clock,
            I2C::Frequence frequence,
            const I2C::Address &address,
            const Period &period):
        _vdd(vdd),
        _i2c(data, clock, frequence, 0),
        _address(address),
        _trigger(period),
        _state(Uninitilized),
        _ping(NULL),
        _humidity(NULL),
        _temperature(NULL){
    }

    virtual ~Private() {

    }

    void completed(int events) {

    }

    virtual void execute() {
        _vdd = 0;

        PRINTF("*si7021a20* stopped\r\n");
    }

    int start(const Sensor::Evented &evented) {
        if (_state != Uninitilized)
            return -1;
        _evented = evented;
        _state = Idle;
        _ping = new Ping(*this);
        _humidity = new Humidity(*this);
        _temperature = new Temperature(*this);
        _done = new Done(*this);
        return 0;
    }

    void done() {
        PRINTF("*si7021a20* done\r\n");
        if (_state != Doning)
            return;
        _vdd = 0;
        _i2c.stop();
        _state = Idle;
    }

    void i2cTemperatured(const I2C::Error &error) {

        PRINTF("*si7021a20* i2cTemperatured\r\n");
        PRINTF("**si7021a20* temperature[0]: 0x%04X temperature[1]: 0x%04X\r\n",
               _result[0],
               _result[1]);
        long tempraw = (long)_result[0] << 8 | _result[1];

        float value = ((175.72f * tempraw) / 65536.0f) - 46.85f;


        PRINTF("**si7021a20* temperature : %f\r\n", value);

        devices::sensor::temperature::Event eventTemp(value);
        _evented.call((const devices::sensor::temperature::Event)eventTemp);

        if (_state != Temperaturing)
            return;

        core::Application *app = core::Application::instance();
        if (error != I2C::Done) {
            app->schedule(_temperature);
        } else {
            _state = Doning;
            app->schedule(_done);
        }
    }

    void i2cHumiditied(const I2C::Error &error) {
        PRINTF("*si7021a20* i2cHumiditied\r\n");

        PRINTF("**si7021a20* humidity[0]: 0x%04X humidity[1]: 0x%04X\r\n",
               _result[0],
               _result[1]);

        long humraw = (long)_result[0] << 8 | _result[1];
        float value = ((125.0f * humraw) / 65536.0f) - 6.0f;
        PRINTF("**si7021a20* humidity : %f\r\n", value);

        const devices::sensor::humidity::Event eventHumidity(value);
        _evented.call((const devices::sensor::humidity::Event)eventHumidity);

        if (_state != Humiditying)
            return;

        core::Application *app = core::Application::instance();
        if (error != I2C::Done) {
            app->schedule(_humidity);
        } else {
            _state = Temperaturing;
            app->schedule(_temperature);
        }
    }

    void i2cPinged(const I2C::Error &error) {

        PRINTF("**si7021a20* i2cPinged ===>>>>>>>>\r\n");
        PRINTF("**si7021a20* Serial id: 0x%04X 0x%04X\r\n",
               _result[0],
               _result[1]);

        if (_state != Pinging)
            return;


        core::Application *app = core::Application::instance();

        if (error != I2C::Done) {
            app->schedule(_ping);
        } else {
            _deviceId = _result[0];
            _state =Humiditying;
            app->schedule(_humidity);
        }

    }

    void ping() {
        wait_ms(100);

        PRINTF("*si7021a20* ping\r\n");
        if (_state != Pinging)
            return;

        _command[0] = 0xFC;
        _command[1] = 0xC9;
        _result[0] = 0;
        _result[1] = 0;
        int error = _i2c.transfer(_address,
                                  &_command[0],
                                  2,
                                  &_result[0],
                                  8,
                                  I2C::Transfered(this, &Private::i2cPinged));


        if (error == 0)
            return;
        core::Application::instance()->schedule(_ping);
    }



    void temperature() {
        wait_ms(100);
        PRINTF("*si7021a20* temperature\r\n");
        if (_state != Temperaturing)
            return;

        _command[0] = 0xE3;
        int error = _i2c.transfer(_address,
                                  &_command[0],
                                  1,
                                  &_result[0],
                                  2,
                                  I2C::Transfered(this, &Private::i2cTemperatured));

        if (error == 0)
            return;
        core::Application::instance()->schedule(_temperature);
    }

    void humidity() {
        wait_ms(100);
        PRINTF("*si7021a20* humidity\r\n");
        if (_state != Humiditying)
            return;

        _command[0] = 0xE5;
        int error = _i2c.transfer(_address,
                                  &_command[0],
                                  1,
                                  &_result[0],
                                  2,
                                  I2C::Transfered(this, &Private::i2cHumiditied));


        if (error == 0)
            return;
        core::Application::instance()->schedule(_humidity);
    }

    int measure() {
        PRINTF("*si7021a20* measure\r\n");

        if (_state != Idle) {
            PRINTF("*si7021a20* measure state: %d\r\n", _state);
            return -1;
        }

        _vdd = 1;

        int error;
        error = _i2c.start();
        if (error != 0)
            return error;

        _state = Pinging;
        error = core::Application::instance()->schedule(_ping);
        if (error == 0) {
            _i2c.stop();
            _state = Idle;
            return -1;
        }
        return 0;
    }

    void stop() {
        _vdd = 0;
        delete _ping;
        _ping = NULL;
        delete _humidity;
        _humidity = NULL;
        delete _temperature;
        _temperature = NULL;
        delete _done;
        _done = NULL;

        _state = Uninitilized;
    }

    inline ticker::Trigger &trigger() {
        return _trigger;
    }

private:
    DigitalOut _vdd;
    devices::I2C _i2c;
    devices::i2c::Settings::Address _address;
    ticker::Trigger _trigger;
    Sensor::Evented _evented;
    State _state;
    uint8_t _command[2];
    uint8_t _result[8];
    uint8_t _deviceId;
    Ping *_ping;
    Humidity *_humidity;
    Temperature *_temperature;
    Done *_done;



};

Sensor::Sensor(PinName vdd,
               PinName data,
               PinName clock,
               I2C::Frequence frequence,
               const I2C::Address &address,
               const Period &period):
    _private(new Private(vdd, data, clock, frequence, address, period))
{

}

Sensor::~Sensor()
{
    delete _private;
}

int Sensor::start(const Evented &evented)
{
    return devices::Sensor::start(_private->trigger(), evented);
}

int Sensor::_start(const Evented &evented)
{
    return _private->start(evented);
}

int Sensor::_update()
{
    return _private->measure();
}



void Sensor::_stop()
{
    _private->stop();
}


} // si7021a20

namespace analog {

class Sensor::Private
{
private:
public:
    Private(const Settings &settings, const Sensor::Period &period):
        _input(settings.pin()),
        _trigger(period) {

    }

    int update() {
        return 0;
    }

    inline ticker::Trigger &trigger() {
        return _trigger;
    }

private:
    AnalogIn _input;
    ticker::Trigger _trigger;
};

/*
 * @brief Sensor::Sensor
 * @param settings
 * @param period
 */
Sensor::Sensor(const Settings &settings, const Period &period):
    _private(new Private(settings, period))
{

}

Sensor::~Sensor()
{
    delete _private;
}

int Sensor::start(const Sensor::Evented &evented)
{
    return devices::Sensor::start(_private->trigger(), evented);
}

int Sensor::_start(const Sensor::Evented &evented)
{
    return 0;
}

int Sensor::_update()
{
    return _private->update();
}

void Sensor::_stop()
{
}

} // analog
} // namespace devices
} // namespace iot
