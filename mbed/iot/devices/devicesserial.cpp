#include <devices/devicesserial.h>
#include <core/coreapplication.h>
#include <core/corecommand.h>
#include <devices/devicesprotocol.h>
#include <std/stdvector.h>
#include <std/stdlist.h>
#include <utils/utilsprint.h>
#include <rtos/rtos.h>
#if DEVICE_SERIAL
namespace iot {
namespace devices {
#define RECEIVE_SIZE 64
class Serial::Private
{
public:

    typedef Serial::Size Size;
    typedef Serial::Id Id;
    typedef std::Vector<uint8_t> Packet;
    class Queue
    {
    public:
        struct Header {
            Id id;
            Size size;
        } __attribute__((__packed__));

        Queue():
            _memory(NULL),
            _size(0),
            _head(0),
            _tail(0),
            _remains(0),
            _id(0) {

        }

        virtual ~Queue() {
            release();
        }

        int enqueue(const uint8_t *payload, const Size &size, Id &id) {
            if (_remains < size + sizeof(struct Header))
                return -1;
            id = _id ++;
            _header.id = id;
            _header.size = size;
            for(Size index = 0; index < sizeof(struct Header); index++) {
                _memory[_tail] = ((uint8_t *)&_header)[index];
                _tail = (_tail + 1) % _size;
            }

            for(Size index = 0; index < size; index++) {
                _memory[_tail] = payload[index];
                _tail = (_tail + 1) % _size;
            }
            _remains -= sizeof(struct Header) + size;
            return 0;
        }


        int dequeue(Id &id, Packet &packet) {
            if (_remains >= _size)
                return -2;

            for(Size index = 0; index < sizeof(_header); index++) {
                ((uint8_t *)&_header)[index] = _memory[_head];
                _head = (_head + 1) % _size;
            }
            packet.resize(_header.size);
            uint8_t *payload = packet.values();
            id = _header.id;
            for(Size index = 0; index < _header.size; index++) {
                payload[index] = _memory[_head];
                _head = (_head + 1) % _size;
            }
            _remains += sizeof(struct Header) + _header.size;
            return 0;
        }

        int skip(Id &id) {
            if (_remains >= _size)
                return -2;

            struct Header header;
            for(Size index = 0; index < sizeof(header); index++) {
                ((uint8_t *)&header)[index] = _memory[_head];
                _head = (_head + 1) % _size;
            }
            id = header.id;
            _head  = (_head + header.size) % _size;
            _remains += sizeof(struct Header) + header.size;
            return 0;
        }

        inline bool emptied() const {
            return _size <= _remains;
        }

        inline bool enough(const Size &size) const {
            return _remains >= sizeof(struct Header) + size;
        }

        int initialize(uint8_t *memory, const Size &size) {
            if (_memory != NULL)
                return -1;

            _memory = memory;
            _size = size;
            _remains = size;
            _head = 0;
            _tail = 0;
            return 0;
        }

        void release() {
            _memory = 0;
            _size = 0;
            _remains = 0;
        }

    private:
        uint8_t *_memory;
        Size _size;
        Size _head;
        Size _tail;
        Size _remains;
        Id _id;
        struct Header _header;
    };

    class Receive: public core::Command
    {
    public:
        typedef uint8_t Packet[RECEIVE_SIZE];
        Receive(const Packet &packet, Private &priv):
            _private(priv) {
            memcpy(&_packet[0], &packet[0], RECEIVE_SIZE);
        }

        virtual void execute() {
            _private.receive(_packet);
            delete this;
        }

    private:
        Packet _packet;
        Private &_private;
    };

    class Send: public core::Command
    {
    public:
        Send(Private &priv):
            _private(priv),
            _activated(false) {

        }

        virtual void execute() {
            _private.send();
        }

        inline void setActivated(bool activated) {
            _activated = activated;
        }

        inline bool activated() const {
            return _activated;
        }

    private:
        Private &_private;
        bool _activated;
    };

    class Resend: public core::Command
    {
    public:
        Resend(Private &priv):
            _private(priv) {

        }

        virtual void execute() {
            _private.resend();
        }

    private:
        Private &_private;
    };

    Private(mbed::Serial &serial, Protocol &protocol):
        _serial(serial),
        _protocol(protocol),
        _received(NULL),
        _sent(NULL),
        _id(0),
        _send(NULL),
        _resend(NULL) {
    }

    virtual ~Private() {
    }

    int open(const Serial::Received &received) {
        _error = _serial.read(&_receiving[0],
                                 RECEIVE_SIZE,
                                 event_callback_t(this, &Private::received),
                                 SERIAL_EVENT_RX_ALL,
                                 _protocol.match());
        if (_error != 0)
            return _error;
        _received = new Serial::Received(received);
        return 0;
    }

    int open(uint8_t *memory, const Size &size, const Serial::Sent &sent) {
        _error = _queue.initialize(memory, size);
        if (_error != 0)
            return _error;
        _sent = new Serial::Sent(sent);
        _send = new Send(*this);
        _resend = new Resend(*this);
        _encoded.resize(64);
        _encoded.clear();
        return 0;
    }

    int send() {
        if (_send->activated())
            return 0;

        _error = _queue.dequeue(_sendingId, _sending);
        if (_error != 0)
            return _error;


        _error = _serial.write(_sending.values(),
                               _sending.size(),
                               event_callback_t(this, &Private::sent));

        if (_error != 0) {
            //            PRINTF("*serial* resend failed with error: %d\r\n", _error);
            return _error;
        }

        _send->setActivated(true);
        return 0;
    }

    int send(const uint8_t *packet, const Size &size, Id &id) {

        if (_sent == NULL)
            return -3;
        //        PRINTF("*serial* send start\r\n");
        bool emptied = _queue.emptied();
        _error =  _protocol.encode(packet, size, _encoded);
        if (_error != 0) {
            //            PRINTF("*serial* send skip failed with error: %d\r\n", _error);
            return _error;
        }

        //        PRINTF("*serial* send enqueue\r\n");
        while (_queue.enqueue(_encoded.values(), _encoded.size(), id) != 0) {
            _error = _queue.skip(id);
            if (_error != 0) {
                //                PRINTF("*serial* send skip failed with error: %d\r\n", _error);
                return _error;
            }
            _sent->call(id, true);
        }

        if (emptied != true) {
            //            PRINTF("*serial* send not emptied\r\n");
            return 0;
        }

        //        PRINTF("*serial* send scheduling...");
        if (core::Application::instance()->schedule(_send) == 0) {
            //            PRINTF("*failed\r\n");
            _queue.skip(id);
            return -1;
        }
        //        PRINTF("*successed\r\n");
        return 0;
    }

    void close() {
        _serial.abort_read();
        _serial.abort_write();
        if (_sent != NULL) {
            delete _sent;
            _sent = NULL;
            _queue.release();
        }

        if (_received != NULL) {
            delete _received;
            _received = NULL;
        }

        if (_send != NULL) {
            delete _send;
            _send = NULL;
        }

        if (_resend != NULL) {
            delete _resend;
            _resend = NULL;
        }
    }
private:
    void received(int events) {

        if ((events & SERIAL_EVENT_RX_COMPLETE) == 0 &&
            (events & SERIAL_EVENT_RX_CHARACTER_MATCH) == 0) {
            //            PRINTF("*serial* received don't handle events: %d\r\n",events);
            return;
        }

        //        PRINTF("*serial* received\r\n");

        Receive *receive = new Receive(_receiving, *this);
        if (core::Application::instance()->schedule(receive) == 0)
            delete receive;
    }

    void sent(int events) {
        if ((events & SERIAL_EVENT_TX_COMPLETE) == 0) {
            //            PRINTF("*serial* sent don't handle events: %d\r\n", events);
            return;
        }

        if (core::Application::instance()->schedule(_resend) == 0) {
            //            PRINTF("*serial* sent schedule failed\r\n");
        }
    }


    void receive(const Receive::Packet &packet) {

        _error = _serial.read(&_receiving[0],
                              RECEIVE_SIZE,
                              event_callback_t(this, &Private::received),
                              SERIAL_EVENT_RX_ALL,
                             _protocol.match());
        if (_error != 0) {
            //            PRINTF("*serial* read failed with error: %d\r\n", error);
            return;
        }

        uint8_t match = _protocol.match();
        Protocol::Size length = 0;
        while(length < RECEIVE_SIZE) {
            uint8_t value = packet[length ++];
            if (value == match)
                break;
        }

        //        PRINTF("*serial* received length: %d\r\n", length);
        _protocol.decode(&packet[0],
                length,
                Protocol::Decoded(this, &Private::decoded));

    }

    void resend() {
        _send->setActivated(false);
        _sent->call(_sendingId, false);
        send();
    }

    void decoded(const uint8_t *packet, const Protocol::Size &size) {
        //        PRINTF("*serial* decoded\r\n");
        _received->call(packet, size);
    }

private:
    mbed::Serial &_serial;
    Protocol &_protocol;
    Serial::Received *_received;
    Serial::Sent *_sent;

    Receive::Packet _receiving;
    Serial::Id _id;

    Protocol::Packet _encoded;
    Packet _sending;
    Serial::Id _sendingId;
    Send *_send;
    Resend *_resend;

    Queue _queue;
    int _error;
};

/**
 * @brief Serial::Serial
 */
Serial::Serial():
    _private(NULL)
{

}

Serial::~Serial()
{

}

int Serial::open(mbed::Serial &serial,
                 Protocol &protocol,
                 uint8_t *memory,
                 const Size &size,
                 const Sent &sent)
{
    if (_private != NULL)
        return -1;

    _private = new Private(serial, protocol);

    int error = _private->open(memory, size, sent);
    if (error != 0) {
        delete _private;
        _private = NULL;
        return error;
    }
    return 0;
}

int Serial::open(mbed::Serial &serial,
                 Protocol &protocol,
                 const Received &received)
{
    if (_private != NULL)
        return -1;

    _private = new Private(serial, protocol);
    int error = _private->open(received);
    if (error != 0) {
        delete _private;
        _private = NULL;
        return error;
    }

    return 0;
}


int Serial::send(const uint8_t *packet, const Size &size, Id &id)
{
    if (_private == NULL)
        return -1;

    return _private->send(packet, size, id);
}

void Serial::close()
{
    if (_private != NULL)
        return;

    _private->close();
    delete _private;
    _private = NULL;
}

} // namespace devices
} // namespace iot

#endif
