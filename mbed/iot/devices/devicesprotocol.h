#ifndef DEVICESPROTOCOL_H
#define DEVICESPROTOCOL_H
#include <std/stdvector.h>
#include <stdint.h>
#include <mbed.h>
namespace iot {
namespace devices {

/**
 * @brief The Protocol class
 */
class Protocol
{
public:
    typedef uint16_t Size;
    typedef std::Vector<uint8_t> Packet;
    typedef Callback<void(const uint8_t *packet, const Size &size)> Decoded;


    virtual ~Protocol();


    virtual int encode(const uint8_t *packet,
                       const Size &size,
                       Packet &encoded) = 0;

    virtual int decode(const uint8_t *packet,
                       const Size &size,
                       const Decoded &decoded) = 0;

    virtual uint8_t match() const = 0;

};

namespace slbp {

class Protocol: public devices::Protocol
{
public:

    Protocol();

    virtual ~Protocol();


    virtual int encode(const uint8_t *packet,
                       const Size &size,
                       Packet &encoded);

    virtual int decode(const uint8_t *packet,
                       const Size &size,
                       const Decoded &decoded);

    virtual uint8_t match() const;

private:
    class Private;
    Private *_private;
};

} // namespace slbp

namespace keypad {

class Protocol: public devices::Protocol
{
public:

    Protocol(uint8_t end, const Size &limit);

    virtual ~Protocol();


    virtual int encode(const uint8_t *packet,
                       const Size &size,
                       Packet &encoded);

    virtual int decode(const uint8_t *packet,
                       const Size &size,
                       const Decoded &decoded);

    virtual uint8_t match() const;

private:
    class Private;
    Private *_private;
};

} // namespace keypad


namespace slip {

class Protocol: public devices::Protocol
{
public:

    Protocol();

    virtual ~Protocol();


    virtual int encode(const uint8_t *packet,
                       const Size &size,
                       Packet &encoded);

    virtual int decode(const uint8_t *packet,
                       const Size &size,
                       const Decoded &decoded);

    virtual uint8_t match() const;

private:
    class Private;
    Private *_private;
};

} // namespace slip
} // namespace devices
} // namespace iot
#endif // DEVICESPROTOCOL_H
