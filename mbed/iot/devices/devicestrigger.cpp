#include <devices/devicestrigger.h>
#include <core/coreapplication.h>
#include <core/corecommand.h>
#include <utils/utilsprint.h>

namespace iot {
namespace devices {
namespace ticker {

/**
 * @brief The Trigger::Private class
 */
class Trigger::Private: public core::Command
{

public:

    Private(const Trigger::Duration &duration):
        _duration(duration),
        _running(false) {

    }

    virtual void execute() {
        _trigged.call();
    }

    void ticked() {
        PRINTF("=========================trigger========================\r\n");

        core::Application::instance()->schedule(this);
    }

    int start(const Trigger::Trigged &trigged) {

        if (_running)
            return -1;

        _running = true;
        _trigged = trigged;
        _ticker.attach(Callback<void()>(this, &Private::ticked), _duration);

        return 0;
    }

    void stop() {
        if (_running != true)
            return;

        _ticker.detach();
        _running = false;
    }

private:
    Trigger::Duration _duration;
    bool _running;
    Ticker _ticker;
    devices::Trigger::Trigged _trigged;
};

/**
 * @brief Trigger::Trigger
 * @param duration
 */
Trigger::Trigger(const Duration &duration):
    _private(new Private(duration))
{

}

Trigger::~Trigger()
{
    delete _private;
}

int Trigger::start(const Trigged &trigged)
{
    return _private->start(trigged);
}

void Trigger::stop()
{
    _private->stop();
}

} // namespace ticker

namespace interrupt {

Trigger::Trigger(PinName pin)
{

}

Trigger::~Trigger()
{

}

int Trigger::start(const Trigged &trigged)
{

}

void Trigger::stop()
{

}

} // namespace interrupt
} // namespace devices
} // namespace iot

