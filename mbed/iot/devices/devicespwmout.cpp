#include <devices/devicespwmout.h>
#include <utils/utilsprint.h>

namespace iot {
namespace devices {

class PwmOut::Private
{
public:
    Private(PinName pin):
        _out(pin),
        _on(0.0f),
        _off(0.0f),
        _period(0.0f),
        _level(0.0f) {
        _out = 0;
    }

    void on() {
        _out = 1;
        _timeout.attach(Callback<void()>(this, &Private::off), _on);
    }

    void off() {
        _out = 0;
        _timeout.attach(Callback<void()>(this, &Private::on), _off);
    }

    void setPeriod(const PwmOut::Period &period) {
        _period = period;
        update();
    }

    void setLevel(const PwmOut::Level &level) {
        _level = level;
        update();
    }

private:
    void update() {

        _on = _period * _level;
        _off = _period * (1.0f - _level);

        PRINTF("*pwm out update* period: %1.8f level: %1.3f, on: %1.8f, off: %1.8f\r\n",
               _period,
               _level,
               _on,
               _off);

        if (_level <= 0.0f) {
            PRINTF("*pwm out update* level zero\r\n");
            return;
        }


        if (_on > 0 && _off > 0) {
            PRINTF("*pwm out update* start dim\r\n");
            _out = 1;
            _timeout.attach(Callback<void()>(this, &Private::off), _on);
        } else if (_on > 0) {
            PRINTF("*pwm out update* turn on\r\n");
            _out = 1;
            _timeout.detach();
        } else {
            PRINTF("*pwm out update* turn off\r\n");
            _out = 0;
            _timeout.detach();
        }
    }

private:
    Timeout _timeout;
    DigitalOut _out;
    PwmOut::Period _on;
    PwmOut::Period _off;
    PwmOut::Period _period;
    PwmOut::Level _level;
};

PwmOut::PwmOut(PinName pin):
    _private(new Private(pin))
{

}

PwmOut::~PwmOut()
{
    delete _private;
}

void PwmOut::setLevel(const Level &level)
{
    _private->setLevel(level);
}

void PwmOut::setPeriod(const Period &period)
{
    _private->setPeriod(period);
}

} // namespace device
} // namespace iot
