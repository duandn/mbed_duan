#ifndef DEVICESI2C_H
#define DEVICESI2C_H
#include <mbed.h>
#include <std/stdvector.h>

namespace iot {
namespace devices {
class I2C
{
public:
    typedef int Address;
    enum Error {
        Done,
        AddressNack,
        DataNack
    };

    typedef Callback<void(const Error &error)> Transfered;

    enum Frequence {
        Frequence100k,
        Frequence250k,
        Frequence400k
    };

    I2C(PinName data,
        PinName clock,
        Frequence frequence,
        int id);

    virtual ~I2C();

    int start();

    int transfer(const Address &address,
                 const uint8_t *transmit,
                 uint8_t transmits,
                 bool stop,
                 const Transfered &transfered);

    int transfer(const Address &address,
                 uint8_t *receive,
                 uint8_t receives,
                 const Transfered &transfered);

    int transfer(const Address &address,
                 const uint8_t *transmit,
                 uint8_t transmits,
                 uint8_t *receive,
                 uint8_t receives,
                 const Transfered &transfered);


    void stop();

private:
    class Private;

    Private *_private;
};

} // namespace devices
} // namespace iot

#endif // DEVICESI2C_H
