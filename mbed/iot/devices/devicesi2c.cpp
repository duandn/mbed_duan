#include <devices/devicesi2c.h>
#include <devices/devicesi2cnrf.h>
#include <std/stdlist.h>
#include <utils/utilsprint.h>

#include <platform/PlatformMutex.h>

#include <nrf_drv_twi.h>
#include <app_util_platform.h>

namespace iot {
namespace devices {

class I2C::Private
{
public:
    Private(PinName data,
            PinName clock,
            I2C::Frequence frequence,
            int id):
        _data(data),
        _clock(clock),
        _frequence(frequence),
        _id(id) {
    }

    int start() {

        PRINTF("*I2C* twi initializing...");
        const nrf_drv_twi_t *twi = devices_i2c_nrf_instance(_id);
        if (twi == NULL) {
            PRINTF("instance not found\r\n");
            return -1;
        }
        nrf_twi_frequency_t frequency;
        switch (_frequence) {
        case I2C::Frequence100k:
            frequency = NRF_TWI_FREQ_100K;
            break;
        case I2C::Frequence250k:
            frequency = NRF_TWI_FREQ_250K;
            break;
        default:
            frequency = NRF_TWI_FREQ_400K;
            break;
        }

        const nrf_drv_twi_config_t config = {
           .scl                = (uint32_t) _clock,
           .sda                = (uint32_t) _data,
           .frequency          = frequency,
           .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
           .clear_bus_init     = true
        };

        ret_code_t code = nrf_drv_twi_init(twi,
                                           &config,
                                           Private::nrf_drv_twi_evt_handler,
                                           this);
        if (code != NRF_SUCCESS) {
            PRINTF("failed with error: %d\r\n", error);
            return int(code);
        }
        nrf_drv_twi_enable(twi);
        PRINTF("successed\r\n");
        return 0;
    }

    int transfer(const I2C::Address &address,
                 const uint8_t *transmit,
                 uint8_t transmits,
                 bool stop,
                 const I2C::Transfered &transfered) {

        const nrf_drv_twi_t *twi = devices_i2c_nrf_instance(_id);
        if (twi == NULL) {
            PRINTF("*I2C* twi transfering...instance not found\r\n");
            return -1;
        }

        _transfered = transfered;
        int error = nrf_drv_twi_tx(twi, address >> 1, transmit, transmits, !stop);

        PRINTF("*I2C* twi transfered with error: %d\r\n", error);

        return (int) error;
    }

    int transfer(const I2C::Address &address,
                 uint8_t *receive,
                 uint8_t receives,
                 const I2C::Transfered &transfered) {
        PRINTF("*I2C* transfer with: address: 0x%08X, size: %d\r\n",
               address,
               receives);

        const nrf_drv_twi_t *twi = devices_i2c_nrf_instance(_id);
        if (twi == NULL) {
            PRINTF("*I2C* twi transfering...instance not found\r\n");
            return -1;
        }

        _transfered = transfered;

        int error = nrf_drv_twi_rx(twi, address >> 1, receive, receives);

        PRINTF("*I2C* twi transfered with error: %d\r\n", error);

        return (int) error;
    }


    int transfer(const I2C::Address &address,
                 const uint8_t *transmit,
                 uint8_t transmits,
                 uint8_t *receive,
                 uint8_t receives,
                 const I2C::Transfered &transfered) {

        wait_ms(100);
        memset(receive,receives, 0);
        const nrf_drv_twi_t *twi = devices_i2c_nrf_instance(_id);
        if (twi == NULL) {
            PRINTF("*I2C* twi transfering...instance not found\r\n");
            return -1;
        }

        _transfered = transfered;

        int error = nrf_drv_twi_txrx(twi,
                                     address >> 1,
                                     transmit,
                                     transmits,
                                     receive,
                                     receives);



        PRINTF("*I2C* twi transfered with error: %d\r\n", error);

        return (int) error;

    }

    void stop() {
        const nrf_drv_twi_t *twi = devices_i2c_nrf_instance(_id);
        if (twi == NULL)
            return;

        nrf_drv_twi_disable(twi);
        nrf_drv_twi_uninit(twi);
    }

private:
    void handle(const nrf_drv_twi_evt_t *event) {

//        PRINTF("*I2C* handle\r\n");
        I2C::Error error;
        switch (event->type) {
        case NRF_DRV_TWI_EVT_DATA_NACK:
            error = I2C::DataNack;
            break;
        case NRF_DRV_TWI_EVT_ADDRESS_NACK:
            error = I2C::AddressNack;
            break;
        default:
            error = I2C::Done;
            break;
        }
        _transfered.call(error);
    }

    static void nrf_drv_twi_evt_handler(nrf_drv_twi_evt_t const *p_event,
                                        void * p_context) {
        Private *priv = (Private *) p_context;
        priv->handle(p_event);
    }

private:
    PinName _data;
    PinName _clock;
    I2C::Frequence _frequence;
    int _id;
    I2C::Transfered _transfered;
};

/**
 * @brief I2C::I2C
 * @param data
 * @param clock
 */
I2C::I2C(PinName data,
         PinName clock,
         Frequence frequence,
         int id):
    _private(new Private(data, clock, frequence, id))
{

}

I2C::~I2C()
{
    delete _private;
}

int I2C::start()
{
    return _private->start();
}

int I2C::transfer(const Address &address,
                  const uint8_t *transmit,
                  uint8_t transmits,
                  bool stop,
                  const Transfered &transfered)
{
    return _private->transfer(address, transmit, transmits, stop, transfered);
}

int I2C::transfer(const Address &address,
                  const uint8_t *txData,
                  uint8_t txSize,
                  uint8_t *rxData,
                  uint8_t rxSize,
                  const Transfered &transfered)
{
    return _private->transfer(address,
                              txData,
                              txSize,
                              rxData,
                              rxSize,
                              transfered);
}

int I2C::transfer(const Address &address,
                  uint8_t *receive,
                  uint8_t receives,
                  const Transfered &transfered)
{
    return _private->transfer(address, receive, receives, transfered);
}

void I2C::stop()
{
    _private->stop();
}

} // namespace devices
} // namespace iot
