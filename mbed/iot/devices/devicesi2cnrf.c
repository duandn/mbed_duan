#include "devicesi2cnrf.h"
#include "sdk_common.h"
#if NRF_MODULE_ENABLED(TWI)
#define ENABLED_TWI_COUNT (TWI0_ENABLED + TWI1_ENABLED)
#if ENABLED_TWI_COUNT

#if NRF_MODULE_ENABLED(TWI0)
static const nrf_drv_twi_t _twi0 = NRF_DRV_TWI_INSTANCE(0);
#endif

#if NRF_MODULE_ENABLED(TWI1)
static const nrf_drv_twi_t _twi1 = NRF_DRV_TWI_INSTANCE(1);
#endif
#endif

const nrf_drv_twi_t *devices_i2c_nrf_instance(int id)
{
#if NRF_MODULE_ENABLED(TWI0)
    if (id == 0)
        return &_twi0;
#endif

#if NRF_MODULE_ENABLED(TWI1)
    if (id == 1)
        return &_twi1;
#endif

    return NULL;
}

#endif // NRF_MODULE_ENABLED(TWI)
