#include <devices/devicesnfc.h>
#include <devices/devicessettings.h>
#include <utils/utilsprint.h>
#include <mbed.h>
namespace iot {
namespace devices {
class NfcCom::Private
{
public:
    typedef short Size;
    Private(){
    }
    virtual ~Private() {
    }
    virtual int writeNFC(const char *buff) ;
    virtual int flush();
private:
}; //end class Private

NfcCom::NfcCom():
    _private(NULL)
{
}

NfcCom::~NfcCom()
{
}

namespace NT3H1201 {
NfcCom::NfcCom():
    _private(NULL)
{
}
NfcCom::~NfcCom()
{
}
class NfcCom::Private
{
public:
    enum _state{
        LOCK_RF = 1,
        UNLOCK_RF = 0,
    };

    Private(const devices::i2c::Settings &settings):
        _i2c(settings.data(), settings.clock()),
        _address(settings.address())
    {
        _i2c.frequency(settings.frequence());
    }
    virtual int set_nfc_state(uint8_t state){
        uint8_t buf_lock[MAX_NFC_LENGTH] = {
            0x00,                           // Block 0
            0x0AA,                           // I2C address
            0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,  // UID : Can not modify- Only Read
            0x00,0x44,0x00,                 // Internal: Only Read
            0x00,0x00,                      // Lock byte: Set to 0x00, 0x00, to unlock and 0xFF,0xFF, to lock
            0xE1,0x10,0xEA,0x00,            // Default CC byte
        };
        if(state == LOCK_RF){

            buf_lock[11] = 0xFF;
            buf_lock[12] = 0xFF;
            int error = _i2c.write(_address, (char*)buf_lock, MAX_NFC_LENGTH);
            if (error != 0){
                PRINTF("Lock with error code: 0x%02X \n\r",error);
                return error;
            }
        } else {
            buf_lock[11] = 0x0;
            buf_lock[12] = 0x0;
            int error = _i2c.write(_address, (char*)buf_lock, MAX_NFC_LENGTH);
            if (error != 0){
                PRINTF("Unlock with error code: 0x%02X \n\r",error);
                return error;
            }
        }
        wait_ms(20);
        return 0;
    }
    virtual int writeNFC(const char *buf) {

        const uint8_t ndef_len = 9;
        uint8_t data_ndef[ndef_len] =
        {
            /*NDEF Text Record*/
            0x03, 0x1F,  //NDEF message, 31 byte message
            0xD1,        //NDEF Record header: MB = 1b, ME = 1b, CF = 0b, SR = 1b, IL = 0b, TNF = 001b
            0x01, 0x1B,  //type length, payload length 27 byte with 24 byte token
            0x54,        //Type = Text
            0x02,        //Language code size
            0x65, 0x6E,  // Language = English, 'e', 'n',
        };
        int string_len = strlen(buf);
        int nfc_len = string_len + ndef_len;
        uint8_t block_len = nfc_len / BLOCK_SIZE;
        if(nfc_len % BLOCK_SIZE){
            ++block_len;
        }

        uint8_t max_len = block_len * MAX_NFC_LENGTH;

        // Create data for nfc buffer
        _buff = new uint8_t[max_len];

        memset(_buff,0x0,max_len);
        memcpy(_buff,data_ndef,ndef_len);
        memcpy(_buff + ndef_len, buf,string_len);

        // Set lenght ndef
        _buff[1] = nfc_len - 2;
        _buff[4] = nfc_len - 6;

        // Create buffer for write to I2C
        static uint8_t i2c_buf[MAX_NFC_LENGTH];
        memset(i2c_buf,0x0,MAX_NFC_LENGTH);

        // Create pointer position of nfc buffer
        uint8_t nfc_pos = 0;
        for( int i = 1 ; i <= block_len ; i++) {
            memset(i2c_buf,0x00, MAX_NFC_LENGTH);
            i2c_buf[0] = i ;
            memcpy(i2c_buf + 1,_buff + nfc_pos,BLOCK_SIZE);

            int  error = _i2c.write(_address, (char*)i2c_buf, MAX_NFC_LENGTH);
            if (error != 0){
                PRINTF("NFC error in write, code: 0x%02X \n\r",error);
                return error;
            }
            nfc_pos += BLOCK_SIZE;
            wait_ms(20);
        }
        set_nfc_state(LOCK_RF);
        return 0;
    }

    virtual int flush() {
        uint8_t buffer[17] = "";
        memset(buffer + 1 , 0x00, 16);
        int error = set_nfc_state(UNLOCK_RF);
        if (error != 0){
            PRINTF("Unlock NFC error %02x \n\r",error);
            return error;
        }
        for (int i = 1; i < 16; i++)
        {
            buffer[0] = i;
            error = _i2c.write(_address,(char*)buffer, MAX_NFC_LENGTH);
            if (error != 0){
                PRINTF("Clear NFC error block %d \n\r",i);
                return error;
            }
            wait_ms(20);
        }
        return 0;
    }
    virtual ~Private() {
        delete(_buff);
    }
    virtual int initialize() {
        flush();
        return 0;
    }
private:
    I2C _i2c;
    devices::i2c::Settings::Address _address;
    uint8_t* _buff;
};

int NfcCom::initialize(const devices::i2c::Settings &settings)
{
    if (_private != NULL)
        return -1;
    _private = new Private(settings);
    int error = _private->initialize();
    if (error != 0) {
        delete _private;
        _private = NULL;
        return error;
    }
    return 0;
}

int NfcCom::writeNFC(const char *buff){
    return _private->writeNFC(buff);
}

int NfcCom::flush(){
    return _private->flush();
}
} // NT3H1201
} // namespace device
} // namespace iot

