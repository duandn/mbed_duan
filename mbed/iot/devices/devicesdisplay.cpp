#include <devices/devicesdisplay.h>
#include <devices/devicessettings.h>
#include <gui/guipaint.h>
#include <utils/utilsprint.h>
namespace iot {
namespace devices {

Display::~Display()
{

}

namespace ssd1306 {

/**
 * @brief The Painter class for ssd1306 display
 */
class Painter: public gui::Painter
{
public:
    /**
     * @brief Contruct the painter
     * @param display The display private
     */
    Painter(Display::Private &display):
        gui::Painter(128, 64),
        _display(display) {

    }

    virtual ~Painter() {

    }

    /**
     * @brief Draw a single pixel at the specified coordinates
     * @param column The X coordinate.
     * @param row The Y coordinate.
     * @param color The color of the pixel as a 32-bit ARGB value.
     */
    virtual void drawPixel(short column,
                           short row,
                           const gui::Color &color);

    /**
     * @brief Flush the content of the painter to the display
     */
    virtual int flush();

private:
    Display::Private &_display;

};

class Display::Private
{
public:
    typedef short Size;

    Private():
        _painter(NULL) {
    }

    virtual ~Private() {
    }

    /**
     * @brief painter
     * @return
     */
    gui::Painter &painter() {
        return *_painter;
    }

    int initialize(Display::Settings::VCC vcc) {
        if (_painter != NULL)
            return -1;

        _painter = new Painter(*this);
        int error = initialize(*_painter, vcc);
        if (error != 0) {
            delete _painter;
            _painter = NULL;
            return error;
        }

        return 0;
    }

    void release() {
        delete _painter;
        _painter = NULL;
    }

    /**
     * @brief Turn on the display
     * @return true if successed else false
     */
    virtual int on() = 0;

    /**
     * @brief Turn off the display
     **/
    virtual int off() = 0;

    /**
     * @brief sleep the display
     */
    virtual int sleep() = 0;

    /**
     * @brief wakeup the display
     */
    virtual int wake() = 0;


    /**
     * @brief Draw a single pixel at the specified coordinates
     * @param column The X coordinate.
     * @param row The Y coordinate.
     * @param color The color of the pixel as a 32-bit ARGB value.
     */
    virtual void drawPixel(Painter &painter,
                           Size column,
                           Size row,
                           const gui::Color &color) = 0;

    /**
     * @brief Flush the content of the painter to the display
     */
    virtual int flush() = 0;


    /**
     * @brief initialize
     * @param painter
     * @return error The error code
     */
    virtual int initialize(Painter &painter, Display::Settings::VCC vcc) = 0;
private:
    Painter *_painter;
};

int Painter::flush()
{
    return _display.flush();
}

void Painter::drawPixel(short column,
                        short row,
                        const gui::Color &color)
{
    _display.drawPixel(*this, column, row, color);
}

namespace i2c {


class Private: public ssd1306::Display::Private
{
public:
    #define SSD1306_I2C_ADDRESS     0x78
    #define SSD1306_SETCONTRAST 0x81
    #define SSD1306_DISPLAYALLON_RESUME  0xA4
    #define SSD1306_DISPLAYALLON  0xA5
    #define SSD1306_NORMALDISPLAY  0xA6
    #define SSD1306_INVERTDISPLAY  0xA7
    #define SSD1306_DISPLAYOFF  0xAE
    #define SSD1306_DISPLAYON  0xAF
    #define SSD1306_SETDISPLAYOFFSET  0xD3
    #define SSD1306_SETCOMPINS  0xDA
    #define SSD1306_SETVCOMDETECT  0xDB
    #define SSD1306_SETDISPLAYCLOCKDIV  0xD5
    #define SSD1306_SETPRECHARGE  0xD9
    #define SSD1306_SETMULTIPLEX  0xA8
    #define SSD1306_SETLOWCOLUMN  0x00
    #define SSD1306_SETHIGHCOLUMN  0x10
    #define SSD1306_SETSTARTLINE  0x40
    #define SSD1306_MEMORYMODE  0x20
    #define SSD1306_COMSCANINC  0xC0
    #define SSD1306_COMSCANDEC  0xC8
    #define SSD1306_SEGREMAP  0xA0
    #define SSD1306_CHARGEPUMP  0x8D
    #define SSD1306_CHARGEPUMPON  0x14
    #define SSD1306_CHARGEPUMPOFF  0x10
    #define SSD1306_ACTIVATESCROLL  0x2F
    #define SSD1306_DEACTIVATESCROLL  0x2E
    #define SSD1306_SETVERTICALSCROLLAREA  0xA3
    #define SSD1306_RIGHTHORIZONTALSCROLL  0x26
    #define SSD1306_LEFTHORIZONTALSCROLL  0x27
    #define SSD1306_VERTICALANDRIGHTHORIZONTALSCROLL  0x29
    #define SSD1306_VERTICALANDLEFTHORIZONTALSCROLL  0x2A

    #define SSD1306_COMMAND 0x00
    #define SSD1306_DATA 0x40


    Private(const devices::i2c::Settings &settings, PinName reset):
        _i2c(settings.data(), settings.clock()),
        _reset(reset),
        _memory(new uint8_t[1024]) {
        _i2c.frequency(400000);
        _i2c.start();
        for(uint16_t index = 0; index < 1024; index++)
            _memory[index] = 0;
    }

    virtual ~Private() {

    }

    virtual int on() {
        return execute(SSD1306_DISPLAYON);
    }

    virtual int off() {
        return execute(SSD1306_DISPLAYOFF);
    }


    virtual int sleep() {
        return execute(SSD1306_DISPLAYOFF);
    }

    virtual int wake() {
        return execute(SSD1306_DISPLAYON);
    }

    virtual int setContrast(unsigned char contrast) {
        int error = execute(SSD1306_SETCONTRAST);
        if (error != 0)
            return error;

        return execute((char) contrast);
    }

    virtual void drawPixel(Painter &painter,
                           Size column,
                           Size row,
                           const gui::Color &color) {
        short width = painter.width();
        //Range check the pixel
        if (0 > column ||
                column >= width ||
                0 > row ||
                row >= painter.height())
            return;

        //Make sure the color is fully opaque
        if (color >> 24 != 255)
            return;

        //Determine the pixel byte index
        unsigned short index = column + (row / 8) * width;

        //Set or clear the pixel
        if ((color & 0x00FFFFFF) > 0)
            _memory[index] |= (1 << (row % 8));
        else
            _memory[index] &= ~(1 << (row % 8));

    }

    virtual int flush() {
        //Select low col 0, hi col 0, line 0
        execute(SSD1306_SETLOWCOLUMN | 0x0);
        execute(SSD1306_SETHIGHCOLUMN | 0x0);
        execute(SSD1306_SETSTARTLINE | 0x0);
        char buff[17];
        buff[0] = 0x40; // Data Mode

        // send display buffer in 16 byte chunks
        for(uint16_t start = 0; start < 1024; start += 16)
        {
            // TODO - this will segfault if buffer.size() % 16 != 0
            for(uint8_t index = 0; index < 16; index++)
                buff[index + 1] = _memory[start + index];

            int error = _i2c.write(SSD1306_I2C_ADDRESS, buff, sizeof(buff));
            if (error != 0)
                return error;
        }
        return 0;
    }

    virtual int initialize(Painter &painter, Display::Settings::VCC vcc) {
        _reset.write(1);
        // VDD (3.3V) goes high at start, lets just chill for a ms
        wait_ms(1);
        // bring reset low
        _reset.write(0);
        // wait 10ms
        wait_ms(10);
        // bring out of reset
        _reset.write(1);
        // turn on VCC (9V?)

        int error = execute(SSD1306_DISPLAYOFF);
        if (error != 0)
            return error;

        error = execute(SSD1306_SETDISPLAYCLOCKDIV);
        if (error != 0)
            return error;

        error = execute(0x80); // the suggested ratio 0x80
        if (error != 0)
            return error;

        error = execute(SSD1306_SETMULTIPLEX);
        if (error != 0)
            return error;

        error = execute(painter.height() - 1);
        if (error != 0)
            return error;

        error = execute(SSD1306_SETDISPLAYOFFSET);
        if (error != 0)
            return error;

        error = execute(0); // no offset
        if (error != 0)
            return error;

        error = execute(SSD1306_SETSTARTLINE | 0x0);
        if (error != 0)
            return error;

        error = execute(0); // no offset
        if (error != 0)
            return error;

        error = execute(SSD1306_CHARGEPUMP);
        if (error != 0)
            return error;

        error = execute(vcc == ssd1306::Display::Settings::External? 0x10:
                                                                     0x14);
        if (error != 0)
            return error;

        error = execute(SSD1306_MEMORYMODE);
        if (error != 0)
            return error;

        error = execute(0x00); // 0x0 act like ks0108
        if (error != 0)
            return error;

        error = execute(SSD1306_SEGREMAP);
        if (error != 0)
            return error;

        error = execute(SSD1306_COMSCANINC);
        if (error != 0)
            return error;

        error = execute(SSD1306_SETCOMPINS);
        if (error != 0)
            return error;

        error = execute(0x12);
        if (error != 0)
            return error;

        error = execute(SSD1306_SETCONTRAST);
        if (error != 0)
            return error;

        error = execute(vcc == ssd1306::Display::Settings::External? 0x9F:
                                                                     0xCF);
        if (error != 0)
            return error;

        error = execute(SSD1306_SETPRECHARGE);
        if (error != 0)
            return error;

        error = execute(vcc == ssd1306::Display::Settings::External? 0x22:
                                                                     0xF1);
        if (error != 0)
            return error;

        error = execute(SSD1306_SETVCOMDETECT);
        if (error != 0)
            return error;

        error = execute(0x40);
        if (error != 0)
            return error;

        error = execute(SSD1306_DISPLAYALLON_RESUME);
        if (error != 0)
            return error;


        error = execute(SSD1306_NORMALDISPLAY);
        if (error != 0)
            return error;

        error = execute(SSD1306_DISPLAYON);
        if (error != 0)
            return error;
        return 0;
    }

private:
    int execute(char command) {
        //Create a temporary buffer
        char buff[2];

        //Load the control byte and 8-bit command
        buff[0] = (char) SSD1306_COMMAND;
        buff[1] = (char) command;

        //Write the command
        return _i2c.write(SSD1306_I2C_ADDRESS, buff, 2);

    }

    int write(char data) {
        //Create a temporary buffer
        char buff[2];

        //Load the control byte and 8-bit command
        buff[0] = (char) SSD1306_DATA;
        buff[1] = (char) data;

        //Write the command
        return _i2c.write(SSD1306_I2C_ADDRESS, buff, 2);
    }

private:
    I2C _i2c;
    DigitalOut _reset;
    uint8_t *_memory;
};

} // namespace i2c


Display::Settings::Settings(const devices::Settings &interface,
                            const VCC &vcc,
                            PinName reset):
    _interface(interface),
    _vcc(vcc),
    _reset(reset)
{

}

/**
 * @brief Display::Display
 */
Display::Display():
    _private(NULL)
{

}


/**
 * @brief Display::~Display
 */
Display::~Display()
{

}

int Display::initialize(const Settings &settings)
{
    if (_private != NULL)
        return -1;

    const devices::Settings &interface = settings.interface();
    if (interface.type() == devices::Settings::I2C) {
        _private = new i2c::Private((devices::i2c::Settings &)
                                           interface,
                                           settings.reset());
    }

    if (_private == NULL)
        return -1;


    int error = _private->initialize(settings.vcc());
    if (error != 0) {
        delete _private;
        _private = NULL;
        return error;
    }
    return 0;
}


int Display::off()
{
    return _private->off();
}


int Display::on()
{
    return _private->on();
}


int Display::sleep()
{
    return _private->sleep();
}


int Display::wake()
{
    return _private->wake();
}

void Display::release()
{
    if (_private == NULL)
        return;
    _private->release();
    delete _private;
    _private = NULL;
}


gui::Painter &Display::painter()
{
    return _private->painter();
}

} // namespace ssd1306
} // namespace device
} // namespace iot
