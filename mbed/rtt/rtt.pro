TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    Examples/Main_RTT_InputEchoApp.c \
    Examples/Main_RTT_MenuApp.c \
    Examples/Main_RTT_PrintfTest.c \
    Examples/Main_RTT_SpeedTestApp.c \
    RTT/SEGGER_RTT.c \
    RTT/SEGGER_RTT_printf.c \
    Syscalls/RTT_Syscalls_GCC.c \
    Syscalls/RTT_Syscalls_IAR.c \
    Syscalls/RTT_Syscalls_KEIL.c

DISTFILES += \
    README.txt

HEADERS += \
    RTT/SEGGER_RTT.h \
    RTT/SEGGER_RTT_Conf.h
